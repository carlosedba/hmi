<?php

function config() {
	$prod = require __DIR__ . '/prod.php';
	$dev = require __DIR__ . '/dev.php';

	$default = [
		'netcore' => [
			'displayErrorDetails' => false, // set to false in production
			'addContentLengthHeader' => false, // Allow the web server to send the content-length header
			'determineRouteBeforeAppMiddleware' => false,
			'connectToWSServer' => false,
			'sendErrorToWSServer' => true,
			'webSocketsServer' => 'http://142.93.195.143:2000',
		],

		'renderer' => [
			'internal' => [
				'cache' => CACHE_PATH,
				'templates' => TEMPLATES_PATH,
			],

			'theme' => [
				'cache' => CACHE_PATH,
				'templates' => THEMES_PATH,
			]
		],

		'db' => [
			'driver' 			=> 'mysql',
			'host' 				=> 'localhost',
			'database' 		=> 'hmi',
			'username'	 	=> 'root',
			'password' 		=> 'AquelaSenhaMarota',
			'charset'   	=> 'utf8',
			'collation' 	=> 'utf8_unicode_ci',
			'prefix'    	=> 'rise_',
		],
	];

	switch (ENV) {
		case 'production':
			return array_replace_recursive($default, $prod);
			break;
		case 'development':
			return array_replace_recursive($default, $dev);
			break;
		default:
			return array_replace_recursive($default, $dev);
	}
}

return config();

