<?php /* /usr/share/nginx/html/hmi/server/public_html/themes/hmi/includes/footer.blade.php */ ?>
<?php $__env->startSection('footer'); ?>
  <footer class="footer">
		<div class="footer-sections">
			<?php $__env->startSection('footer-sections'); ?>
				<?php echo $__env->yieldSection(); ?>
		</div>
    <!--
    <div class="footer-top">
      <div class="footer-content">
        <div class="footer-content-left">
          <div class="footer-menus">
            <div class="footer-menu">
              <a href="#" class="footer-menu-title"></a>
              <ul>
                <li><a href="#"></a></li>
              </ul>
            </div>
            <div class="footer-menu open">
              <a href="javascript:void(0)" class="footer-menu-title"><ul>
                <li><a href="#"></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="footer-content-right"></div>
      </div>
    </div>
    -->
    <div class="footer-bottom">
      <div class="footer-content">
        <div class="footer-content-left">
          <div class="footer-info">
            <p>HMI - Honoris Mérito Institute</p>
          </div>
        </div>
        <div class="footer-content-center">
          <div class="social-icons">
            <a href="#" class="social-icon social-icon-linkedin" target="_blank">
              <svg viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414">
                <path d="M13.632 13.635h-2.37V9.922c0-.886-.018-2.025-1.234-2.025-1.235 0-1.424.964-1.424 1.96v3.778h-2.37V6H8.51v1.04h.03c.318-.6 1.092-1.233 2.247-1.233 2.4 0 2.845 1.58 2.845 3.637v4.188zM3.558 4.955c-.762 0-1.376-.617-1.376-1.377 0-.758.614-1.375 1.376-1.375.76 0 1.376.617 1.376 1.375 0 .76-.617 1.377-1.376 1.377zm1.188 8.68H2.37V6h2.376v7.635zM14.816 0H1.18C.528 0 0 .516 0 1.153v13.694C0 15.484.528 16 1.18 16h13.635c.652 0 1.185-.516 1.185-1.153V1.153C16 .516 15.467 0 14.815 0z" fill-rule="nonzero"/>
              </svg>
            </a>
          </div>
        </div>
        <div class="footer-content-right">
          <div class="footer-info">
            <a href="mailto:contato@hmi.net.br">contato@hmi.net.br</a>
          </div>
        </div>
      </div>
    </div>
  </footer>
	<?php echo $__env->yieldSection(); ?>