<?php /* /usr/share/nginx/html/hmi/server/public_html/themes/hmi/layouts/default.blade.php */ ?>
<!DOCTYPE html>
<html lang="pt-BR">
	<?php echo $__env->make('includes.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<body>
		<?php echo $__env->make('includes.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
		<?php $__env->startSection('content'); ?>
			<?php echo $__env->yieldSection(); ?>
		<?php echo $__env->make('includes.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php $__env->startSection('modal'); ?>
      <?php echo $__env->yieldSection(); ?>
		<?php echo $__env->make('includes.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	</body>
</html>