<?php /* /usr/share/nginx/html/hmi/server/public_html/themes/hmi/o-que-oferecemos.blade.php */ ?>
<?php $__env->startSection('title'); ?>
  O que oferecemos | HMI - Honoris Mérito Institute
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description'); ?>
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('vendor-css'); ?>
  ##parent-placeholder-97688f63ed1a87ba587e78933c42edf42ecae775##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('application-css'); ?>
  ##parent-placeholder-b51d72c3ca446ab0f6f653f45ff8b7eb92a61211##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('fonts'); ?>
  ##parent-placeholder-04d3b602cdc8d51e1a3bb4d03f7dab96a9ec37e5##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  ##parent-placeholder-040f06fd774092478d450774f5ba30c5da78acc8##

  <section class="section white section-one wide">
    <div class="section-wrapper">
      <div class="row no-pad lr">
        <div class="row-titles">
          <p class="row-title">O que oferecemos</p>
        </div>
        <div class="content"><?php echo $conteudo['data']['content']; ?></div>
      </div>
    </div>
  </section>
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer-sections'); ?>
  ##parent-placeholder-2b652bef4ae9be2d3a3fe2f169e6a106e1d3faee##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
  ##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>