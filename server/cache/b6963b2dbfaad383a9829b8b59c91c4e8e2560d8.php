<?php /* /usr/share/nginx/html/hmi/server/public_html/themes/hmi/includes/head.blade.php */ ?>
<head>
	<meta charset="utf-8">
	<title><?php echo $__env->yieldContent('title'); ?></title>
	<meta name="description" content="<?php echo $__env->yieldContent('description'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="ClearType" content="true">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<?php $__env->startSection('vendor-css'); ?>
		<!-- Vendor CSS -->
		<link rel="stylesheet" type="text/css" href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'vendor/normalize/normalize.css')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'vendor/hamburgers/hamburgers.min.css')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'vendor/micromodal/micromodal.css')); ?>">
  	<link rel="stylesheet" type="text/css" href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'vendor/css-loaders/css/load8.css')); ?>">
		<?php echo $__env->yieldSection(); ?>

	<?php $__env->startSection('application-css'); ?>
		<!-- Application CSS -->
		<link rel="stylesheet" type="text/css" href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'resources/css/navbar.css')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'resources/css/hero.css')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'resources/css/section.css')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'resources/css/inputs.css')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'resources/css/main.css')); ?>">
		<?php echo $__env->yieldSection(); ?>

	<?php $__env->startSection('fonts'); ?>
		<!-- Fonts -->
		<link rel="stylesheet" type="text/css" href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'resources/fonts/opensans/stylesheet.css')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'resources/fonts/vollkorn/stylesheet.css')); ?>">
		<?php echo $__env->yieldSection(); ?>

	<?php $__env->startSection('js'); ?>
		<script src="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'vendor/modernizr/modernizr-custom.js')); ?>"></script>
		<?php echo $__env->yieldSection(); ?>
</head>