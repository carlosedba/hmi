<?php /* /usr/share/nginx/html/hmi/server/public_html/themes/hmi/includes/scripts.blade.php */ ?>
<?php $__env->startSection('scripts'); ?>
  <!-- Scripts -->
  <script type="text/javascript" src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script type="text/javascript" src="https://unpkg.com/micromodal/dist/micromodal.min.js"></script>
  <script type="text/javascript" src="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'resources/js/Utils.js')); ?>"></script>
  <script type="text/javascript" src="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'resources/js/Event.js')); ?>"></script>
  <script type="text/javascript">
    MicroModal.init();
  </script>
  <script type="text/javascript">
    const hamburger = document.querySelector('.hamburger')
    const navbarMenu = document.querySelector('.navbar-menu')

    hamburger.addEventListener('click', function (event) {
      hamburger.classList.toggle('is-active')
      navbarMenu.classList.toggle('active')
    })
  </script>
  <?php echo $__env->yieldSection(); ?>