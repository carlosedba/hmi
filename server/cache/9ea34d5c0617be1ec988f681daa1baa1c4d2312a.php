<?php /* /usr/share/nginx/html/hmi/server/public_html/themes/hmi/index.blade.php */ ?>
<?php $__env->startSection('title'); ?>
  HMI - Honoris Mérito Institute
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description'); ?>
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('vendor-css'); ?>
  ##parent-placeholder-97688f63ed1a87ba587e78933c42edf42ecae775##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('application-css'); ?>
  ##parent-placeholder-b51d72c3ca446ab0f6f653f45ff8b7eb92a61211##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('fonts'); ?>
  ##parent-placeholder-04d3b602cdc8d51e1a3bb4d03f7dab96a9ec37e5##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  ##parent-placeholder-040f06fd774092478d450774f5ba30c5da78acc8##

  <section class="hero">
    <div class="hero-background" style="background-image: url(<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/data/content_data/' . $hero['data']['picture']->filename)); ?>)"></div>
    <div class="hero-overlay"></div>
    <div class="hero-content">
      <div class="hero-content-wrapper">
        <p class="hero-title"><?php echo e($hero['data']['title']); ?></p>
      </div>
    </div>
  </section>

  <section class="section white section-one">
    <div class="section-wrapper">
      <div class="row no-pad lr">
        <div class="row-titles">
          <p class="row-title"><?php echo e($quemSomos['data']['title']); ?></p>
          <p class="row-text"><?php echo e($quemSomos['data']['description']); ?></p>
        </div>
        <div class="box box-one">
          <div class="box-content">
            <p><?php echo e($quemSomos['data']['box']->text); ?></p>
          </div>
          <a href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/quem-somos')); ?>" class="box-link">Saiba mais &#11208;</a>
        </div>
      </div>
    </div>
  </section>

  <section class="section white section-two">
    <div class="row small-pad no-pad lr bottom">
      <div class="row-titles">
        <p class="row-title">O que oferecemos</p>
      </div>
      <div class="featured-container">
        <?php $__currentLoopData = $oQueOferecemos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $content): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <a class="featured featured-one" href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . $content['data']['url'])); ?>">
            <div class="featured-background" style="background-image: url('<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/data/content_data/' . $content['data']['picture']->filename)); ?>')"></div>
            <img class="featured-icon" src="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/data/content_data/' . $content['data']['icon']->filename)); ?>">
            <p class="featured-title"><?php echo e($content['data']['name']); ?></p>
            <p class="featured-subtitle">Saiba mais &#11208;</p>
          </a>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
    </div>
  </section>

  <section class="section white section-three">
    <div class="section-wrapper">
      <div class="row small-pad no-pad lr">
        <div class="row-titles">
          <p class="row-title">Artigos e Notícias</p>
        </div>
        <div class="content">
        <?php $__currentLoopData = $noticias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="component-news small">
            <a class="news-picture" href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/noticias/' . $post['slug'])); ?>" style="background-image: url('<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/data/content_data/' . $post['cover']['filename'])); ?>')"></a>
            <p class="news-title"><?php echo e($post['title']); ?></p>
            <p class="news-description"><?php echo e($post['description']); ?></p>
            <a href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/noticias/' . $post['slug'])); ?>" class="btn">Saiba mais</a>
          </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
      </div>
    </div>
  </section>

  <section class="section white section-four">
    <div class="section-wrapper">
      <div class="row small-pad no-pad lr">
        <div class="row-titles">
          <p class="row-title">Eventos</p>
        </div>
        <div class="content">
          <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="component-event event-alpha" data-id="<?php echo e($event['id']); ?>">
              <div class="event-row">
                <div class="left">
                  <div class="event-date"><?php echo $event['date']; ?></div>
                  <div class="event-info">
                    <p class="event-title"><?php echo e($event['title']); ?></p>
                    <p class="event-text"><?php echo e($event['description']); ?></p>
                  </div>
                  <div class="event-info">
                    <p class="event-datetime">Local: <?php echo e($event['location']); ?></p>
                    <p class="event-datetime">Horário: <?php echo e($event['start_time']); ?> às <?php echo e($event['end_time']); ?></p>
                    <a class="event-link" href="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/eventos#' . $event['slug'])); ?>">Mais informações</a>
                  </div>
                </div>
                <div class="right">
                  <a href="<?php echo e($event['url']); ?>" class="btn btn-event">Inscreva-se</a>
                </div>
              </div>
            </div>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
      </div>
    </div>
  </section>

  <section id="cases-e-depoimentos" class="section white section-five">
    <div class="row no-pad">
      <div class="row-titles">
        <p class="row-title">Cases e depoimentos</p>
      </div>
      <div class="content">
        <?php $__currentLoopData = $cases; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $content): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <a class="component-case" href="<?php echo e($content['data']['url']); ?>">
            <div class="case-background" style="background-image: url('<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/data/content_data/' . $content['data']['picture']->filename)); ?>');"></div>
            <div class="case-background-overlay"></div>
            <div class="case-info">
              <p class="case-name"><?php echo e($content['data']['name']); ?></p>
              <p class="case-company"><?php echo e($content['data']['description']); ?></p>
            </div>
          </a>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
    </div>
  </section>

  <section class="section white section-six">
    <div class="row small-pad">
      <div class="content">
        <div class="component-noticias">
          <div class="noticias-items">
          <div class="noticias-cover""></div>
            <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $content): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <div class="noticias-item active">
                <a href="#" class="noticias-item-title"><?php echo e($content['data']['name']); ?></a>
                <p class="noticias-item-description"><?php echo e($content['data']['description']); ?></p>
                <div class="noticias-item-picture" data-src="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/data/content_data/' . $content['data']['picture']->filename)); ?>"></div>
              </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer-sections'); ?>
  ##parent-placeholder-2b652bef4ae9be2d3a3fe2f169e6a106e1d3faee##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('modal'); ?>
  ##parent-placeholder-964a3b119be846ddf05b003479487e359c1fa3da##

  <div class="modal micromodal-slide" id="event-subscription-modal" aria-hidden="true">
    <div class="modal__overlay" tabindex="-1" data-micromodal-close>
      <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="">
        <header class="modal__header">
          <div class="modal__titles">
            <h2 class="modal__title">
              Formulário de inscrição
            </h2>
            <p class="modal__description">Preencha os campos para inscrever-se no evento: <br><strong class="modal__event-name"></strong></p>
          </div>
          <button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
        </header>
        <main class="modal__content">
          <form method="POST" action="/subscribe">
            <div class="inputs">
              <div class="input w3-q1">
                <label>Nome:</label>
                <input type="text" name="name" required>
              </div>
              <div class="input w3-q1">
                <label>E-mail:</label>
                <input type="email" name="email" required>
              </div>
            </div>
          </form>
        </main>
        <footer class="modal__footer">
          <button class="modal__btn modal__btn-primary">Enviar</button>
        </footer>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
  ##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
  <script type="text/javascript" src="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . vendor/flickity/flickity.pkgd.min.js)); ?>"></script>
  <script type="text/javascript" src="<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('http://localhost:8080/hmi/server/public_html' . '/themes/' . 'hmi' . '/' . 'resources/js/EventSubscription.js')); ?>"></script>
  <script type="text/javascript">
    var secCases = document.querySelector('#cases-e-depoimentos .content')
    var cases = new Flickity(secCases, {
      cellAlign: 'left',
      wrapAround: true,
      freeScroll: true,
      contain: true,
      autoPlay: true,
      prevNextButtons: false,
    })
  </script>
  <script type="text/javascript">
    function setWidgetNoticias() {
      let widgets = document.querySelectorAll('.component-noticias .noticias-items')

      if (widgets.length) {
        ;[].forEach.call(widgets, function (el, i) {
          let loop = setInterval(loopWidgetNoticias, 5000)

          el.addEventListener('click',handleNoticiaClick)
        })
      }
    }

    function loopWidgetNoticias() {
      let noticias = document.querySelectorAll('.component-noticias .noticias-item')

      if (noticias.length) {
        let currentIndex = null
        let currentItem = null
        let nextPicture = null
        let nextItem = null
        let cover = document.querySelector('.noticias-cover')

        ;[].forEach.call(noticias, function (el, i, arr) {
          if (el.classList.contains('active')) {
            currentIndex = i
            currentItem = el
          }
        })

        if (currentIndex < noticias.length - 1) {
          nextItem = noticias[currentIndex+1]
        } else {
          nextItem = noticias[0]
        }

        nextPicture = nextItem.querySelector('.component-noticias .noticias-item-picture').dataset.src

        cover.style.backgroundImage = 'url(' + nextPicture + ')'
        currentItem.classList.remove('active')
        nextItem.classList.add('active')
      }
    }

    function handleNoticiaClick(event) {
      console.log(event.target)
    }

    setWidgetNoticias()
  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>