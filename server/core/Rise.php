<?php
namespace Rise;

require_once('Bootstrap.php');

use \PHPMailer;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Utils\IdGenerator;
use Rise\Auth\JWT;
use Rise\Models\User;

$container = new Container($config);
$net = new Netcore($container);

$net->add(new JWT([
  "secure" => false,
  "header" => "Authorization",
  "signature" => "RSA",
  "algorithm" => "SHA256",
  "key" => PUBLIC_KEY,
  "path" => ["/admin", "/api"],
  "ignore" => ["/api/v1/token", "/email/contato"],
  "callback" => function ($request, $response, $args) use ($container) {
    $container->set('token', $args['token']);
    return $response;
  },
  "error" => function ($request, $response, $args) {
    return $response;
  }
]));


/* ****************************** */
/* ************ API ************* */
/* ****************************** */

$net->group('/api/hmi', function () {
  $this->delete('/users/delete/{id}[/]',      constant('NAMESPACE') . '\Api\HMIUsers:delete');
  $this->post('/users/update/{id}[/]',        constant('NAMESPACE') . '\Api\HMIUsers:update');
  $this->post('/users/create[/]',             constant('NAMESPACE') . '\Api\HMIUsers:create');
  $this->get('/users/email/{email}[/]',       constant('NAMESPACE') . '\Api\HMIUsers:findOne');
  $this->get('/users/{id}[/]',                constant('NAMESPACE') . '\Api\HMIUsers:findOneById');
  $this->get('/users[/]',                     constant('NAMESPACE') . '\Api\HMIUsers:findAll');

  $this->delete('/pages/delete/{id}[/]',        constant('NAMESPACE') . '\Api\HMIPages:delete');
  $this->post('/pages/update/{id}[/]',          constant('NAMESPACE') . '\Api\HMIPages:update');
  $this->post('/pages/create[/]',               constant('NAMESPACE') . '\Api\HMIPages:create');  
  $this->get('/pages/{id}[/]',                  constant('NAMESPACE') . '\Api\HMIPages:findOneById');
  $this->get('/pages[/]',                       constant('NAMESPACE') . '\Api\HMIPages:findAll');

  $this->delete('/posts/delete/{id}[/]',        constant('NAMESPACE') . '\Api\HMIPosts:delete');
  $this->post('/posts/update/{id}[/]',          constant('NAMESPACE') . '\Api\HMIPosts:update');
  $this->post('/posts/create[/]',               constant('NAMESPACE') . '\Api\HMIPosts:create');  
  $this->get('/posts/{id}[/]',                  constant('NAMESPACE') . '\Api\HMIPosts:findOneById');
  $this->get('/posts[/]',                       constant('NAMESPACE') . '\Api\HMIPosts:findAll');

  $this->delete('/events/delete/{id}[/]',        constant('NAMESPACE') . '\Api\HMIEvents:delete');
  $this->post('/events/update/{id}[/]',          constant('NAMESPACE') . '\Api\HMIEvents:update');
  $this->post('/events/create[/]',               constant('NAMESPACE') . '\Api\HMIEvents:create');  
  $this->get('/events/{id}[/]',                  constant('NAMESPACE') . '\Api\HMIEvents:findOneById');
  $this->get('/events[/]',                       constant('NAMESPACE') . '\Api\HMIEvents:findAll');

  $this->get('/event-subscriptions/event/{id}[/]',        constant('NAMESPACE') . '\Api\HMIEventSubscriptions:findByEventId');
  $this->delete('/event-subscriptions/delete/{id}[/]',        constant('NAMESPACE') . '\Api\HMIEventSubscriptions:delete');
  $this->post('/event-subscriptions/update/{id}[/]',          constant('NAMESPACE') . '\Api\HMIEventSubscriptions:update');
  $this->post('/event-subscriptions/create[/]',               constant('NAMESPACE') . '\Api\HMIEventSubscriptions:create');  
  $this->get('/event-subscriptions/{id}[/]',                  constant('NAMESPACE') . '\Api\HMIEventSubscriptions:findOneById');
  $this->get('/event-subscriptions[/]',                       constant('NAMESPACE') . '\Api\HMIEventSubscriptions:findAll');

  $this->delete('/components/delete/{id}[/]',   constant('NAMESPACE') . '\Api\HMIComponents:delete');
  $this->post('/components/update/{id}[/]',     constant('NAMESPACE') . '\Api\HMIComponents:update');
  $this->post('/components/create[/]',          constant('NAMESPACE') . '\Api\HMIComponents:create');  
  $this->get('/components/{id}[/]',             constant('NAMESPACE') . '\Api\HMIComponents:findOneById');
  $this->get('/components[/]',                  constant('NAMESPACE') . '\Api\HMIComponents:findAll');

  $this->get('/content/component/{id}[/]',   constant('NAMESPACE') . '\Api\HMIContent:findByComponent');
  $this->delete('/content/delete/{id}[/]',   constant('NAMESPACE') . '\Api\HMIContent:delete');
  $this->post('/content/update/{id}[/]',     constant('NAMESPACE') . '\Api\HMIContent:update');
  $this->post('/content/create[/]',          constant('NAMESPACE') . '\Api\HMIContent:create');  
  $this->get('/content/{id}[/]',             constant('NAMESPACE') . '\Api\HMIContent:findOneById');
  $this->get('/content[/]',                  constant('NAMESPACE') . '\Api\HMIContent:findAll');
});

$net->group('/api/v1', function () {
  $this->delete('/users/delete/{id}[/]',      constant('NAMESPACE') . '\Api\Users:delete');
  $this->post('/users/update/{id}[/]',        constant('NAMESPACE') . '\Api\Users:update');
  $this->post('/users/create[/]',             constant('NAMESPACE') . '\Api\Users:create');
  $this->get('/users/email/{email}[/]',       constant('NAMESPACE') . '\Api\Users:findOne');
  $this->get('/users/{id}[/]',                constant('NAMESPACE') . '\Api\Users:findOneById');
  $this->get('/users[/]',                     constant('NAMESPACE') . '\Api\Users:findAll');

  $this->delete('/config/delete/{id}[/]',     constant('NAMESPACE') . '\Api\Config:delete');
  $this->post('/config/update/{id}[/]',       constant('NAMESPACE') . '\Api\Config:update');
  $this->post('/config/create[/]',            constant('NAMESPACE') . '\Api\Config:create');  
  $this->get('/config/{id}[/]',               constant('NAMESPACE') . '\Api\Config:findOneById');
  $this->get('/config[/]',                    constant('NAMESPACE') . '\Api\Config:findAll');

  $this->delete('/attributes/delete/{id}[/]', constant('NAMESPACE') . '\Api\Attributes:delete');
  $this->post('/attributes/update/{id}[/]',   constant('NAMESPACE') . '\Api\Attributes:update');
  $this->post('/attributes/create[/]',        constant('NAMESPACE') . '\Api\Attributes:create');  
  $this->get('/attributes/{id}[/]',           constant('NAMESPACE') . '\Api\Attributes:findOneById');
  $this->get('/attributes[/]',                constant('NAMESPACE') . '\Api\Attributes:findAll');

  $this->delete('/fields/delete/{id}[/]',     constant('NAMESPACE') . '\Api\Fields:delete');
  $this->post('/fields/update/{id}[/]',       constant('NAMESPACE') . '\Api\Fields:update');
  $this->post('/fields/create[/]',            constant('NAMESPACE') . '\Api\Fields:create');  
  $this->get('/fields/{id}[/]',               constant('NAMESPACE') . '\Api\Fields:findOneById');
  $this->get('/fields[/]',                    constant('NAMESPACE') . '\Api\Fields:findAll');

  $this->delete('/collections/delete/{id}[/]',  constant('NAMESPACE') . '\Api\Collections:delete');
  $this->post('/collections/update/{id}[/]',    constant('NAMESPACE') . '\Api\Collections:update');
  $this->post('/collections/create[/]',         constant('NAMESPACE') . '\Api\Collections:create'); 
  $this->get('/collections/{id}[/]',            constant('NAMESPACE') . '\Api\Collections:findOneById');
  $this->get('/collections[/]',                 constant('NAMESPACE') . '\Api\Collections:findAll');

  $this->delete('/content/delete/{id}[/]',      constant('NAMESPACE') . '\Api\Content:delete');
  $this->post('/content/update/{id}[/]',        constant('NAMESPACE') . '\Api\Content:update');
  $this->post('/content/create[/]',             constant('NAMESPACE') . '\Api\Content:create'); 
  $this->get('/content/{id}[/]',                constant('NAMESPACE') . '\Api\Content:findOneById');
  $this->get('/content[/]',                     constant('NAMESPACE') . '\Api\Content:findAll');

  $this->delete('/pages/delete/{id}[/]',        constant('NAMESPACE') . '\Api\Page:delete');
  $this->post('/pages/update/{id}[/]',          constant('NAMESPACE') . '\Api\Page:update');
  $this->post('/pages/create[/]',               constant('NAMESPACE') . '\Api\Page:create');  
  $this->get('/pages/{id}[/]',                  constant('NAMESPACE') . '\Api\Page:findOneById');
  $this->get('/pages[/]',                       constant('NAMESPACE') . '\Api\Page:findAll');
});

$net->post('/api/v1/token', function (Request $request, Response $response, $args) {
  $data = $request->getParsedBody();
  $email = $data['email'];
  $password = $data['password'];
  $factory = Model::factory('HMIUser');

  if ($user = $factory->where('email', $email)->findOne()) {
    if ($user->password == hash('sha256', $password)) {
      $token = JWT::generate('RSA', 'SHA256', PRIVATE_KEY, [
        'id' => $user->id,
        'first_name' => $user->first_name,
        'last_name' => $user->last_name,
        'email' => $user->email,
        'picture' => $user->picture,
        'role' => $user->role,
      ])->__toString();
      $json = json_encode(array('token' => $token));
      $response->getBody()->write($json);
    } else {
      $json = json_encode(array(
        "error" => [
          "code" => 1001,
          "message" => "Invalid password."
        ]
      ));

      $response = $response->withStatus(401);
      $response->getBody()->write($json);
    }
  } else {
    $json = json_encode(array(
      "error" => [
        "code" => 1002,
        "message" => "Email not found."
      ]
    ));

    $response = $response->withStatus(401);
    $response->getBody()->write($json);
  }

  return $response;
});

$net->post('/api/v1/token/renew', function (Request $request, Response $response, $args) {
  $data = $request->getParsedBody();
  $token = $data['token'];

  $isValid = JWT::verify('RSA', 'SHA256', PUBLIC_KEY, $token);
  $factory = Model::factory('HMIUser');

  if ($isValid) {
    if ($user = $factory->where('id', $isValid->getClaim('id'))->findOne()) {
      $newToken = JWT::generate('RSA', 'SHA256', PRIVATE_KEY, [
        'id' => $user->id,
        'first_name' => $user->first_name,
        'last_name' => $user->last_name,
        'email' => $user->email,
        'picture' => $user->picture,
        'role' => $user->role,
      ])->__toString();
      $json = json_encode(array('token' => $token));
      $response->getBody()->write($json);
    } else {
      $json = json_encode(array(
        "error" => [
          "code" => 1003,
          "message" => "Invalid token provided."
        ]
      ));
    }
  } else {
    $json = json_encode(array(
      "error" => [
        "code" => 1003,
        "message" => "Invalid token provided."
      ]
    ));

    $response = $response->withStatus(401);
    $response->getBody()->write($json);
  }

  return $response;
});

$net->post('/api/v1/confirm/password/{id}', function (Request $request, Response $response, $args) {
  $id = $args['id'];

  $data = $request->getParsedBody();
  $password = $data['password'];

  $user = Model::factory('HMIUser')->where('id', $id)->findOne();

  if ($user->password == hash('sha256', $password)) {
    $response = $response->withStatus(200);
  } else {
    $json = json_encode(array(
      "error" => [
        "code" => 1001,
        "message" => "Invalid password."
      ]
    ));
    $response->getBody()->write($json);
  }

  return $response;
});


/* ****************************** */
/* ******* Control routes ********* */
/* ****************************** */

$net->get('/control[/{path:.*}]', function (Request $request, Response $response, $args) { 
  return $this->internal->render($response, 'index', [
    "config" => $this->store->get('config'),
  ]);
});


/* ****************************** */
/* ******* Theme routes ********* */
/* ****************************** */

$manifest = Theme::getManifest($container->store);

foreach ($manifest->routes as $route) {
  $net->get($route->pattern, function (Request $request, Response $response, $args) use ($route) {
    return $this->theme->render($response, $route->layout, ($route->data) ? $route->data : []);
  });
}

/*
$net->get('/', function (Request $request, Response $response, $args) {
  return $this->theme->render($response, 'index');
});
*/


/* ****************************** */
/* ********* DEV ROUTES ********* */
/* ****************************** */

$net->get('/dev/pwd', function (Request $request, Response $response, $args) {  
  $params = $request->getParams();
  $password = $params['password'];
  $hash = hash('sha256', $password);

  $response->getBody()->write($hash);
  return $response;
});

$net->get('/dev/id', function (Request $request, Response $response, $args) {
  $response->getBody()->write(IdGenerator::uniqueId(8));
  return $response;
});


/* ****************************** */
/* ******* CUSTOM ROUTES ******** */
/* ****************************** */

require_once('Routes.php');

$net->run();
?>
