<?php
namespace Rise;

use \PDO;
use \ORM;
use \Memcached;
use Rise\Model;
use Rise\Socket;

class Container extends \Slim\Container
{
	private $config;
	private $renderer;

	public function __construct(array $values = [])
	{
		parent::__construct(array('settings' => $values['netcore']));

		$this->setConfig($values);
		$this->setMemcached();
		$this->initializeDatabase();
		$this->setStore();
		$this->setSocket();
		$this->setRenderer();
		$this->setAliases();
		//s$this->setFacebook();
	}

	private function setConfig(array $values = [])
	{
		$this->config = $values;
	}

	public function setMemcached()
	{
		$this->offsetSet('mc', function () {
			$mc = new Memcached(); 

			$host = $this->config['memcached']['host'];
			$port = $this->config['memcached']['port'];

			$mc->addServer($host, $port);

			return $mc;
		});
	}

	public function initializeDatabase()
	{
		ORM::configure("{$this->config['db']['driver']}:host={$this->config['db']['host']};dbname={$this->config['db']['database']}");
		ORM::configure('username', $this->config['db']['username']);
		ORM::configure('password', $this->config['db']['password']);
		ORM::configure('id_column_overrides', array(
			'likes' => 'user_id'
		));
		ORM::configure('return_result_sets', true);
		ORM::configure('driver_options', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
		Model::$auto_prefix_models = 'Rise\\Models\\';
		Model::$short_table_names = true;
	}

	public function setStore()
	{
		$this->offsetSet('store', function () {
			return new Store();
		});
	}

	public function setRenderer()
	{
		$store = $this->get('store');
		$socket = $this->get('socket');

		$this->renderer = new Renderer($this->config['renderer'], $store, $socket);
	}

	public function setAliases()
	{
		$this->offsetSet('internal', function () {
			return $this->renderer->internal();
		});

		$this->offsetSet('theme', function () {
			return $this->renderer->theme();
		});
	}

	public function setFacebook()
	{
		$this->offsetSet('fb', function () {
			$fb = new \Facebook\Facebook([
				'app_id' => '185517835598495',
				'app_secret' => '26ff3bece198abb9b65a67025d231de4',
				'default_graph_version' => 'v2.10',
			]);

			return $fb;
		});
	}

	public function setSocket()
	{
		$this->offsetSet('socket', function () {
			return new Socket($this->config['netcore']);
		});
	}
}
?>
