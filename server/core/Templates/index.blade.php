@extends('layouts.default')

@section('title', 'Painel | Rise')
@section('description', '')
@section('css')
	@parent
@endsection
@section('content')
	@parent
	<div id="root"></div>
@endsection
@section('scripts')
	@parent
	<script type="text/javascript" src="{{$config['siteUrl']}}/rise/vendors.app.bundle.js"></script>
	<script type="text/javascript" src="{{$config['siteUrl']}}/rise/app.bundle.js"></script>
@endsection