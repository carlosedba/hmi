<?php
namespace Rise\Api;

use \Psr\Container\ContainerInterface as Container;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Upload;
use Rise\Utils\IdGenerator;

class HMIContent
{
  protected $container;

  public function __construct(Container $container) {
    $this->container = $container;
  }

	public function findAll(Request $request, Response $response, $args)
	{
		$content = Model::factory('HMIContent')->findMany();
		$items = array();
		
		for ($i = 0; $i < count($content); ++$i) {
			array_push($items, array_merge($content[$i]->asArray(), [
				'data' => json_decode($content[$i]->data),
				'component' => $content[$i]->hmiComponent()->findOne()->asArray()
			]));
		}

		$json = json_encode($items);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public function findByComponent(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$content = Model::factory('HMIContent')->where('hmi_component_id', $id)->findMany();
		$items = array();
		
		for ($i = 0; $i < count($content); ++$i) {
			array_push($items, array_merge($content[$i]->asArray(), [
				'data' => json_decode($content[$i]->data),
				'component' => $content[$i]->hmiComponent()->findOne()->asArray()
			]));
		}

		$json = json_encode($items);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$content = Model::factory('HMIContent')->where('id', $id)->findOne();

		if ($content) {
			$content = $content->asArray();
			//$content['data'] = str_replace("\\", "", $content['data']);
			$content['data'] = json_decode($content['data']);
			$json = json_encode($content);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
	
	public function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$files = $request->getUploadedFiles();

		if (!empty($files['icon'])) {
			$data['icon'] = Upload::saveTo(CONTENT_DATA_PATH, $files['icon']);
		}

		if (!empty($files['picture'])) {
			$data['picture'] = Upload::saveTo(CONTENT_DATA_PATH, $files['picture']);
		}

		$data['data'] = json_encode(array_merge(
			(array) json_decode($data['data']),
			["icon" => $data['icon'], "picture" => $data['picture']]
		));

		$content = Model::factory('HMIContent')->create(array(
			'id' 								=> IdGenerator::uniqueId(8),
			'hmi_component_id' 	=> $data['hmi_component_id'],
			'data' 							=> $data['data'],
		));

		if ($content->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function update(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$files = $request->getUploadedFiles();
		$id = $args['id'];

		if (!empty($files['icon'])) {
			$data['icon'] = Upload::saveTo(CONTENT_DATA_PATH, $files['icon']);
		}

		if (!empty($files['picture'])) {
			$data['picture'] = Upload::saveTo(CONTENT_DATA_PATH, $files['picture']);
		}

		$content = Model::factory('HMIContent')->where('id', $id)->findOne();

		if ($content) {

			if (json_decode($content->data)) {
				$content->fillAttributes([
					"data" => json_encode(array_merge(
						(array) json_decode($content->data),
						(array) json_decode($data['data']),
						["picture" => $data['picture']],
						["icon" => $data['icon']]
					))
				]);
			} else {
				$content->fillAttributes([
					"data" => json_encode(array_merge(
						(array) json_decode($data['data']),
						["picture" => $data['picture']],
						["icon" => $data['icon']]
					))
				]);
			}
			
			if ($content->save()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
			
		} else {
			$response = $response->withStatus(400);
		}
				
		return $response;
	}

	public function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$content = Model::factory('HMIContent')->where('id', $id)->findOne();

		if ($content) {
			if ($content->delete()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
