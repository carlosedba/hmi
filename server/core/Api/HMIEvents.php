<?php
namespace Rise\Api;

use \Psr\Container\ContainerInterface as Container;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Ausi\SlugGenerator\SlugGenerator;
use Rise\Model;
use Rise\Upload;
use Rise\Utils\IdGenerator;

class HMIEvents
{
  protected $container;

  public function __construct(Container $container) {
    $this->container = $container;
  }

	public function findAll(Request $request, Response $response, $args)
	{
		$events = Model::factory('HMIEvent')->findMany();
		$items = array();
		
		for ($i = 0; $i < count($events); ++$i) {
			array_push($items, array_merge($events[$i]->asArray(), [
				//'cover' => json_decode($events[$i]->cover),
				'user' => $events[$i]->hmiUser()->findOne()->asArray()
			]));
		}

		$json = json_encode($items);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$event = Model::factory('HMIEvent')->where('id', $id)->findOne();

		if ($event) {
			$event = array_merge($event->asArray(), [
				//'cover' => json_decode($event->cover),
				'user' => $event->hmiUser()->findOne()->asArray()
			]);
			$json = json_encode($event);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function findLatestBySlug(Request $request, Response $response, $args)
	{
		$slug = $args['slug'];
		$event = Model::factory('HMIEvent')->where('slug', $slug)->orderByDesc('updated_at')->findOne();

		if ($event) {
			$event = array_merge($event->asArray(), [
				//'cover' => json_decode($event->cover),
				'user' => $event->hmiUser()->findOne()->asArray()
			]);
			$json = json_encode($event);
			$response->getBody()->write($json);
			$response = $response->withAddedHeader('Content-Type','application/json');
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
	
	public function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$files = $request->getUploadedFiles();
		$slug = new SlugGenerator;

		if (!empty($files['cover'])) {
			$data['cover'] = json_encode(Upload::saveTo(CONTENT_DATA_PATH, $files['cover']));
		}

		$event = Model::factory('HMIEvent')->create(array(
			'id' 						=> IdGenerator::uniqueId(8),
			'hmi_user_id' 	=> $data['hmi_user_id'],
			'title' 				=> $data['title'],
			'description' 	=> $data['description'],
			'slug' 					=> $slug->generate($data['title']),
			'location' 			=> $data['location'],
			'date' 					=> $data['date'],
			'start_time' 		=> $data['start_time'],
			'end_time' 			=> $data['end_time'],
			'content' 			=> $data['content'],
			'cover' 				=> $data['cover'],
			'url' 					=> $data['url'],
			'created_at' 		=> date('Y-m-d H:i:s'),
			'updated_at' 		=> date('Y-m-d H:i:s'),
		));

		if ($event->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function update(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$data = $request->getParsedBody();
		$files = $request->getUploadedFiles();
		$slug = new SlugGenerator;

		if (!empty($files['cover'])) {
			$data['cover'] = json_encode(Upload::saveTo(CONTENT_DATA_PATH, $files['cover']));
		}

		$event = Model::factory('HMIEvent')->where('id', $id)->findOne();

		if ($event) {
			$event->fillAttributes(array_merge($data, [
				"slug" => $slug->generate($data['title']),
				"updated_at" => date('Y-m-d H:i:s')
			]));

			if ($event->save()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}
				
		return $response;
	}

	public function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$event = Model::factory('HMIEvent')->where('id', $id)->findOne();

		if ($event) {
			if ($event->delete()) {
				$response = $response->withStatus(201);
			} else {
				$response = $response->withStatus(400);
			}
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
