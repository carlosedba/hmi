<?php

namespace Rise;

class Renderer
{
	private $config;
	private $store;
	private $socket;
	private $internalRenderer;
	private $themeRenderer;

	public function __construct(array $config = [], Store $store, Socket $socket)
	{
		$this->config = $config;
		$this->store = $store;
		$this->socket = $socket;

		$this->setInternalRenderer();
		$this->setThemeRenderer();
		$this->applyDirectives();
	}

	public function internal()
	{
		return $this->internalRenderer;
	}

	public function theme()
	{
		return $this->themeRenderer;
	}

	public function setInternalRenderer()
	{
		$templates = $this->config['internal']['templates'];
		$cache = $this->config['internal']['cache'];

		$this->internalRenderer = new Blade($templates, $cache);
	}

	public function setThemeRenderer()
	{
		$theme = $this->store->get('config.theme');
		$templates = $this->config['theme']['templates'] . $theme;
		$cache = $this->config['theme']['cache'];

		$this->themeRenderer = new Blade($templates, $cache);
	}

	private function applyDirectives()
	{
		$store = $this->store;

		$this->themeRenderer->directive('set', function ($expression) {
			list($variable, $value) = explode(', ', str_replace(['(', ')'], '', $expression));
			return "<?php {$variable} = {$value}; ?>";
		});

		$this->themeRenderer->directive('store', function ($expression) use ($store) {
			return $store->get($expression);
		});

		$this->themeRenderer->directive('route', function ($expression) use ($store) {
			$siteUrl = $store->get('config.siteUrl');

			return "<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('$siteUrl' . $expression)); ?>";
		});

		$this->themeRenderer->directive('asset', function ($expression) use ($store) {
			$siteUrl = $store->get('config.siteUrl');
			$theme = $store->get('config.theme');

			return "<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('$siteUrl' . '/themes/' . '$theme' . '/' . $expression)); ?>";
		});

		$this->themeRenderer->directive('userData', function ($expression) use ($store) {
			$siteUrl = $store->get('config.siteUrl');

			return "<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('$siteUrl' . '/data/user_data/' . $expression)); ?>";
		});

		$this->themeRenderer->directive('contentData', function ($expression) use ($store) {
			$siteUrl = $store->get('config.siteUrl');

			return "<?php echo GuzzleHttp\Psr7\UriNormalizer::normalize(new GuzzleHttp\Psr7\Uri('$siteUrl' . '/data/content_data/' . $expression)); ?>";
		});
	}
}
?>
