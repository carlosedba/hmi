<?php
namespace Rise\Models;

use Rise\Model;

class HMIPost extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_hmi_posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'hmi_user_id', 'title', 'description', 'slug', 'tags', 'categories', 'content', 'cover', 'is_draft', 'created_at', 'updated_at',
    ];

    public function hmiUser()
    {
        return $this->belongsTo('HMIUser', 'hmi_user_id');
    }
}
?>