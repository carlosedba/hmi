<?php
namespace Rise\Models;

use Rise\Model;

class HMIUser extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_hmi_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'first_name', 'last_name', 'email', 'password', 'picture', 'role'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
?>