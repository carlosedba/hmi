<?php
namespace Rise\Models;

use Rise\Model;

class HMIComponent extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_hmi_components';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'data',
    ];
}
?>