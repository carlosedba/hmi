<?php
namespace Rise\Models;

use Rise\Model;

class HMIEventSubscription extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_hmi_event_subscriptions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'hmi_event_id', 'name', 'email',
    ];

    public function hmiEvent()
    {
        return $this->belongsTo('HMIEvent', 'hmi_event_id');
    }
}
?>