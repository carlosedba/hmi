<?php
namespace Rise\Models;

use Rise\Model;

class HMIEvent extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_hmi_events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'hmi_user_id', 'title', 'description', 'slug', 'location', 'date', 'start_time', 'end_time', 'content', 'cover', 'url', 'created_at', 'updated_at',
    ];

    public function hmiUser()
    {
        return $this->belongsTo('HMIUser', 'hmi_user_id');
    }
}
?>