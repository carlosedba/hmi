<?php
namespace Rise\Models;

use Rise\Model;

class HMIContent extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_hmi_content';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'hmi_component_id', 'data',
    ];

    public function hmiComponent()
    {
        return $this->belongsTo('HMIComponent', 'hmi_component_id');
    }
}
?>