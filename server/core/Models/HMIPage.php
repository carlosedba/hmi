<?php
namespace Rise\Models;

use Rise\Model;

class HMIPage extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_hmi_pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'description', 'slug',
    ];
}
?>