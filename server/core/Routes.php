<?php
namespace Rise;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use Rise\Utils\IdGenerator;
use Rise\Model;

function localizeMonth($str) {
  switch ($str) {
    case 'January':
      return 'Janeiro';
      break;

    case 'February':
      return 'Fevereiro';
      break;
      
    case 'March':
      return 'Março';
      break;
      
    case 'April':
      return 'Abril';
      break;
      
    case 'May':
      return 'Maio';
      break;
      
    case 'June':
      return 'Junho';
      break;
      
    case 'July':
      return 'Julho';
      break;
      
    case 'August':
      return 'Agosto';
      break;
      
    case 'September':
      return 'Setembro';
      break;
      
    case 'October':
      return 'Outubro';
      break;
      
    case 'November':
      return 'Novembro';
      break;
      
    case 'December':
      return 'Dezembro';
      break;
  }
}

function getConfig() {
  return Model::factory('Config')->findArray();
}

function getHeroHome() {
  $content = Model::factory('HMIContent')->where('hmi_component_id', '1')->findMany();
  $items = array();
  
  for ($i = 0; $i < count($content); ++$i) {
    array_push($items, array_merge($content[$i]->asArray(), [
      'data' => (array) json_decode($content[$i]->data),
      'component' => $content[$i]->hmiComponent()->findOne()->asArray()
    ]));
  }

  return $items[0];
}

function getQuemSomosHome() {
  $content = Model::factory('HMIContent')->where('hmi_component_id', '2')->findMany();
  $items = array();
  
  for ($i = 0; $i < count($content); ++$i) {
    array_push($items, array_merge($content[$i]->asArray(), [
      'data' => (array) json_decode($content[$i]->data),
      'component' => $content[$i]->hmiComponent()->findOne()->asArray()
    ]));
  }

  return $items[0];
}

function getOQueOferecemosHome() {
  $content = Model::factory('HMIContent')->where('hmi_component_id', '3')->findMany();
  $items = array();
  
  for ($i = 0; $i < count($content); ++$i) {
    array_push($items, array_merge($content[$i]->asArray(), [
      'data' => (array) json_decode($content[$i]->data),
      'component' => $content[$i]->hmiComponent()->findOne()->asArray()
    ]));
  }

  return $items;
}

function getNoticiasHome() {
  $posts = Model::factory('HMIPost')->orderByDesc('created_at')->limit(3)->findMany();
  $items = array();
  
  for ($i = 0; $i < count($posts); ++$i) {
    array_push($items, array_merge($posts[$i]->asArray(), [
      'cover' => (array) json_decode($posts[$i]->cover),
    ]));
  }

  return $items;
}

function getEventosHome() {
  $events = Model::factory('HMIEvent')->orderByDesc('date')->limit(4)->findMany();
  $items = array();
  
  for ($i = 0; $i < count($events); ++$i) {
    $day = formatDateTime($events[$i]->date, 'Y-m-d', 'd');
    $month = formatDateTime($events[$i]->date, 'Y-m-d', 'F');

    $month = strtolower(localizeMonth($month));

    array_push($items, array_merge($events[$i]->asArray(), [
      'date' => "{$day} <span>{$month}</span>",
    ]));
  }

  return $items;
}

function getCasesHome() {
  $content = Model::factory('HMIContent')->where('hmi_component_id', '4')->findMany();
  $items = array();
  
  for ($i = 0; $i < count($content); ++$i) {
    array_push($items, array_merge($content[$i]->asArray(), [
      'data' => (array) json_decode($content[$i]->data),
      'component' => $content[$i]->hmiComponent()->findOne()->asArray()
    ]));
  }

  return $items;
}

function getSliderHome() {
  $content = Model::factory('HMIContent')->where('hmi_component_id', '5')->findMany();
  $items = array();
  
  for ($i = 0; $i < count($content); ++$i) {
    array_push($items, array_merge($content[$i]->asArray(), [
      'data' => (array) json_decode($content[$i]->data),
      'component' => $content[$i]->hmiComponent()->findOne()->asArray()
    ]));
  }

  return $items;
}

function getEventos() {
  $events = Model::factory('HMIEvent')->orderByDesc('date')->findMany();
  $items = array();
  
  for ($i = 0; $i < count($events); ++$i) {
    $day = formatDateTime($events[$i]->date, 'Y-m-d', 'd');
    $month = formatDateTime($events[$i]->date, 'Y-m-d', 'F');

    $month = strtolower(localizeMonth($month));

    array_push($items, array_merge($events[$i]->asArray(), [
      'date' => "{$day} <span>{$month}</span>",
      'cover' => (array) json_decode($events[$i]->cover),
      'user' => $events[$i]->hmiUser()->findOne()->asArray()
    ]));
  }

  return $items;
}

function getNoticias() {
  $posts = Model::factory('HMIPost')->orderByDesc('created_at')->findMany();
  $items = array();
  
  for ($i = 0; $i < count($posts); ++$i) {
    array_push($items, array_merge($posts[$i]->asArray(), [
      'cover' => (array) json_decode($posts[$i]->cover),
      'user' => $posts[$i]->hmiUser()->findOne()->asArray()
    ]));
  }

  return $items;
}

function getNoticiaBySlug($slug) {
  $post = Model::factory('HMIPost')->where('slug', $slug)->findOne();

  if ($post) {
    //setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    //date_default_timezone_set('America/Sao_Paulo');
    //var_dump(strftime("%e de %B de %Y", strtotime($post->created_at)));
    $created_at = new \DateTime($post->created_at);

    $day = $created_at->format('j');
    $month = $created_at->format('F');
    $year = $created_at->format('Y');

    $month = strtolower(localizeMonth($month));

    $post = array_merge($post->asArray(), [
      'created_at' => "{$day} de {$month} de {$year}",
      'cover' => (array) json_decode($post->cover),
      'user' => $post->hmiUser()->findOne()->asArray()
    ]);
  }

  return $post;
}

function getContentById($id) {
  $content = Model::factory('HMIContent')->where('id', $id)->findOne();

  if ($content) {
    $content = array_merge($content->asArray(), [
      'data' => (array) json_decode($content->data),
    ]);
  }

  return $content;
}

function getContentByComponentId($componentId) {
  $content = Model::factory('HMIContent')->where('hmi_component_id', $componentId)->findMany();
  $items = array();
  
  for ($i = 0; $i < count($content); ++$i) {
    array_push($items, array_merge($content[$i]->asArray(), [
      'data' => (array) json_decode($content[$i]->data),
    ]));
  }

  return $items;
}

function formatDateTime($str, $currentFormat, $newFormat) {
  $datetime = \DateTime::createFromFormat($currentFormat, $str);
  $newDateTimeString = $datetime->format($newFormat);

  return $newDateTimeString;
}

$net->get('/', function (Request $request, Response $response, $args) {
  return $this->theme->render($response, 'index', [
    "siteUrl" => $this->store->get('config.siteUrl'),
    "hero" => getHeroHome(),
    "quemSomos" => getQuemSomosHome(),
    "oQueOferecemos" => getOQueOferecemosHome(),
    "noticias" => getNoticiasHome(),
    "events" => getEventosHome(),
    "cases" => getCasesHome(),
    "slider" => getSliderHome(),
  ]);
});

$net->get('/quem-somos[/]', function (Request $request, Response $response, $args) {
  return $this->theme->render($response, 'quem-somos', [
    "texto" => getContentById('0649c90d'),
    "boxLaranja1" => getContentById('0649c30d'),
    "boxLaranja2" => getContentById('0649c31d'),
    "boxRoxo1" => getContentById('67ab55ae'),
    "boxRoxo2" => getContentById('67ab53ce'),
    "boxRoxo3" => getContentById('33ab51be'),
    "equipe" => getContentByComponentId('13'),
  ]);
});

$net->get('/o-que-oferecemos[/]', function (Request $request, Response $response, $args) {
  return $this->theme->render($response, 'o-que-oferecemos', [
    "conteudo" => getContentById('0657c90d')
  ]);
});

$net->get('/eventos[/]', function (Request $request, Response $response, $args) {
  return $this->theme->render($response, 'eventos', [
    "events" => getEventos(),
    "patrocinadores" => getContentByComponentId('30'),
  ]);
});

$net->get('/noticias[/]', function (Request $request, Response $response, $args) {
  return $this->theme->render($response, 'noticias', [
    "noticias" => getNoticias()
  ]);
});

$net->get('/noticias/{slug}[/]', function (Request $request, Response $response, $args) {
  $slug = $args['slug'];

  return $this->theme->render($response, 'noticia', [
    "noticia" => getNoticiaBySlug($slug)
  ]);
});

$net->get('/contato[/]', function (Request $request, Response $response, $args) {
  return $this->theme->render($response, 'contato', []);
});

$net->post('/subscribe[/]', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();

    $eventSubscription = Model::factory('HMIEventSubscription')->create(array(
      'id'            => IdGenerator::uniqueId(8),
      'hmi_event_id'  => $data['hmi_event_id'],
      'name'          => $data['name'],
      'email'         => $data['email'],
    ));

    if ($eventSubscription->save()) {
      $response = $response->withStatus(201);
    } else {
      $response = $response->withStatus(400);
    }

    return $response;
});

$net->post('/contato', function (Request $request, Response $response, $args) {
  $data = $request->getParsedBody();
  $nome = $data['nome'];
  $email = $data['email'];
  $celular = $data['celular'];
  $assunto = $data['assunto'];
  $mensagem = $data['mensagem'];

  if (!empty($nome) && !empty($email) && !empty($assunto) && !empty($mensagem)) {
    $mail = new \PHPMailer;

    $mail->isSMTP();
    $mail->Host = 'mail.hmi.net.br';
    $mail->SMTPAuth = true;
    $mail->Username = 'contato@hmi.net.br';
    $mail->Password = 'EQJW7qO1_YVD';
    $mail->SMTPSecure = 'ssl';
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    $mail->SMTPDebug = 3;
    $mail->Port = 465;

    $mail->setFrom('contato@hmi.net.br', 'Contato - HMI');
    $mail->addAddress('contato@hmi.net.br', 'Contato - HMI');

    $mail->CharSet = 'UTF-8';
    $mail->isHTML(true);

    $mail->Subject = 'Contato - HMI';
    $mail->Body    = "<b>{$nome} preencheu o formulário.<br>Dados abaixo:</b><br><br><ul><li><b>Nome:</b> {$nome}</li><li><b>E-mail:</b> {$email}</li><li><b>Celular:</b> {$celular}</li><li><b>Assunto:</b> {$assunto}</li><li><b>Mensagem:</b> {$mensagem}</li></ul>";

    if ($mail->send()) {
      $response = $response->withStatus(201);
    } else {
      echo 'Message could not be sent.';
      echo 'Mailer Error: ' . $mail->ErrorInfo;
      $response = $response->withStatus(400);
    }
  } else {
    $response = $response->withStatus(400);
  }

  return $response;
});

?>

