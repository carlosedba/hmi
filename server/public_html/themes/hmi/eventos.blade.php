@extends('layouts.default')

@section('title')
  Eventos | @store(config.siteName)
@endsection

@section('description')
  @store(config.siteDescription)
@endsection

@section('vendor-css')
  @parent
@endsection

@section('application-css')
  @parent
@endsection

@section('fonts')
  @parent
@endsection

@section('content')
  @parent

  <section class="section section-eight">
    <div class="section-header" style="background-image: url('@asset('resources/img/helloquence-51716-unsplash-x1280.jpg')');">
      <p class="section-title">Eventos</p>
    </div>
    <div class="section-wrapper">
      <div class="content">
        @foreach ($events as $event)
          <div class="component-event event-alpha" data-id="{{$event['id']}}">
            <div class="event-row">
              <div class="left">
                <div class="event-date">{!! $event['date'] !!}</div>
                <div class="event-info">
                  <p class="event-title">{{$event['title']}}</p>
                  <p class="event-text">{{$event['description']}}</p>
                </div>
                <div class="event-info">
                  <p class="event-datetime">Local: {{$event['location']}}</p>
                  <p class="event-datetime">Horário: {{$event['start_time']}} às {{$event['end_time']}}</p>
                </div>
              </div>
              <div class="right">
                <a href="{{$event['url']}}" class="btn btn-event">Inscreva-se</a>
              </div>
            </div>
            <div class="event-row">
              <div class="event-content">{!! $event['content'] !!}</div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>

  <section id="patrocinadores" class="section white section-eleven">
    <div class="row no-pad">
      <div class="row-titles">
        <p class="row-title">Patrocinadores</p>
      </div>
      <div class="content">
        @foreach ($patrocinadores as $patrocinador)
          <div class="component-patrocinador">
            <div class="patrocinador-logo" style="background-image: url('@contentData($patrocinador['data']['picture']->filename)');"></div>
          </div>
        @endforeach
      </div>
    </div>
  </section>

  <div class="modal micromodal-slide" id="event-subscription-modal" aria-hidden="true">
    <div class="modal__overlay" tabindex="-1" data-micromodal-close>
      <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="">
        <header class="modal__header">
          <div class="modal__titles">
            <h2 class="modal__title">
              Formulário de inscrição
            </h2>
            <p class="modal__description">Preencha os campos para inscrever-se no evento: <br><strong class="modal__event-name"></strong></p>
          </div>
          <button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
        </header>
        <main class="modal__content">
          <form method="POST" action="/subscribe">
            <div class="inputs">
              <div class="input w3-q1">
                <label>Nome:</label>
                <input type="text" name="name" required>
              </div>
              <div class="input w3-q1">
                <label>E-mail:</label>
                <input type="email" name="email" required>
              </div>
            </div>
          </form>
        </main>
        <footer class="modal__footer">
          <button class="modal__btn modal__btn-primary">Enviar</button>
        </footer>
      </div>
    </div>
  </div>

@endsection

@section('footer-sections')
  @parent
@endsection

@section('scripts')
  @parent
  <script type="text/javascript" src="@asset('vendor/flickity/flickity.pkgd.min.js')"></script>
  <script type="text/javascript" src="@asset('resources/js/EventSubscription.js')"></script>
  <script type="text/javascript">
    var secPatrocinadores = document.querySelector('#patrocinadores .content')
    var cases = new Flickity(secPatrocinadores, {
      cellAlign: 'center',
      wrapAround: false,
      freeScroll: true,
      contain: true,
      autoPlay: true,
      prevNextButtons: false,
    })
  </script>
@endsection