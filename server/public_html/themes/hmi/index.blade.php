@extends('layouts.default')

@section('title')
  @store(config.siteName)
@endsection

@section('description')
  @store(config.siteDescription)
@endsection

@section('vendor-css')
  @parent
@endsection

@section('application-css')
  @parent
@endsection

@section('fonts')
  @parent
@endsection

@section('content')
  @parent

  <section class="hero">
    <div class="hero-background" style="background-image: url(@contentData($hero['data']['picture']->filename))"></div>
    <div class="hero-overlay"></div>
    <div class="hero-content">
      <div class="hero-content-wrapper">
        <p class="hero-title">{{$hero['data']['title']}}</p>
      </div>
    </div>
  </section>

  <section class="section white section-one">
    <div class="section-wrapper">
      <div class="row no-pad lr">
        <div class="row-titles">
          <p class="row-title">{{$quemSomos['data']['title']}}</p>
          <p class="row-text">{{$quemSomos['data']['description']}}</p>
        </div>
        <div class="box box-one">
          <div class="box-content">
            <p>{{$quemSomos['data']['box']->text}}</p>
          </div>
          <a href="@route('/quem-somos')" class="box-link">Saiba mais &#11208;</a>
        </div>
      </div>
    </div>
  </section>

  <section class="section white section-two">
    <div class="row small-pad no-pad lr bottom">
      <div class="row-titles">
        <p class="row-title">O que oferecemos</p>
      </div>
      <div class="featured-container">
        @foreach ($oQueOferecemos as $content)
          <a class="featured featured-one" href="@route($content['data']['url'])">
            <div class="featured-background" style="background-image: url('@contentData($content['data']['picture']->filename)')"></div>
            <img class="featured-icon" src="@contentData($content['data']['icon']->filename)">
            <p class="featured-title">{{$content['data']['name']}}</p>
            <p class="featured-subtitle">Saiba mais &#11208;</p>
          </a>
        @endforeach
      </div>
    </div>
  </section>

  <section class="section white section-three">
    <div class="section-wrapper">
      <div class="row small-pad no-pad lr">
        <div class="row-titles">
          <p class="row-title">Artigos e Notícias</p>
        </div>
        <div class="content">
        @foreach ($noticias as $post)
          <div class="component-news small">
            <a class="news-picture" href="@route('/noticias/' . $post['slug'])" style="background-image: url('@contentData($post['cover']['filename'])')"></a>
            <p class="news-title">{{$post['title']}}</p>
            <p class="news-description">{{$post['description']}}</p>
            <a href="@route('/noticias/' . $post['slug'])" class="btn">Saiba mais</a>
          </div>
        @endforeach
        </div>
      </div>
    </div>
  </section>

  <section class="section white section-four">
    <div class="section-wrapper">
      <div class="row small-pad no-pad lr">
        <div class="row-titles">
          <p class="row-title">Eventos</p>
        </div>
        <div class="content">
          @foreach ($events as $event)
            <div class="component-event event-alpha" data-id="{{$event['id']}}">
              <div class="event-row">
                <div class="left">
                  <div class="event-date">{!! $event['date'] !!}</div>
                  <div class="event-info">
                    <p class="event-title">{{$event['title']}}</p>
                    <p class="event-text">{{$event['description']}}</p>
                  </div>
                  <div class="event-info">
                    <p class="event-datetime">Local: {{$event['location']}}</p>
                    <p class="event-datetime">Horário: {{$event['start_time']}} às {{$event['end_time']}}</p>
                    <a class="event-link" href="@route('/eventos#' . $event['slug'])">Mais informações</a>
                  </div>
                </div>
                <div class="right">
                  <a href="{{$event['url']}}" class="btn btn-event">Inscreva-se</a>
                </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </section>

  <section id="cases-e-depoimentos" class="section white section-five">
    <div class="row no-pad">
      <div class="row-titles">
        <p class="row-title">Cases e depoimentos</p>
      </div>
      <div class="content">
        @foreach ($cases as $content)
          <a class="component-case" href="{{$content['data']['url']}}">
            <div class="case-background" style="background-image: url('@contentData($content['data']['picture']->filename)');"></div>
            <div class="case-background-overlay"></div>
            <div class="case-info">
              <p class="case-name">{{$content['data']['name']}}</p>
              <p class="case-company">{{$content['data']['description']}}</p>
            </div>
          </a>
        @endforeach
      </div>
    </div>
  </section>

  <section class="section white section-six">
    <div class="row small-pad">
      <div class="content">
        <div class="component-noticias">
          <div class="noticias-items">
          <div class="noticias-cover""></div>
            @foreach ($slider as $content)
              <div class="noticias-item active">
                <a href="#" class="noticias-item-title">{{$content['data']['name']}}</a>
                <p class="noticias-item-description">{{$content['data']['description']}}</p>
                <div class="noticias-item-picture" data-src="@contentData($content['data']['picture']->filename)"></div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@section('footer-sections')
  @parent
@endsection

@section('modal')
  @parent

  <div class="modal micromodal-slide" id="event-subscription-modal" aria-hidden="true">
    <div class="modal__overlay" tabindex="-1" data-micromodal-close>
      <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="">
        <header class="modal__header">
          <div class="modal__titles">
            <h2 class="modal__title">
              Formulário de inscrição
            </h2>
            <p class="modal__description">Preencha os campos para inscrever-se no evento: <br><strong class="modal__event-name"></strong></p>
          </div>
          <button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
        </header>
        <main class="modal__content">
          <form method="POST" action="/subscribe">
            <div class="inputs">
              <div class="input w3-q1">
                <label>Nome:</label>
                <input type="text" name="name" required>
              </div>
              <div class="input w3-q1">
                <label>E-mail:</label>
                <input type="email" name="email" required>
              </div>
            </div>
          </form>
        </main>
        <footer class="modal__footer">
          <button class="modal__btn modal__btn-primary">Enviar</button>
        </footer>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  @parent
  <script type="text/javascript" src="@asset(vendor/flickity/flickity.pkgd.min.js)"></script>
  <script type="text/javascript" src="@asset('resources/js/EventSubscription.js')"></script>
  <script type="text/javascript">
    var secCases = document.querySelector('#cases-e-depoimentos .content')
    var cases = new Flickity(secCases, {
      cellAlign: 'left',
      wrapAround: true,
      freeScroll: true,
      contain: true,
      autoPlay: true,
      prevNextButtons: false,
    })
  </script>
  <script type="text/javascript">
    function setWidgetNoticias() {
      let widgets = document.querySelectorAll('.component-noticias .noticias-items')

      if (widgets.length) {
        ;[].forEach.call(widgets, function (el, i) {
          let loop = setInterval(loopWidgetNoticias, 5000)

          el.addEventListener('click',handleNoticiaClick)
        })
      }
    }

    function loopWidgetNoticias() {
      let noticias = document.querySelectorAll('.component-noticias .noticias-item')

      if (noticias.length) {
        let currentIndex = null
        let currentItem = null
        let nextPicture = null
        let nextItem = null
        let cover = document.querySelector('.noticias-cover')

        ;[].forEach.call(noticias, function (el, i, arr) {
          if (el.classList.contains('active')) {
            currentIndex = i
            currentItem = el
          }
        })

        if (currentIndex < noticias.length - 1) {
          nextItem = noticias[currentIndex+1]
        } else {
          nextItem = noticias[0]
        }

        nextPicture = nextItem.querySelector('.component-noticias .noticias-item-picture').dataset.src

        cover.style.backgroundImage = 'url(' + nextPicture + ')'
        currentItem.classList.remove('active')
        nextItem.classList.add('active')
      }
    }

    function handleNoticiaClick(event) {
      console.log(event.target)
    }

    setWidgetNoticias()
  </script>
@endsection