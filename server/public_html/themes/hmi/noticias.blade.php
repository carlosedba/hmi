@extends('layouts.default')

@section('title')
  Notícias | @store(config.siteName)
@endsection

@section('description')
  @store(config.siteDescription)
@endsection

@section('vendor-css')
  @parent
@endsection

@section('application-css')
  @parent
@endsection

@section('fonts')
  @parent
@endsection

@section('content')
  @parent

  <section class="section section-eight">
    <div class="section-header" style="background-image: url('@asset('resources/img/helloquence-51716-unsplash-x1280.jpg')');">
      <p class="section-title">Notícias</p>
    </div>
    <div class="section-wrapper">
      <div class="content">
        @foreach ($noticias as $post)
          <div class="component-news">
            <a class="news-picture" href="@route('/noticias/' . $post['slug'])" style="background-image: url('@contentData($post['cover']['filename'])')"></a>
            <p class="news-title">{{$post['title']}}</p>
            <p class="news-description">{{$post['description']}}</p>
            <a href="@route('/noticias/' . $post['slug'])" class="btn">Saiba mais</a>
          </div>
        @endforeach
      </div>
    </div>
  </section>

@endsection

@section('footer-sections')
  @parent
@endsection

@section('scripts')
  @parent
  <script type="text/javascript" src="@asset(vendor/flickity/flickity.pkgd.min.js)"></script>
@endsection