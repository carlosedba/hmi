@extends('layouts.default')

@section('title')
  {{$noticia['title']}} | @store(config.siteName)
@endsection

@section('description')
  @store(config.siteDescription)
@endsection

@section('vendor-css')
  @parent
@endsection

@section('application-css')
  @parent
@endsection

@section('fonts')
  @parent
@endsection

@section('content')
  @parent

  <section class="section section-nine">
    <div class="section-header" style="background-image: url('@contentData($noticia['cover']['filename'])');">
      <div class="section-wrapper">
        <p class="section-hat">{{$noticia['created_at']}}</p>
        <p class="section-title">{{$noticia['title']}}</p>
        <p class="section-author">por {{$noticia['user']['first_name']}}</p>
      </div>
    </div>
    <div class="section-wrapper">
      <div class="content">{!! $noticia['content'] !!}</div>
    </div>
  </section>

@endsection

@section('footer-sections')
  @parent
@endsection

@section('scripts')
  @parent
  <script type="text/javascript" src="@asset(vendor/flickity/flickity.pkgd.min.js)"></script>
@endsection