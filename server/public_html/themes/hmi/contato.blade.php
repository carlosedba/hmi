@extends('layouts.default')

@section('title')
  Contato | @store(config.siteName)
@endsection

@section('description')
  @store(config.siteDescription)
@endsection

@section('vendor-css')
  @parent
@endsection

@section('application-css')
  @parent
@endsection

@section('fonts')
  @parent
@endsection

@section('content')
  @parent

  <section class="section section-ten">
    <div class="section-header" style="background-image: url('@asset('resources/img/helloquence-51716-unsplash-x1280.jpg')');">
      <p class="section-title">Contato</p>
    </div>
    <div class="section-wrapper">
      <div class="content">
        <form method="POST" action="@route('/contato')">
          <div class="inputs">
            <div class="input w5-q3">
              <label>Nome:</label>
              <input type="text" name="nome" required>
            </div>
            <div class="input w5-q3">
              <label>E-mail:</label>
              <input type="email" name="email" required>
            </div>
            <div class="input w5-q3">
              <label>Celular:</label>
              <input type="tel" name="celular" required>
            </div>
            <div class="input w5-q3">
              <label>Assunto:</label>
              <input type="text" name="assunto" required>
            </div>
            <div class="input w5-q3">
              <label>Mensagem:</label>
              <textarea name="mensagem" required></textarea>
            </div>
          </div>
          <button class="btn btn-submit">Enviar</button>
        </form>

        <!-- Loading Indicator -->
        <div class="loading load8">
            <div class="loader">Carregando...</div>
        </div>
      </div>
    </div>
  </section>

@endsection

@section('footer-sections')
  @parent
@endsection

@section('scripts')
  @parent
  <script type="text/javascript" src="@asset('resources/js/Loading.js')"></script>
  <script type="text/javascript" src="@asset('resources/js/Form.js')"></script>
@endsection