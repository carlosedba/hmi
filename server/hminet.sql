CREATE TABLE IF NOT EXISTS `rise_hmi_users` (
	`id`						varchar(255)  NOT NULL,
	`first_name`		varchar(255)	NOT NULL,
	`last_name`			varchar(255)	NOT NULL,
	`email`					varchar(255)	NOT NULL,
	`password`			varchar(255)	NOT NULL,
	`picture`				varchar(255),
	`role`					varchar(255),
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_hmi_pages` (	
	`id`						varchar(255)	NOT NULL,
	`name`					varchar(255) 	NOT NULL,
	`description`		varchar(255),
	`slug`					varchar(255),
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_hmi_posts` (	
	`id`						varchar(255)	NOT NULL,
	`hmi_user_id`   varchar(255)	NOT NULL,
	`title`					varchar(255) 	NOT NULL,
	`description`		varchar(255),
	`slug`					varchar(255),
	`tags`					text,
	`categories`		text,
	`content`				longtext,
	`cover`					varchar(255),
	`is_draft`			boolean,
	`created_at`  	datetime			NOT NULL,
	`updated_at` 		datetime			NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`hmi_user_id`) REFERENCES `rise_hmi_users` (`id`) ON DELETE CASCADE
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_hmi_events` (	
	`id`						varchar(255)	NOT NULL,
	`hmi_user_id`   varchar(255)	NOT NULL,
	`title`					varchar(255) 	NOT NULL,
	`description`		varchar(255),
	`slug`					varchar(255),
	`location`			varchar(255),
	`date`  				date					NOT NULL,
	`start_time`  	varchar(255)	NOT NULL,
	`end_time` 			varchar(255)	NOT NULL,
	`content`				longtext,
	`cover`					varchar(255),
	`url`						varchar(255),
	`created_at`  	datetime			NOT NULL,
	`updated_at` 		datetime			NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`hmi_user_id`) REFERENCES `rise_hmi_users` (`id`) ON DELETE CASCADE
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_hmi_event_subscriptions` (	
	`id`						varchar(255)	NOT NULL,
	`hmi_event_id`  varchar(255)	NOT NULL,
	`name`					varchar(255) 	NOT NULL,
	`email`					varchar(255)	NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`hmi_event_id`) REFERENCES `rise_hmi_events` (`id`) ON DELETE CASCADE
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_hmi_components` (	
	`id`						varchar(255)	NOT NULL,
	`name`					varchar(255) 	NOT NULL,
	`alias`					varchar(255),
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_hmi_content` (	
	`id`								varchar(255)	NOT NULL,
	`hmi_component_id`  varchar(255)	NOT NULL,
	`data`							longtext,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`hmi_component_id`) REFERENCES `rise_hmi_components` (`id`) ON DELETE CASCADE
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_config` (
	`id`						int 					NOT NULL 	AUTO_INCREMENT,
	`name`					varchar(255) 	NOT NULL,
	`value`					longtext 			NOT NULL,
	`autoload`			bool 					NOT NULL,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_collections` (	
	`id`					varchar(255)	NOT NULL,
	`name`				varchar(255) 	NOT NULL,
	`description`	varchar(255),
	`slug`				varchar(255) 	NOT NULL,
	`picture`			varchar(255),
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_field_types` (
	`id`					int 			NOT NULL 	AUTO_INCREMENT,
	`name`				varchar(255) 	NOT NULL,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_fields` (
	`id`							varchar(255)	NOT NULL,
	`name`						varchar(255) 	NOT NULL,
	`description`			varchar(255),
	`attribute`				varchar(255) 	NOT NULL,
	`field_type_id`		int 					NOT NULL,
	`collection_id`		varchar(255)	NOT NULL,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`field_type_id`) REFERENCES `rise_field_types` (`id`) ON DELETE CASCADE,
	FOREIGN KEY (`collection_id`) REFERENCES `rise_collections` (`id`) ON DELETE CASCADE
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_content` (
	`id`							varchar(255)	NOT NULL,
	`data`						longtext			NOT NULL,
	`collection_id`		varchar(255) 	NOT NULL,
	FOREIGN KEY (`collection_id`) REFERENCES `rise_collections` (`id`) ON DELETE CASCADE,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_pages` (
	`id`							varchar(255)	NOT NULL,
	`name`						varchar(255)	NOT NULL,
	`slug`						varchar(255)	NOT NULL,
	`collection_id`		varchar(255) 	NOT NULL,
	FOREIGN KEY (`collection_id`) REFERENCES `rise_collections` (`id`) ON DELETE CASCADE,
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rise_users` (
	`id`						varchar(255)  NOT NULL,
	`first_name`		varchar(255)	NOT NULL,
	`last_name`			varchar(255)	NOT NULL,
	`email`					varchar(255)	NOT NULL,
	`password`			varchar(255)	NOT NULL,
	`picture`				varchar(255),
	PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;


INSERT INTO `rise_users` (`id`, `first_name`, `last_name`, `email`, `password`, `picture`) VALUES ('0cbdeb00', 'Carlos Eduardo', 'Barbosa de Almeida', 'carlosedba@outlook.com', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 'eu.jpg');

INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_url', 'http://hmi.net.br', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_home', 'http://hmi.net.br', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_name', 'HMI - Honoris Mérito Institute', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('site_description', '', true);

INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('theme', 'hmi', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('active_addons', '[{"active": true, "name": "rise-pages", }, {"active": true, "name": "rise-posts", }, ]', true);

INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('admin_email', 'carlosedba@outlook.com', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('timezone_string', 'America/Sao_Paulo', true);

INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('mailserver_url', '', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('mailserver_login', '', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('mailserver_pass', '', true);
INSERT INTO `rise_config` (`name`, `value`, `autoload`) VALUES ('mailserver_port', '', true);

INSERT INTO `rise_field_types` (`name`) VALUES ('Texto');
INSERT INTO `rise_field_types` (`name`) VALUES ('Editor de texto');
INSERT INTO `rise_field_types` (`name`) VALUES ('Imagem');
INSERT INTO `rise_field_types` (`name`) VALUES ('Arquivo');
INSERT INTO `rise_field_types` (`name`) VALUES ('Tags');
INSERT INTO `rise_field_types` (`name`) VALUES ('Categoria');
INSERT INTO `rise_field_types` (`name`) VALUES ('Data');
INSERT INTO `rise_field_types` (`name`) VALUES ('Hora');

INSERT INTO `rise_collections` (`id`, `name`, `description`, `slug`, `picture`) VALUES ('0cbdeb00', 'Páginas', '', 'pages', '');
INSERT INTO `rise_collections` (`id`, `name`, `description`, `slug`, `picture`) VALUES ('1cbdeb11', 'Posts', '', 'posts', '');

INSERT INTO `rise_fields` (`id`, `name`, `description`, `attribute`, `field_type_id`, `collection_id`) VALUES ('0cbdeb00', 'Título', '', 'title', '1', '0cbdeb00');
INSERT INTO `rise_fields` (`id`, `name`, `description`, `attribute`, `field_type_id`, `collection_id`) VALUES ('1cbdeb00', 'Conteúdo', '', 'content', '2', '0cbdeb00');

INSERT INTO `rise_fields` (`id`, `name`, `description`, `attribute`, `field_type_id`, `collection_id`) VALUES ('2cbdeb00', 'Título', '', 'title', '1', '0cbdeb00');
INSERT INTO `rise_fields` (`id`, `name`, `description`, `attribute`, `field_type_id`, `collection_id`) VALUES ('3cbdeb00', 'Data', '', 'date', '6', '0cbdeb00');
INSERT INTO `rise_fields` (`id`, `name`, `description`, `attribute`, `field_type_id`, `collection_id`) VALUES ('4cbdeb00', 'Categoria', '', 'category', '1', '0cbdeb00');
INSERT INTO `rise_fields` (`id`, `name`, `description`, `attribute`, `field_type_id`, `collection_id`) VALUES ('5cbdeb00', 'Conteúdo', '', 'content', '2', '0cbdeb00');

/* INSERT INTO `rise_pages` (`id`, `name`, `layout`, `slug`, `collection_id`) VALUES ('0cbdeb00', 'Páginas de conteúdo', 'page', 'pages', '0cbdeb00');
INSERT INTO `rise_pages` (`id`, `name`, `layout`, `slug`, `collection_id`) VALUES ('0cbdeb00', 'Lista de posts', 'posts', 'posts', '1cbdeb11'); */

INSERT INTO `rise_hmi_users` (`id`, `first_name`, `last_name`, `email`, `password`, `picture`, `role`) VALUES ('0cbdeb00', 'Carlos Eduardo', 'Barbosa de Almeida', 'carlosedba@outlook.com', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 'eu.jpg', 'admin');
INSERT INTO `rise_hmi_users` (`id`, `first_name`, `last_name`, `email`, `password`, `picture`, `role`) VALUES ('1cbdeb11', 'Mariciane', 'Gemin', 'mariciane@s7consulting.com.br', '8c8a5aff925e98538a11bb97e5ccd6deb794912ba72d9d8dbac29a168a20028a', 'Mariciane.jpg', 'admin');

INSERT INTO `rise_hmi_components` (`id`, `name`, `alias`) VALUES
('1', 'Hero', 'hero'),
('10', 'Texto - Quem somos', 'texto-quem-somos'),
('11', 'Box laranja - Quem Somos', 'box-laranja-quem-somos'),
('12', 'Box roxo - Quem somos', 'box-roxo-quem-somos'),
('13', 'Equipe - Quem somos', 'equipe-quem-somos'),
('2', 'Quem somos', 'quem-somos'),
('20', 'Conteúdo - O que oferecemos', 'conteudo-o-que-oferecemos'),
('3', 'O que oferecemos', 'o-que-oferecemos'),
('4', 'Cases', 'cases'),
('5', 'Slider', 'slider');

INSERT INTO `rise_hmi_content` (`id`, `hmi_component_id`, `data`) VALUES
('0649c30d', '11', '{\"picture\":null,\"icon\":null,\"text\":\"Nosso prop\\u00f3sito \\u00e9 fomentar a cultura do m\\u00e9rito dentro das empresas.\"}'),
('0649c31d', '11', '{\"picture\":null,\"icon\":null,\"text\":\"Melhores eventos no territ\\u00f3rio nacional.\"}'),
('0649c90d', '10', '{\"content\":\"<p><span style=\\\"color: #5d5d5d; font-family: \'Open Sans\', sans-serif; font-size: 18px;\\\" data-mce-style=\\\"color: #5d5d5d; font-family: \'Open Sans\', sans-serif; font-size: 18px;\\\">O HMI \\u00e9 uma entidade sem fins lucrativos que acredita na cultura meritocr\\u00e1tica dentro das corpora\\u00e7\\u00f5es como estrat\\u00e9gia de crescimento sustent\\u00e1vel atrav\\u00e9s de entregas diferenciadas.&nbsp;<\\/span><br style=\\\"color: #5d5d5d; font-family: \'Open Sans\', sans-serif; font-size: 18px;\\\" data-mce-style=\\\"color: #5d5d5d; font-family: \'Open Sans\', sans-serif; font-size: 18px;\\\"><br style=\\\"color: #5d5d5d; font-family: \'Open Sans\', sans-serif; font-size: 18px;\\\" data-mce-style=\\\"color: #5d5d5d; font-family: \'Open Sans\', sans-serif; font-size: 18px;\\\"><span style=\\\"color: #5d5d5d; font-family: \'Open Sans\', sans-serif; font-size: 18px;\\\" data-mce-style=\\\"color: #5d5d5d; font-family: \'Open Sans\', sans-serif; font-size: 18px;\\\">Assumimos o compromisso de ser um agente de transforma\\u00e7\\u00e3o, estimulando o protagonismo das Pessoas, contribuindo para que as Organiza\\u00e7\\u00f5es sejam mais produtivas e com equidade nas a\\u00e7\\u00f5es e recompensas.&nbsp;<\\/span><br style=\\\"color: #5d5d5d; font-family: \'Open Sans\', sans-serif; font-size: 18px;\\\" data-mce-style=\\\"color: #5d5d5d; font-family: \'Open Sans\', sans-serif; font-size: 18px;\\\"><br style=\\\"color: #5d5d5d; font-family: \'Open Sans\', sans-serif; font-size: 18px;\\\" data-mce-style=\\\"color: #5d5d5d; font-family: \'Open Sans\', sans-serif; font-size: 18px;\\\"><span style=\\\"color: #5d5d5d; font-family: \'Open Sans\', sans-serif; font-size: 18px;\\\" data-mce-style=\\\"color: #5d5d5d; font-family: \'Open Sans\', sans-serif; font-size: 18px;\\\">Atuamos em territ\\u00f3rio nacional com a miss\\u00e3o de promover o m\\u00e9rito em todos os n\\u00edveis hier\\u00e1rquicos por meio de eventos, pesquisas, troca de experi\\u00eancias, interc\\u00e2mbio colaborativo, estimulando debates das melhores pr\\u00e1ticas em todos os segmentos econ\\u00f4micos.<\\/span><br><\\/p>\",\"picture\":null,\"icon\":null}'),
('0657c90d', '20', '{\"content\":\"<div style=\\\"color: rgb(34, 34, 34); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; margin-bottom: 50px;\\\"><img src=\\\"http:\\/\\/hmi.net.br\\/themes\\/hmi\\/resources\\/img\\/ic_congressos.png\\\" style=\\\"border: 0px; vertical-align: middle; height: 70px;\\\"><p style=\\\"margin: 10px 0px; max-width: 500px; color: rgb(206, 128, 4); font-size: 20px; font-weight: 700;\\\">Congressos<\\/p><p style=\\\"margin: 0px; max-width: 500px; color: rgb(0, 0, 0); font-size: 18px;\\\">Promovemos discuss\\u00f5es atualizadas sobre a meritocracia nas corpora\\u00e7\\u00f5es. Conte\\u00fados liderados por grandes executivos e especialistas, apresenta\\u00e7\\u00e3o dos melhores cases al\\u00e9m de novas tend\\u00eancias na Gest\\u00e3o Disruptiva.<\\/p><\\/div><div style=\\\"color: rgb(34, 34, 34); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; margin-bottom: 50px;\\\"><img src=\\\"http:\\/\\/hmi.net.br\\/themes\\/hmi\\/resources\\/img\\/ic_palestras.png\\\" style=\\\"border: 0px; vertical-align: middle; height: 70px;\\\"><p style=\\\"margin: 10px 0px; max-width: 500px; color: rgb(206, 128, 4); font-size: 20px; font-weight: 700;\\\">Palestras&nbsp;<i>in company<\\/i><\\/p><p style=\\\"margin: 0px; max-width: 500px; color: rgb(0, 0, 0); font-size: 18px;\\\">Desenvolvemos workshops&nbsp;<i>in company<\\/i>&nbsp;desenhados sob medida, entendendo modelo de gest\\u00e3o, maturidade corporativa, capacidade de investimentos, porte e segmento.<\\/p><\\/div><div style=\\\"color: rgb(34, 34, 34); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; margin-bottom: 50px;\\\"><img src=\\\"http:\\/\\/hmi.net.br\\/themes\\/hmi\\/resources\\/img\\/ic_consultoria.png\\\" style=\\\"border: 0px; vertical-align: middle; height: 70px;\\\"><p style=\\\"margin: 10px 0px; max-width: 500px; color: rgb(206, 128, 4); font-size: 20px; font-weight: 700;\\\">Consultoria<\\/p><p style=\\\"margin: 0px; max-width: 500px; color: rgb(0, 0, 0); font-size: 18px;\\\">Disponibilizamos especialistas em Finan\\u00e7as, Processos, Recursos Humanos, Comunica\\u00e7\\u00e3o Interna para contribuir em desenhos estrat\\u00e9gicos para implanta\\u00e7\\u00e3o da Meritocracia.<\\/p><\\/div><div style=\\\"color: rgb(34, 34, 34); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; margin-bottom: 50px;\\\"><img src=\\\"http:\\/\\/hmi.net.br\\/themes\\/hmi\\/resources\\/img\\/ic_artigos.png\\\" style=\\\"border: 0px; vertical-align: middle; height: 70px;\\\"><p style=\\\"margin: 10px 0px; max-width: 500px; color: rgb(206, 128, 4); font-size: 20px; font-weight: 700;\\\">Artigos<\\/p><p style=\\\"margin: 0px; max-width: 500px; color: rgb(0, 0, 0); font-size: 18px;\\\">Entendemos que a informa\\u00e7\\u00e3o e conhecimento s\\u00e3o compartilhados em prol do desenvolvimento da sociedade, empresas e pessoas fomentam a economia do pa\\u00eds, ent\\u00e3o exercemos nosso papel de agente de transforma\\u00e7\\u00e3o.<\\/p><\\/div>\",\"picture\":null,\"icon\":null}'),
('0689c90d', '2', '{\"title\":\"Quem Somos\",\"description\":\"O HMI \\u00e9 uma entidade sem fins lucrativos que acredita na cultura meritocr\\u00e1tica dentro das corpora\\u00e7\\u00f5es como estrat\\u00e9gia de crescimento sustent\\u00e1vel atrav\\u00e9s de entregas diferenciadas.\",\"box\":{\"text\":\"Nosso prop\\u00f3sito \\u00e9 fomentar a cultura do m\\u00e9rito dentro das empresas.\",\"link\":\"4\"},\"picture\":null,\"icon\":null}'),
('0f04f3f7', '5', '{\"name\":\"Est\\u00edmulo a diversidade\",\"icon\":null,\"picture\":{\"filename\":\"996792cb.jpg\",\"hash\":\"a7641876e27b78e42024cab1b5660b9e221c666d\"}}'),
('1c61a057', '5', '{\"name\":\"Vantagem competitiva\",\"icon\":null,\"picture\":{\"filename\":\"40611ca5.jpg\",\"hash\":\"bb5447c2c80818b2ed39be54934ee39ff0d40ce6\"}}'),
('33ab51be', '12', '{\"content\":\"<ul style=\\\"padding: 0px 0px 0px 18px; margin: 0px; color: rgb(255, 255, 255); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;\\\"><li>Transpar\\u00eancia nas a\\u00e7\\u00f5es;<\\/li><li>Coer\\u00eancia para ser justo;<\\/li><li>Postura \\u00e9tica e apartid\\u00e1ria;<\\/li><li>Estimulo a diversidade;<\\/li><li>Busca pela inova\\u00e7\\u00e3o conectada;<\\/li><li>Incentivo a redes colaborativas;<\\/li><li>Conduta de compliance.<\\/li><\\/ul>\",\"title\":\"Valores\",\"picture\":null,\"icon\":null}'),
('67181b86', '3', '{\"name\":\"Palestras\",\"icon\":{\"filename\":\"f2527599.png\",\"hash\":\"44c9c517d68757abbe2478b83cb6fd084d88813d\"},\"picture\":{\"filename\":\"7f2d893a.jpg\",\"hash\":\"9316f1a22ae5754a7b4fc6f4dbf476e96ff352b3\"},\"url\":\"\\/o-que-oferecemos\"}'),
('67ab33ae', '1', '{\"title\":\"Remunerar diferente as entregas diferenciadas.\",\"picture\":{\"filename\":\"906a23d0.jpg\",\"hash\":\"bb4c151a387428639a58b24a34b4f703b5944a1a\"},\"icon\":null}'),
('67ab53ce', '12', '{\"content\":\"<p>Sermos uma comunidade participativa que busca resultados melhores em toda cadeia empresarial, adotando as melhores pr\\u00e1ticas em todo ciclo de recompensa.<br data-mce-bogus=\\\"1\\\"><\\/p>\",\"title\":\"Vis\\u00e3o de futuro\",\"picture\":null,\"icon\":null}'),
('67ab55ae', '12', '{\"content\":\"<p>Encorajar as organiza\\u00e7\\u00f5es na implanta\\u00e7\\u00e3o da meritocracia, remunerando as entregas diferenciadas que trazem resultados efetivos, contribuindo para a excel\\u00eancia corporativa atrav\\u00e9s do alto desempenho.<\\/p>\",\"title\":\"Prop\\u00f3sito\",\"picture\":null,\"icon\":null}'),
('76c099c4', '3', '{\"name\":\"Congressos\",\"description\":\"\",\"icon\":{\"filename\":\"a3e933c8.png\",\"hash\":\"1e4892e3e0ad3928293e02350b913b0b87d0ee7a\"},\"picture\":{\"filename\":\"e9fbad37.jpg\",\"hash\":\"9316f1a22ae5754a7b4fc6f4dbf476e96ff352b3\"},\"url\":\"\\/o-que-oferecemos\"}'),
('7b6aa1dc', '13', '{\"name\":\"Mariciane Gemin\",\"role\":\"Co-Founder\",\"content\":\"<p><span style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\">Mariciane \\u00e9 executiva com mais de 17 anos de experi\\u00eancia em Recursos Humanos em empresas multinacionais e nacionais, liderou projetos de cargos e remunera\\u00e7\\u00e3o estrat\\u00e9gica, implanta\\u00e7\\u00e3o de meritocracia, desenvolvimento organizacional, aquisi\\u00e7\\u00e3o de talentos, avalia\\u00e7\\u00e3o de performance, gest\\u00e3o do conhecimento, comunica\\u00e7\\u00e3o interna, auditoria trabalhista com rela\\u00e7\\u00f5es sindicais. J\\u00e1 implementou pol\\u00edticas e processos de gest\\u00e3o para Am\\u00e9rica Latina. Conduziu processos de expatria\\u00e7\\u00e3o, programas de Assessment e Coaching para Executivos.<\\/span><br style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\"><br style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\"><span style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\">Mariciane tem experi\\u00eancia em start up de novos neg\\u00f3cios, fus\\u00e3o entre empresas, gest\\u00e3o da mudan\\u00e7a, cultura empresarial, atuando como advisor em reengenharia organizacional. Acumula tamb\\u00e9m viv\\u00eancia de projetos para grupos familiares em momento de turnaround.<\\/span><br style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\"><br style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\"><span style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\">Foi a s\\u00f3cia respons\\u00e1vel pelo start up da ASAP nos estados do Paran\\u00e1 e Santa Catarina, depois respondendo tamb\\u00e9m pela divis\\u00e3o da FESA para toda regi\\u00e3o Sul. Durante mais de 06 anos liderou projetos para grandes empresas de diversos segmentos de atua\\u00e7\\u00e3o (Ind\\u00fastria, Bens de Consumo, Mercado Financeiro, TI Telecom, Agroneg\\u00f3cios e Servi\\u00e7os Especializados). Atualmente \\u00e9 s\\u00f3cia da S7 Consulting, que promove solu\\u00e7\\u00f5es voltadas para Gest\\u00e3o de Pessoas, atendendo as maiores empresas em territ\\u00f3rio nacional.<\\/span><br style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\"><br style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\"><span style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\">\\u00c9 membro do IBEF (Instituto Brasileiro de Finan\\u00e7as) , na divis\\u00e3o de Governan\\u00e7a e Compliance no Paran\\u00e1. Membro de Mulheres do Brasil no comit\\u00ea \\u201cEduca\\u00e7\\u00e3o\\u201d e tamb\\u00e9m no \\u201c80 em 8\\u201d que visa a diversidade dentro das corpora\\u00e7\\u00f5es. Membro do FinancIES no pilar de Capital Humano dentro do segmento educacional.<\\/span><br style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\"><br style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\"><span style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\">\\u00c9 formada em Pedagogia, Especializa\\u00e7\\u00e3o em Gest\\u00e3o Estrat\\u00e9gica de Recursos Humanos pela FGV, MBA em Gest\\u00e3o Estrat\\u00e9gica pela Universidade Federal do Paran\\u00e1 (UFPR). Certificada pela Quantum Assessment e Personal &amp; Professional Coaching pela Sociedade Brasileira de Coaching, certificada pela Hays Group em Cargos e Sal\\u00e1rios.<\\/span><br data-mce-bogus=\\\"1\\\"><\\/p>\",\"icon\":null,\"picture\":null}'),
('7cc948a8', '13', '{\"name\":\"Adriano Dias\",\"role\":\"Co-Founder\",\"icon\":null,\"picture\":null,\"content\":\"<p><span style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\">Adriano&nbsp;tem 30 anos de experi\\u00eancia em Finan\\u00e7as e Controladoria em empresas nacionais e multinacionais dos segmentos de ind\\u00fastria, farmac\\u00eautica, log\\u00edstica, servi\\u00e7os especializados incluindo educa\\u00e7\\u00e3o. Liderou a implanta\\u00e7\\u00e3o de or\\u00e7amento no Brasil e exterior, dirigiu reengenharia organizacional para ganhos de efici\\u00eancia operacional, redu\\u00e7\\u00e3o de custos e otimiza\\u00e7\\u00e3o de processos. Implantou Planejamento Estrat\\u00e9gico com gest\\u00e3o de KPI\\u2019s nas \\u00e1reas de backoffice. Participou de momentos de turnaround em estruturas com vi\\u00e9s de profissionaliza\\u00e7\\u00e3o da gest\\u00e3o do neg\\u00f3cio, incluindo reporte para fundo de investimento. Liderou processos de M&amp;A, desde constru\\u00e7\\u00e3o de indicadores de performance, valuation, roadshow at\\u00e9 a efetiva\\u00e7\\u00e3o da venda. \\u00c9 especialista em finan\\u00e7as estruturadas, especialmente em Fundos de Investimentos em Direitos Credit\\u00f3rios (FIDC).&nbsp;<\\/span><br style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\"><br style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\"><span style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\">Foi o Fundador e atual Secret\\u00e1rio Executivo do FinancIES \\u2013 F\\u00f3rum dos Executivos Financeiros para as Institui\\u00e7\\u00f5es de Ensino Privadas do Brasil. \\u00c9 desenvolvedor do software Thor de gest\\u00e3o administrativa, focado essencialmente na realiza\\u00e7\\u00e3o de objetivos estrat\\u00e9gicos, que integra o programa Capacita\\u00e7\\u00e3o para Resultado. \\u00c9 Conselheiro para o desenvolvimento do aplicativo Escala, para otimiza\\u00e7\\u00e3o da gest\\u00e3o de ativos relevantes para institui\\u00e7\\u00f5es educacionais.&nbsp;<\\/span><br style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\"><br style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\"><span style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\">\\u00c9 autor dos livros: \\u201cA Forma\\u00e7\\u00e3o e o Gerenciamento do Pre\\u00e7o de Venda no Com\\u00e9rcio\\u201d \\u2013 1992, \\u201cGest\\u00e3o Efetiva \\u2013 Capacitando a Equipe a Realizar Objetivos\\u201d - 2008, 3\\u00aa edi\\u00e7\\u00e3o, \\u201cGest\\u00e3o e Lideran\\u00e7a Universit\\u00e1ria\\u201d \\u2013 Volume II, Uniso, 2001, al\\u00e9m de artigos em jornais sobre gest\\u00e3o econ\\u00f4mica e financeira.&nbsp;<\\/span><br style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\"><br style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\"><span style=\\\"color: rgb(70, 70, 70); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: justify;\\\">Formado em Economia, MBA em Finan\\u00e7as Empresariais pela FGV, MBA em Gest\\u00e3o e Lideran\\u00e7a Universit\\u00e1ria \\u2013 OUI Organiza\\u00e7\\u00e3o Universit\\u00e1ria Interamericana, Brasil e Canad\\u00e1.<\\/span><br><\\/p>\"}'),
('871f56ea', '3', '{\"name\":\"Artigos\",\"icon\":{\"filename\":\"30d6658c.png\",\"hash\":\"55a7af17dbb91c21bb74ae02759e61492283a92f\"},\"picture\":{\"filename\":\"057741e8.jpg\",\"hash\":\"9316f1a22ae5754a7b4fc6f4dbf476e96ff352b3\"},\"url\":\"\\/o-que-oferecemos\"}'),
('add5c9cc', '5', '{\"name\":\"Boas pr\\u00e1ticas de Governan\\u00e7a Corporativa e Compliance\",\"icon\":null,\"picture\":{\"filename\":\"946abcc5.jpg\",\"hash\":\"bb5447c2c80818b2ed39be54934ee39ff0d40ce6\"}}'),
('be322877', '3', '{\"name\":\"Consultoria\",\"icon\":{\"filename\":\"d19a6fcc.png\",\"hash\":\"d283df3a48c308d91573d61833ccd50fb2023fef\"},\"picture\":{\"filename\":\"fcd7be66.jpg\",\"hash\":\"9316f1a22ae5754a7b4fc6f4dbf476e96ff352b3\"},\"url\":\"\\/o-que-oferecemos\"}'),
('c048e756', '5', '{\"name\":\"Fomentar a cultura do m\\u00e9rito\",\"icon\":null,\"picture\":{\"filename\":\"e6cf54f7.jpg\",\"hash\":\"bb4c151a387428639a58b24a34b4f703b5944a1a\"}}'),
('e8f8091b', '5', '{\"name\":\"Inova\\u00e7\\u00e3o na abordagem\",\"description\":\"M\\u00e9todo que inicia \\u00e1 partir do contexto local\",\"icon\":null,\"picture\":{\"filename\":\"d774dcea.jpg\",\"hash\":\"bb5447c2c80818b2ed39be54934ee39ff0d40ce6\"}}'),
('edb13a10', '4', '{\"name\":\"Alberto Cairo\",\"description\":\"MGM Consultoria\",\"url\":\"link\",\"icon\":null,\"picture\":{\"filename\":\"78b43177.png\",\"hash\":\"352fd7df699e1bda6fc99f7dcf87c6e3351be2c8\"}}');


INSERT INTO `rise_hmi_posts` (`id`, `hmi_user_id`, `title`, `description`, `slug`, `tags`, `categories`, `content`, `cover`, `is_draft`, `created_at`, `updated_at`) VALUES
('229096ca', '0cbdeb00', 'Gestão para líderes: um começo simples', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.', 'gestao-para-lideres-um-comeco-simples', 'null', 'null', '<p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non lorem vehicula, vestibulum lorem a, interdum felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus orci urna, vestibulum tempor eleifend at, sodales vitae ligula. Donec viverra risus nec diam pellentesque, a consectetur tellus pharetra. Nam convallis sem ut malesuada suscipit. Aenean tincidunt semper augue. Morbi elementum ante ut ante varius convallis. Aenean ut mollis urna, blandit auctor urna. Morbi at lectus urna. Ut placerat erat a dui placerat, et varius lacus varius. Suspendisse potenti. Ut nec lobortis metus, a faucibus neque. Nulla sit amet ex sit amet lorem dictum consequat ut sit amet augue. Cras in metus eu arcu efficitur egestas condimentum et purus. Proin nisi nunc, tristique eu sollicitudin vitae, congue eu orci.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Morbi vitae nisl non sapien suscipit tristique. Nullam gravida nisl posuere ante aliquam gravida. Etiam interdum erat ac est varius fringilla at a ante. In mollis, libero in hendrerit faucibus, nisi nisl rhoncus nisl, a posuere metus urna vitae nisl. Etiam vitae congue quam. Fusce non facilisis enim. Aliquam erat volutpat. Aliquam efficitur mauris sit amet nulla condimentum, at dignissim justo sollicitudin.</p><p><br></p>', '{\"filename\":\"73cb9803.jpg\",\"hash\":\"bb5447c2c80818b2ed39be54934ee39ff0d40ce6\"}', 0, '2019-02-27 05:30:52', '2019-02-27 10:10:27'),
('fff8f768', '0cbdeb00', 'Publicação 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non lorem vehicula, vestibulum lorem.', 'publicacao-1', 'null', 'null', '<p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non lorem vehicula, vestibulum lorem a, interdum felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus orci urna, vestibulum tempor eleifend at, sodales vitae ligula. Donec viverra risus nec diam pellentesque, a consectetur tellus pharetra. Nam convallis sem ut malesuada suscipit. Aenean tincidunt semper augue. Morbi elementum ante ut ante varius convallis. Aenean ut mollis urna, blandit auctor urna. Morbi at lectus urna. Ut placerat erat a dui placerat, et varius lacus varius. Suspendisse potenti. Ut nec lobortis metus, a faucibus neque. Nulla sit amet ex sit amet lorem dictum consequat ut sit amet augue. Cras in metus eu arcu efficitur egestas condimentum et purus. Proin nisi nunc, tristique eu sollicitudin vitae, congue eu orci.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Morbi vitae nisl non sapien suscipit tristique. Nullam gravida nisl posuere ante aliquam gravida. Etiam interdum erat ac est varius fringilla at a ante. In mollis, libero in hendrerit faucibus, nisi nisl rhoncus nisl, a posuere metus urna vitae nisl. Etiam vitae congue quam. Fusce non facilisis enim. Aliquam erat volutpat. Aliquam efficitur mauris sit amet nulla condimentum, at dignissim justo sollicitudin.</p><p><br></p>', '{\"filename\":\"24f3e390.jpg\",\"hash\":\"9316f1a22ae5754a7b4fc6f4dbf476e96ff352b3\"}', 0, '2019-02-27 00:55:35', '2019-02-27 10:12:45');
