import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import moment from 'moment'

import { apiFetchEvent } from '@/actions/events'
import { apiFetchEventSubscriptionsByEvent, apiDeleteEventSubscription } from '@/actions/eventSubscriptions'

import picture_placeholder from '@/assets/img/picture_placeholder.png'
import ic_sq_plus from '@/assets/img/ic_sq_plus.png'
import ic_sq_home from '@/assets/img/ic_sq_home.png'
import ic_sq_posts from '@/assets/img/ic_sq_posts.png'
import ic_sq_pages from '@/assets/img/ic_sq_pages.png'
import ic_sq_calendar from '@/assets/img/ic_sq_calendar.png'
import ic_sq_settings from '@/assets/img/ic_sq_settings.png'
import ic_sq_exit from '@/assets/img/ic_sq_exit.png'

@connect((state) => {
  return {
    event: state.events.active,
    eventSubscriptions: state.eventSubscriptions.list,
  }
}, (dispatch, ownProps) => {
  return {
    apiFetchEvent(params) {
      return dispatch(apiFetchEvent(params))
    },

    apiFetchEventSubscriptionsByEvent(params) {
      return dispatch(apiFetchEventSubscriptionsByEvent(params))
    },

    apiDeleteEventSubscription(params) {
      return dispatch(apiDeleteEventSubscription(params))
    },
  }
})
export default class EventSubscriptions extends Component {
  constructor(props) {
    super(props)

    this.handleDeleteSuccess = this.handleDeleteSuccess.bind(this)
    this.handleDeleteError = this.handleDeleteError.bind(this)
  }

  componentDidMount() {
    const { id } = this.props.match.params

    this.props.apiFetchEvent({ id: id })
    this.props.apiFetchEventSubscriptionsByEvent({ id: id })
  }

  handleDelete(id, event) {
    event.preventDefault()

    this.props.apiDeleteEventSubscription({ id: id })
      .then(this.handleDeleteSuccess)
      .catch(this.handleDeleteError)
  }

  handleDeleteSuccess() {
    const { id } = this.props.match.params

    alert('Item deletado com sucesso!')

    this.props.apiFetchEventSubscriptionsByEvent({ id: id })
  }

  handleDeleteError(err) {
    alert('Um erro ocorreu. Por favor contatar o administrador da plataforma.')
  }

  render() {
    const { event, eventSubscriptions } = this.props

    return (
      <div className="page center event-subscriptions-page">
        <div className="widget widget-buttons">
          <div className="widget-content">
            <Link className="card card-collection card-collection-secondary" to="/posts/new" style={{backgroundImage: `url('${ic_sq_plus}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Novo Post</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/posts" style={{backgroundImage: `url('${ic_sq_posts}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Posts</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/pages" style={{backgroundImage: `url('${ic_sq_pages}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Páginas</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/events" style={{backgroundImage: `url('${ic_sq_calendar}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Eventos</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/logout" style={{backgroundImage: `url('${ic_sq_exit}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Sair</p>
            </Link>
          </div>
        </div>

        <div className="widget widget-alpha widget-events-list">
          <div className="widget-header">
            <p className="widget-title">Lista de inscritos</p>
            <p className="widget-description">{(event.item) && event.item.title}</p>
          </div>
          <div className="widget-content">
            <div className="list list-alpha">
              <div className="list-header">
                <span className="list-header-attribute" data-attribute="index"><span></span></span>
                <span className="list-header-attribute" data-attribute="name"><span>Nome</span></span>
                <span className="list-header-attribute" data-attribute="email"><span>E-mail</span></span>
              </div>
              <div className="list-rows">
              {(eventSubscriptions.items) && eventSubscriptions.items.map((subscription, i) => {
                return (
                  <div key={i} className="list-row">
                    <div className="left">
                      <div className="list-row-attribute" data-attribute="index">
                        <div className="value">{i+1}</div>
                      </div>
                      <div className="list-row-attribute" data-attribute="name">
                        <div className="value">{subscription.name}</div>
                      </div>
                      <div className="list-row-attribute" data-attribute="email">
                        <div className="value">{subscription.email}</div>
                      </div>
                    </div>
                    <div className="right">
                      <div className="actions">
                        <a className="action" href="javascript:void(0)" onClick={this.handleDelete.bind(this, subscription.id)}>
                          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path fill="#000" d="M425.3 51.4h-91.5V16.7C333.8 7.5 326.4 0 317 0H195C185.5 0 178 7.5 178 16.7v34.7H86.7C77.5 51.4 70 58.8 70 68v51.4c0 9.2 7.5 16.7 16.7 16.7h338.6c9.2 0 16.7-7.4 16.7-16.6V68c0-9.2-7.5-16.6-16.7-16.6zm-125 0h-88.8v-18h89v18zM93.2 169.5L107 496c.4 9 7.8 16 16.7 16h264.6c9 0 16.3-7 16.7-16l13.8-326.5H93.2zM205.5 444c0 9.3-7.4 16.8-16.7 16.8-9.2 0-16.7-7.5-16.7-16.7V237.5c0-9.2 7.6-16.7 16.8-16.7 9.3 0 16.7 7.5 16.7 16.7V444zm67.2 0c0 9.3-7.5 16.8-16.7 16.8s-16.7-7.5-16.7-16.7V237.5c0-9.2 7.5-16.7 16.7-16.7s16.7 7.5 16.7 16.7V444zm67.2 0c0 9.3-7.6 16.8-16.8 16.8s-16.7-7.5-16.7-16.7V237.5c0-9.2 7.4-16.7 16.7-16.7s16.7 7.5 16.7 16.7V444z"/>
                          </svg>
                        </a>
                      </div>
                    </div>
                  </div>
                )
              })}
              </div>
            </div>

          </div>
        </div>
      </div>
    )
  }
}

