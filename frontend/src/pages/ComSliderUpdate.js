import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { SERVER_ADDRESS } from '../constants/Globals'

import IcArrowLeft from '@/icons/ic_arrow_left'

import collection_placeholder from '@/assets/img/collection_placeholder.jpg'
import picture_placeholder from '@/assets/img/picture_placeholder.png'

import { apiFetchContent, apiUpdateContent, changeActiveContent } from '@/actions/content'

@connect((state) => {
  return {
    content: state.content.active,
  }
}, (dispatch, ownProps) => {
  return {
    apiFetchContent(props) {
      return dispatch(apiFetchContent(props))
    },

    apiUpdateContent(params, props) {
      return dispatch(apiUpdateContent(params, props))
    },

    changeActiveContent(props) {
      return dispatch(changeActiveContent(props))
    },
  }
})
export default class ComSliderUpdate extends Component {
  constructor(props) {
    super(props)

    this.previewIconInput = React.createRef()
    this.iconInput = React.createRef()
    this.previewPictureInput = React.createRef()
    this.pictureInput = React.createRef()

    this.handlePictureChange = this.handlePictureChange.bind(this)
    this.handleSave = this.handleSave.bind(this)
    this.handleSaveSuccess = this.handleSaveSuccess.bind(this)
    this.handleSaveError = this.handleSaveError.bind(this)
  }

  componentDidMount() {
    const { id } = this.props.match.params

    this.props.apiFetchContent({ id: id })
  }

  getUserDataLink(filename) {
    return `${SERVER_ADDRESS}/data/user_data/${filename}`
  }

  getContentDataLink(filename) {
    return `${SERVER_ADDRESS}/data/content_data/${filename}`
  }

  handleTextInputChange(name, event) {
    const { content } = this.props
    const value = event.target.value

    if (value !== undefined && value !== null) {
      this.props.changeActiveContent(update(content.item, {
          data: { $merge: { [name]: value } }
      }))
    }
  }

  handlePictureChange(event) {
    const target = event.currentTarget
    const previewPicture = target.parentNode

    for (let i = 0; i < target.files.length; i++) {
      if (i === 0) {
        const file = target.files[i]

        if (!file.type.startsWith('image/')){ continue }

        const reader = new FileReader()
        reader.onload = function (event) {
          previewPicture.style.backgroundImage = `url(${event.target.result})`
        }
        reader.readAsDataURL(file)
      }
    }
  }

  handleSave(event) {
    const { id } = this.props.match.params
    const { content } = this.props
    const data = new FormData()

    for (var property in content.item) {
      if (content.item.hasOwnProperty(property)) {
        if (property === 'data') data.append(property, JSON.stringify(content.item[property]))
        else data.append(property, content.item[property])
      }
    }

    if (this.pictureInput.current.files && this.pictureInput.current.files[0]) {
      data.append('picture', this.pictureInput.current.files[0])
    }

    this.props.apiUpdateContent({ id: id }, data)
      .then(this.handleSaveSuccess)
      .catch(this.handleSaveError)
  }

  handleSaveSuccess(result) {
    const { history } = this.props

    alert('Item atualizado com sucesso!')
    
    history.push('/#cases')
  }

  handleSaveError(err) {
    alert('Um erro ocorreu. Por favor contate o administrador da plataforma.')
  }

  render() {
    return (
      <div className="page-primary w6">
        <div className="page-header">
          <Link className="page-tie" to="/#slider">Slider</Link>
          <p className="page-title">Editar item</p>
        </div>

        <div className="page-content">

          <div className="widget w6">
            <div className="widget-header">
              <div className="widget-header-row">
                <div className="left">
                  <p className="widget-title">Informações básicas</p>
                </div>
                <div className="right"></div>
              </div>
            </div>
            <div className="widget-content">
              <div className="left">
                <div className="inputs inline">
                  <div className="inputs">
                    <div className="input w2">
                      <label>Nome</label>
                      <input type="text" value={this.props.content.item && this.props.content.item.data.name || ''} onChange={this.handleTextInputChange.bind(this, 'name')}/>
                    </div>
                    <div className="input w3-q3">
                      <label>Descrição</label>
                      <input type="text" value={this.props.content.item && this.props.content.item.data.description || ''} onChange={this.handleTextInputChange.bind(this, 'description')}/>
                    </div>
                    <div className="input w4">
                      <label>Link</label>
                      <input type="text" value={this.props.content && this.props.content.data && this.props.content.data.url} onChange={this.handleTextInputChange.bind(this, 'url')}/>
                    </div>
                  </div>
                </div>
              </div>
              <div className="right"></div>
            </div>
          </div>

          <div className="widget widget-beta widget-picture">
            <div className="widget-header">
              <span className="widget-title">Imagem de fundo</span>
            </div>
            <div className="widget-content">
              <div className="preview-picture" style={{ backgroundImage: `url(${this.props.content.item && this.props.content.item.data.picture && this.getContentDataLink(this.props.content.item.data.picture.filename) || picture_placeholder})`}} ref={this.previewPictureInput}>
                <div className="preview-picture-overlay"></div>
                <input type="file" ref={this.pictureInput} onChange={this.handlePictureChange}/>
              </div>
              <div className="picture-message">Clique para enviar uma imagem</div>
            </div>
          </div>

        </div>
        <div className="page-content">
          <div className="buttons">
            <button className="btn btn-one" onClick={this.handleSave}>Salvar</button>
          </div>
        </div>
      </div>
    )
  }
}

