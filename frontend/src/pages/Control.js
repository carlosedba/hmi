import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'

import Collapse from '@/components/Collapse'
import CollapseHero from '@/components/CollapseHero'
import CollapseQuemSomos from '@/components/CollapseQuemSomos'
import CollapseOQueOferecemos from '@/components/CollapseOQueOferecemos'
import CollapseCases from '@/components/CollapseCases'
import CollapseSlider from '@/components/CollapseSlider'

import picture_placeholder from '@/assets/img/picture_placeholder.png'
import ic_sq_plus from '@/assets/img/ic_sq_plus.png'
import ic_sq_home from '@/assets/img/ic_sq_home.png'
import ic_sq_posts from '@/assets/img/ic_sq_posts.png'
import ic_sq_pages from '@/assets/img/ic_sq_pages.png'
import ic_sq_calendar from '@/assets/img/ic_sq_calendar.png'
import ic_sq_settings from '@/assets/img/ic_sq_settings.png'
import ic_sq_exit from '@/assets/img/ic_sq_exit.png'

@connect((state) => {
  return {
    components: state.components.list,
    content: state.content.list,
  }
})
export default class Control extends Component {
  constructor(props) {
    super(props)

    this.previewPicture = React.createRef()
    this.inputPicture = React.createRef()

    this.state = {}

    this.handlePictureChange = this.handlePictureChange.bind(this)
  }

  componentDidMount() {}

  handlePictureChange(event) {
    event.preventDefault()

    const previewPicture = this.previewPicture.current
    const inputPicture = this.inputPicture.current

    for (let i = 0; i < inputPicture.files.length; i++) {
      if (i === 0) {
        const file = inputPicture.files[i]

        if (!file.type.startsWith('image/')){ continue }

        const reader = new FileReader()
        reader.onload = function (event) {
          previewPicture.style.backgroundImage = `url(${event.target.result})`
        }
        reader.readAsDataURL(file)
      }
    }
  }

  filterContentByAlias(alias = '') {
    if (!this.props.content.loading) {
      return this.props.content.items.filter(obj => {
        return obj.component.alias === alias
      })
    }
  }

  getContentByAlias(alias = '') {
    const content = this.filterContentByAlias(alias)

    if (content) return content[0]
  }

  render() {
    return (
      <div className="page center control-page">
        <div className="widget widget-buttons">
          <div className="widget-content">
            <Link className="card card-collection card-collection-secondary" to="/posts/new" style={{backgroundImage: `url('${ic_sq_plus}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Novo Post</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/posts" style={{backgroundImage: `url('${ic_sq_posts}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Posts</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/pages" style={{backgroundImage: `url('${ic_sq_pages}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Páginas</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/events" style={{backgroundImage: `url('${ic_sq_calendar}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Eventos</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/logout" style={{backgroundImage: `url('${ic_sq_exit}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Sair</p>
            </Link>
          </div>
        </div>
        <div className="widget widget-alpha">
          <div className="widget-header">
            <p className="widget-title">Página inicial</p>
            <p className="widget-description">Expanda os itens para editar o conteúdo da página.</p>
          </div>
          <div className="widget-content">
            <Collapse name="Hero" content={CollapseHero} history={this.props.history}></Collapse>
            <Collapse name="Quem somos" content={CollapseQuemSomos} history={this.props.history}></Collapse>
            <Collapse name="O que oferecemos" content={CollapseOQueOferecemos} history={this.props.history}></Collapse>
            <Collapse name="Cases" content={CollapseCases} history={this.props.history}></Collapse>
            <Collapse name="Slider" content={CollapseSlider} history={this.props.history}></Collapse>
          </div>
        </div>
      </div>
    )
  }
}

