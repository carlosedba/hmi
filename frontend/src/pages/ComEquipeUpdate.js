import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { SERVER_ADDRESS } from '../constants/Globals'

import IcArrowLeft from '@/icons/ic_arrow_left'

import collection_placeholder from '@/assets/img/collection_placeholder.jpg'
import picture_placeholder from '@/assets/img/picture_placeholder.png'

import { apiFetchContent, apiUpdateContent, changeActiveContent } from '@/actions/content'

@connect((state) => {
  return {
    content: state.content.active,
  }
}, (dispatch, ownProps) => {
  return {
    apiFetchContent(props) {
      return dispatch(apiFetchContent(props))
    },

    apiUpdateContent(params, props) {
      return dispatch(apiUpdateContent(params, props))
    },

    changeActiveContent(props) {
      return dispatch(changeActiveContent(props))
    },
  }
})
export default class ComEquipeUpdate extends Component {
  constructor(props) {
    super(props)

    this.target = React.createRef()
    this.previewPictureInput = React.createRef()
    this.pictureInput = React.createRef()

    this.handlePictureChange = this.handlePictureChange.bind(this)
    this.handleSave = this.handleSave.bind(this)
    this.handleSaveSuccess = this.handleSaveSuccess.bind(this)
    this.handleSaveError = this.handleSaveError.bind(this)
    this.setEditorContent = this.setEditorContent.bind(this)
  }

  componentDidMount() {
    const { id } = this.props.match.params

    this.props.apiFetchContent({ id: id }).then(this.setEditorContent)
    this.setEditor()
    this.setEditorContent()
  }

  getUserDataLink(filename) {
    return `${SERVER_ADDRESS}/data/user_data/${filename}`
  }

  getContentDataLink(filename) {
    return `${SERVER_ADDRESS}/data/content_data/${filename}`
  }

  setEditor() {
    if (window.tinymce) {
      window.tinymce.init({
        target: this.target.current,
        height: 300,
        menubar: false,
        resize: false,
        elementpath: false,
        plugins: 'autolink autosave link image wordcount',
        toolbar: 'styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist blockquote | outdent indent | link unlink image | code',
         style_formats: [
          {title: 'Header 1', format: 'h1'},
          {title: 'Header 2', format: 'h2'},
          {title: 'Header 3', format: 'h3'},
          {title: 'Paragraph', format: 'p'},
          {title: 'Blockquote', format: 'blockquote'}
        ],
        fontsize_formats: '12pt 14pt 18pt 24pt 36pt',
        language: 'pt_BR',
      })
    }
  }

  setEditorContent() {
    const { content } = this.props

    if (tinyMCE.activeEditor && tinyMCE.activeEditor.contentDocument && content.item && content.item.data.content) {
      tinyMCE.activeEditor.execCommand('mceSetContent', false, content.item.data.content)
    }

    if ((!content.item && content.loading) || !tinyMCE.activeEditor.contentDocument) {
      setTimeout(() => { this.setEditorContent() }, 100)
    }
  }

  handleTextInputChange(name, event) {
    const { content } = this.props
    const value = event.target.value

    if (value !== undefined && value !== null) {
      this.props.changeActiveContent(update(content.item, {
          data: { $merge: { [name]: value } }
      }))
    }
  }

  handlePictureChange(event) {
    const target = event.currentTarget
    const previewPicture = target.parentNode

    for (let i = 0; i < target.files.length; i++) {
      if (i === 0) {
        const file = target.files[i]

        if (!file.type.startsWith('image/')){ continue }

        const reader = new FileReader()
        reader.onload = function (event) {
          previewPicture.style.backgroundImage = `url(${event.target.result})`
        }
        reader.readAsDataURL(file)
      }
    }
  }

  handleSave(event) {
    const { id } = this.props.match.params
    const { content } = this.props
    const data = new FormData()
    const editorData = tinyMCE.activeEditor.getContent({ format: 'raw' }) || null
    
    for (var property in content.item) {
      if (content.item.hasOwnProperty(property)) {

        if (property === 'data') {
          let payload = content.item[property]
          if (editorData) payload = Object.assign({}, payload, { content: editorData })
            
          data.append(property, JSON.stringify(payload))
        } else {
          data.append(property, content.item[property])
        }

      }
    }
    
    if (this.pictureInput.current.files && this.pictureInput.current.files[0]) {
      data.append('picture', this.pictureInput.current.files[0])
    }

    this.props.apiUpdateContent({ id: id }, data)
      .then(this.handleSaveSuccess)
      .catch(this.handleSaveError)
  }

  handleSaveSuccess(result) {
    const { history } = this.props

    alert('Item atualizado com sucesso!')
    
    history.push('/pages/quem-somos/#equipe')
  }

  handleSaveError(err) {
    alert('Um erro ocorreu. Por favor contate o administrador da plataforma.')
  }

  render() {
    const { content } = this.props

    return (
      <div className="page-primary w6">
        <div className="page-header">
          <Link className="page-tie" to="/pages/quem-somos/#equipe">Equipe</Link>
          <p className="page-title">Editar item</p>
        </div>

        <div className="page-content">

          <div className="widget w6">
            <div className="widget-header">
              <div className="widget-header-row">
                <div className="left">
                  <p className="widget-title">Informações básicas</p>
                </div>
                <div className="right"></div>
              </div>
            </div>
            <div className="widget-content">
              <div className="left">
                <div className="inputs inline">
                  <div className="inputs">
                    <div className="input w2">
                      <label>Nome:</label>
                      <input type="text" value={content.item && content.item.data && content.item.data.name || ''} onChange={this.handleTextInputChange.bind(this, 'name')}/>
                    </div>
                    <div className="input w3-q3">
                      <label>Cargo:</label>
                      <input type="text" value={content.item && content.item.data && content.item.data.role || ''} onChange={this.handleTextInputChange.bind(this, 'role')}/>
                    </div>
                    <div className="input input-tinymce">
                      <label>Conteúdo:</label>
                      <textarea ref={this.target}></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div className="right"></div>
            </div>
          </div>

          <div className="widget widget-beta widget-picture">
            <div className="widget-header">
              <span className="widget-title">Foto</span>
            </div>
            <div className="widget-content">
              <div className="preview-picture" style={{ backgroundImage: `url(${this.props.content.item && this.props.content.item.data.picture && this.getContentDataLink(this.props.content.item.data.picture.filename) || picture_placeholder})`}} ref={this.previewPictureInput}>
                <div className="preview-picture-overlay"></div>
                <input type="file" ref={this.pictureInput} onChange={this.handlePictureChange}/>
              </div>
              <div className="picture-message">Clique para enviar uma imagem</div>
            </div>
          </div>

        </div>
        <div className="page-content">
          <div className="buttons">
            <button className="btn btn-one" onClick={this.handleSave}>Salvar</button>
          </div>
        </div>
      </div>
    )
  }
}

