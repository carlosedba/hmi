import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'

import picture_placeholder from '@/assets/img/picture_placeholder.png'
import ic_sq_plus from '@/assets/img/ic_sq_plus.png'
import ic_sq_home from '@/assets/img/ic_sq_home.png'
import ic_sq_posts from '@/assets/img/ic_sq_posts.png'
import ic_sq_pages from '@/assets/img/ic_sq_pages.png'
import ic_sq_calendar from '@/assets/img/ic_sq_calendar.png'
import ic_sq_settings from '@/assets/img/ic_sq_settings.png'
import ic_sq_exit from '@/assets/img/ic_sq_exit.png'

@connect((state) => {
  return {}
}, (dispatch, ownProps) => {
  return {}
})
export default class Pages extends Component {
  constructor(props) {
    super(props)

    //this.handleDeleteSuccess = this.handleDeleteSuccess.bind(this)
  }

  componentDidMount() {}

  render() {
    return (
      <div className="page center pages-page">
        <div className="widget widget-buttons">
          <div className="widget-content">
            <Link className="card card-collection card-collection-secondary" to="/posts/new" style={{backgroundImage: `url('${ic_sq_plus}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Novo Post</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/posts" style={{backgroundImage: `url('${ic_sq_posts}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Posts</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/pages" style={{backgroundImage: `url('${ic_sq_pages}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Páginas</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/events" style={{backgroundImage: `url('${ic_sq_calendar}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Eventos</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/logout" style={{backgroundImage: `url('${ic_sq_exit}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Sair</p>
            </Link>
          </div>
        </div>

        <div className="widget widget-alpha widget-posts-list">
          <div className="widget-header">
            <p className="widget-title">Páginas</p>
            <p className="widget-description">Clique no botão "Editar" para atualizar o conteúdo de uma página.</p>
          </div>
          <div className="widget-content">

            <div className="list list-alpha">
              <div className="list-header">
                <span className="list-header-attribute" data-attribute="name"><span>Nome</span></span>
                <span className="list-header-attribute" data-attribute="description"><span></span></span>
              </div>
              <div className="list-rows">
                <div className="list-row">
                  <div className="left">
                    <div className="list-row-attribute" data-attribute="name">
                      <div className="value">Página inicial</div>
                    </div>
                    <div className="list-row-attribute" data-attribute="description">
                      <div className="value"></div>
                    </div>
                  </div>
                  <div className="right">
                    <Link className="btn" to="/">Editar</Link>
                  </div>
                </div>
                <div className="list-row">
                  <div className="left">
                    <div className="list-row-attribute" data-attribute="name">
                      <div className="value">Quem Somos</div>
                    </div>
                    <div className="list-row-attribute" data-attribute="description">
                      <div className="value"></div>
                    </div>
                  </div>
                  <div className="right">
                    <Link className="btn" to="/pages/quem-somos">Editar</Link>
                  </div>
                </div>
                <div className="list-row">
                  <div className="left">
                    <div className="list-row-attribute" data-attribute="name">
                      <div className="value">O que oferecemos</div>
                    </div>
                    <div className="list-row-attribute" data-attribute="description">
                      <div className="value"></div>
                    </div>
                  </div>
                  <div className="right">
                    <Link className="btn" to="/pages/o-que-oferecemos">Editar</Link>
                  </div>
                </div>
                <div className="list-row">
                  <div className="left">
                    <div className="list-row-attribute" data-attribute="name">
                      <div className="value">Eventos</div>
                    </div>
                    <div className="list-row-attribute" data-attribute="description">
                      <div className="value"></div>
                    </div>
                  </div>
                  <div className="right">
                    <Link className="btn" to="/pages/eventos">Editar</Link>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    )
  }
}

