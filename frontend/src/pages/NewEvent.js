import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { SERVER_ADDRESS } from '../constants/Globals'

import IcArrowLeft from '@/icons/ic_arrow_left'

import collection_placeholder from '@/assets/img/collection_placeholder.jpg'
import picture_placeholder from '@/assets/img/picture_placeholder.png'

import { apiCreateEvent, changeNewEvent } from '@/actions/events'

@connect((state) => {
  return {
    user: state.session.user,
    event: state.events.new,
  }
}, (dispatch, ownProps) => {
  return {
    apiCreateEvent(props) {
      return dispatch(apiCreateEvent(props))
    },

    changeNewEvent(props) {
      return dispatch(changeNewEvent(props))
    },
  }
})
export default class NewEvent extends Component {
  constructor(props) {
    super(props)

    this.target = React.createRef()
    this.previewIconInput = React.createRef()
    this.iconInput = React.createRef()
    this.previewCoverInput = React.createRef()
    this.coverInput = React.createRef()

    this.handlePictureChange = this.handlePictureChange.bind(this)
    this.handleSave = this.handleSave.bind(this)
    this.handleSaveSuccess = this.handleSaveSuccess.bind(this)
    this.handleSaveError = this.handleSaveError.bind(this)
  }

  componentDidMount() {
    const { user, event } = this.props

    this.props.changeNewEvent(update(event.item, {
      $set: {
        hmi_user_id: user.data.id,
        title: '',
        description: '',
        location: '',
        start_time: '',
        end_time: '',
        content: '',
        url: '',
      }
    }))

    this.setEditor()
  }

  getUserDataLink(filename) {
    return `${SERVER_ADDRESS}/data/user_data/${filename}`
  }

  getContentDataLink(filename) {
    return `${SERVER_ADDRESS}/data/content_data/${filename}`
  }

  setEditor() {
    if (window.tinymce) {
      window.tinymce.init({
        target: this.target.current,
        height: 300,
        menubar: false,
        resize: false,
        elementpath: false,
        plugins: 'autolink autosave link image wordcount',
        toolbar: 'styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist blockquote | outdent indent | link unlink image',
         style_formats: [
          {title: 'Header 1', format: 'h1'},
          {title: 'Header 2', format: 'h2'},
          {title: 'Header 3', format: 'h3'},
          {title: 'Paragraph', format: 'p'},
          {title: 'Blockquote', format: 'blockquote'}
        ],
        fontsize_formats: '12pt 14pt 18pt 24pt 36pt',
        language: 'pt_BR',
      })
    }
  }

  handleTextInputChange(name, event) {
    const propEvent = this.props.event
    const value = event.target.value

    if (value !== undefined && value !== null) {
      this.props.changeNewEvent(update(propEvent.item, {
        [name]: { $set: value }
      }))
    }
  }

  handlePictureChange(event) {
    const target = event.currentTarget
    const previewPicture = target.parentNode

    for (let i = 0; i < target.files.length; i++) {
      if (i === 0) {
        const file = target.files[i]

        if (!file.type.startsWith('image/')){ continue }

        const reader = new FileReader()
        reader.onload = function (event) {
          previewPicture.style.backgroundImage = `url(${event.target.result})`
        }
        reader.readAsDataURL(file)
      }
    }
  }

  handleSave(event) {
    const propEvent = this.props.event
    const data = new FormData()
    const editorData = tinyMCE.activeEditor.getContent({ format: 'raw' }) || null

    for (var property in propEvent.item) {
      if (propEvent.item.hasOwnProperty(property)) {
        if (property === 'data') data.append(property, JSON.stringify(propEvent.item[property]))
        else data.append(property, propEvent.item[property])
      }
    }

    if (editorData) {
      data.append('content', editorData)
    }

    for (var entry of data.entries()) { console.log(entry) }

    this.props.apiCreateEvent(data)
      .then(this.handleSaveSuccess)
      .catch(this.handleSaveError)
  }

  handleSaveSuccess(result) {
    const { history } = this.props

    alert('Evento publicado com sucesso!')
    
    history.push('/events')
  }

  handleSaveError(err) {
    alert('Um erro ocorreu. Por favor contate o administrador da plataforma.')
  }

  render() {
    const { event } = this.props

    return (
      <div className="page-primary w7">
        <div className="page-header">
          <Link className="page-tie" to="/events">Eventos</Link>
          <p className="page-title">Novo evento</p>
        </div>

        <div className="page-content">

          <div className="widget w7 widget-post-editor">
            <div className="widget-header">
              <div className="widget-header-row">
                <div className="left">
                  <p className="widget-title">Informações básicas</p>
                </div>
                <div className="right"></div>
              </div>
            </div>
            <div className="widget-content">
              <div className="inputs inline">
                <div className="inputs">
                  <div className="input w4-q2">
                    <label>Título:</label>
                    <input type="text" value={event && event.title} onChange={this.handleTextInputChange.bind(this, 'title')}/>
                  </div>
                  <div className="input w5">
                    <label>Descrição:</label>
                    <input type="text" value={event && event.description} onChange={this.handleTextInputChange.bind(this, 'description')}/>
                  </div>
                  <div className="multi-input">
                    <div className="input w2">
                      <label>Local:</label>
                      <input type="text" value={event && event.location} onChange={this.handleTextInputChange.bind(this, 'location')}/>
                    </div>
                    <div className="input w1-q2">
                      <label>Data:</label>
                      <input type="date" value={event && event.date} onChange={this.handleTextInputChange.bind(this, 'date')}/>
                    </div>
                    <div className="input w1">
                      <label>Início:</label>
                      <input type="text" value={event && event.start_time} onChange={this.handleTextInputChange.bind(this, 'start_time')}/>
                    </div>
                    <div className="input w1">
                      <label>Término:</label>
                      <input type="text" value={event && event.end_time} onChange={this.handleTextInputChange.bind(this, 'end_time')}/>
                    </div>
                  </div>
                  <div className="input input-tinymce">
                    <label>Conteúdo:</label>
                    <textarea ref={this.target}></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="page-content">
          <div className="buttons">
            <button className="btn btn-one" onClick={this.handleSave}>Salvar</button>
          </div>
        </div>
      </div>
    )
  }
}

