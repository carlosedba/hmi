import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { SERVER_ADDRESS } from '../constants/Globals'

import IcArrowLeft from '@/icons/ic_arrow_left'

import collection_placeholder from '@/assets/img/collection_placeholder.jpg'
import picture_placeholder from '@/assets/img/picture_placeholder.png'

import { apiFetchPost, apiUpdatePost, changeActivePost } from '@/actions/posts'

@connect((state) => {
  return {
    user: state.session.user,
    post: state.posts.active,
  }
}, (dispatch, ownProps) => {
  return {
    apiFetchPost(params) {
      return dispatch(apiFetchPost(params))
    },

    apiUpdatePost(params, props) {
      return dispatch(apiUpdatePost(params, props))
    },

    changeActivePost(props) {
      return dispatch(changeActivePost(props))
    },
  }
})
export default class UpdatePost extends Component {
  constructor(props) {
    super(props)

    this.target = React.createRef()
    this.previewIconInput = React.createRef()
    this.iconInput = React.createRef()
    this.previewCoverInput = React.createRef()
    this.coverInput = React.createRef()

    this.handlePictureChange = this.handlePictureChange.bind(this)
    this.handleSave = this.handleSave.bind(this)
    this.handleSaveSuccess = this.handleSaveSuccess.bind(this)
    this.handleSaveError = this.handleSaveError.bind(this)
    this.setEditorContent = this.setEditorContent.bind(this)
  }

  componentDidMount() {
    const { id } = this.props.match.params

    this.props.apiFetchPost({ id: id }).then(this.setEditorContent)
    this.setEditor()
    this.setEditorContent()
  }

  getUserDataLink(filename) {
    return `${SERVER_ADDRESS}/data/user_data/${filename}`
  }

  getContentDataLink(filename) {
    return `${SERVER_ADDRESS}/data/content_data/${filename}`
  }

  setEditor() {
    const { post } = this.props

    if (window.tinymce) {
      window.tinyMCE.init({
        target: this.target.current,
        height: 300,
        menubar: false,
        resize: false,
        elementpath: false,
        plugins: 'autolink autosave link image wordcount',
        toolbar: 'styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist blockquote | outdent indent | link unlink image | code',
         style_formats: [
          {title: 'Header 1', format: 'h1'},
          {title: 'Header 2', format: 'h2'},
          {title: 'Header 3', format: 'h3'},
          {title: 'Paragraph', format: 'p'},
          {title: 'Blockquote', format: 'blockquote'}
        ],
        fontsize_formats: '12pt 14pt 18pt 24pt 36pt',
        language: 'pt_BR',
        setup: function (editor) {
          editor.on('init', function () {
            //if (post.item) editor.execCommand('mceSetContent', false, post.item.content)
          })
        }
      })
    }
  }

  handleTextInputChange(name, event) {
    const { post } = this.props
    const value = event.target.value

    if (value !== undefined && value !== null) {
      this.props.changeActivePost(update(post.item, {
        [name]: { $set: value }
      }))
    }
  }

  handlePictureChange(event) {
    const target = event.currentTarget
    const previewPicture = target.parentNode

    for (let i = 0; i < target.files.length; i++) {
      if (i === 0) {
        const file = target.files[i]

        if (!file.type.startsWith('image/')){ continue }

        const reader = new FileReader()
        reader.onload = function (event) {
          previewPicture.style.backgroundImage = `url(${event.target.result})`
        }
        reader.readAsDataURL(file)
      }
    }
  }

  handleSave(event) {
    const { id } = this.props.match.params
    const { post } = this.props
    const data = new FormData()
    const editorData = tinyMCE.activeEditor.getContent({ format: 'raw' }) || null

    for (var property in post.item) {
      if (post.item.hasOwnProperty(property)) {
        if (property === 'data') data.append(property, JSON.stringify(post.item[property]))
        else data.append(property, post.item[property])
      }
    }

    if (this.coverInput.current.files && this.coverInput.current.files[0]) {
      data.append('cover', this.coverInput.current.files[0])
    } else {
      data.delete('cover')
    }

    if (editorData) {
      data.append('content', editorData)
    } else {
      data.delete('content')
    }

    for (var entry of data.entries()) { console.log(entry) }

    this.props.apiUpdatePost({ id: id }, data)
      .then(this.handleSaveSuccess)
      .catch(this.handleSaveError)
  }

  handleSaveSuccess(result) {
    const { history } = this.props

    alert('Post atualizado com sucesso!')
    
    history.push('/posts')
  }

  handleSaveError(err) {
    alert('Um erro ocorreu. Por favor contate o administrador da plataforma.')
  }

  setEditorContent() {
    console.log('UpdatePost > setEditorContent called!')

    const { post } = this.props

    if (tinyMCE.activeEditor && tinyMCE.activeEditor.contentDocument && post.item) {
      tinyMCE.activeEditor.execCommand('mceSetContent', false, post.item.content)
    }

    if ((!post.item && post.loading) || !tinyMCE.activeEditor.contentDocument) {
      setTimeout(() => { this.setEditorContent() }, 100)
    }
  }

  render() {
    const { post } = this.props

    return (
      <div className="page-primary w7">
        <div className="page-header">
          <Link className="page-tie" to="/posts">Posts</Link>
          <p className="page-title">Editar publicação</p>
        </div>

        <div className="page-content">

          <div className="widget w7 widget-post-editor">
            <div className="widget-header">
              <div className="widget-header-row">
                <div className="left">
                  <p className="widget-title">Informações básicas</p>
                </div>
                <div className="right"></div>
              </div>
            </div>
            <div className="widget-content">
              <div className="inputs inline">
                <div className="inputs">
                  <div className="input w4-q2">
                    <label>Nome:</label>
                    <input type="text" value={post.item && post.item.title || ''} onChange={this.handleTextInputChange.bind(this, 'title')}/>
                  </div>
                  <div className="input w5">
                    <label>Descrição:</label>
                    <input type="text" value={post.item && post.item.description || ''} onChange={this.handleTextInputChange.bind(this, 'description')}/>
                  </div>
                  <div className="input input-tinymce">
                    <label>Conteúdo:</label>
                    <textarea ref={this.target}></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="widget widget-beta widget-picture">
            <div className="widget-header">
              <span className="widget-title">Imagem de capa:</span>
            </div>
            <div className="widget-content">
              <div className="preview-picture" style={{ backgroundImage: `url(${post.item && post.item.cover && this.getContentDataLink(post.item.cover.filename) || picture_placeholder})`}} ref={this.previewCoverInput}>
                <div className="preview-picture-overlay"></div>
                <input type="file" ref={this.coverInput} onChange={this.handlePictureChange}/>
              </div>
              <div className="picture-message">Clique para enviar uma imagem</div>
            </div>
          </div>

        </div>
        <div className="page-content">
          <div className="buttons">
            <button className="btn btn-one" onClick={this.handleSave}>Salvar</button>
          </div>
        </div>
      </div>
    )
  }
}

