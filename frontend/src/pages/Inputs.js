import React, { Component } from 'react'
import { connect } from 'react-redux'

@connect((store) => {
  return {}
})
export default class Inputs extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="page">
        <div className="inputs-test">
          <div className="inputs">
            <div className="input w1">
              <label>Nome:</label>
              <input type="text"/>
            </div>
            <div className="input w1-q1">
              <label>Nome:</label>
              <input type="text"/>
            </div>
            <div className="input w1-q2">
              <label>Nome:</label>
              <input type="text"/>
            </div>
            <div className="input w1-q3">
              <label>Nome:</label>
              <input type="text"/>
            </div>
          </div>
          <div className="inputs">
            <div className="input w2">
              <label>Nome:</label>
              <input type="text"/>
            </div>
            <div className="input w2-q1">
              <label>Nome:</label>
              <input type="text"/>
            </div>
            <div className="input w2-q2">
              <label>Nome:</label>
              <input type="text"/>
            </div>
            <div className="input w2-q3">
              <label>Nome:</label>
              <input type="text"/>
            </div>
          </div>
          <div className="inputs">
            <div className="input w3">
              <label>Nome:</label>
              <input type="text"/>
            </div>
            <div className="input w3-q1">
              <label>Nome:</label>
              <input type="text"/>
            </div>
            <div className="input w3-q2">
              <label>Nome:</label>
              <input type="text"/>
            </div>
            <div className="input w3-q3">
              <label>Nome:</label>
              <input type="text"/>
            </div>
          </div>
          <div className="inputs">
            <div className="input w4">
              <label>Nome:</label>
              <input type="text"/>
            </div>
            <div className="input w4-q1">
              <label>Nome:</label>
              <input type="text"/>
            </div>
            <div className="input w4-q2">
              <label>Nome:</label>
              <input type="text"/>
            </div>
            <div className="input w4-q3">
              <label>Nome:</label>
              <input type="text"/>
            </div>
          </div>
          <div className="inputs">
            <div className="input w5">
              <label>Nome:</label>
              <input type="text"/>
            </div>
            <div className="input w5-q1">
              <label>Nome:</label>
              <input type="text"/>
            </div>
            <div className="input w5-q2">
              <label>Nome:</label>
              <input type="text"/>
            </div>
            <div className="input w5-q3">
              <label>Nome:</label>
              <input type="text"/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

