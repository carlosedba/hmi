import React, { Component } from 'react'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { login } from '@/actions/session'

@connect((state) => {
  return {
    auth: state.session.auth,
  }
}, (dispatch, ownProps) => {
  return {
    login(data) {
      return dispatch(login(data))
    },
  }
})
export default class Login extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      user: {
        email: '',
        password: '',
      }
    }

    this.handleEmailChange = this.handleEmailChange.bind(this)
    this.handlePasswordChange = this.handlePasswordChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount() {
    console.log('Login > componentDidMount called!')
  }

  componentDidUpdate(prevProps, prevState) {
    const { history, auth } = this.props
    if (auth.authenticated) history.push('/')
  }

  handleEmailChange(event) {
    const value = event.target.value
    
    this.setState((prevState, props) => {
      return update(prevState, { user: { email: { $set: value } } })
    })
  }

  handlePasswordChange(event) {
    const value = event.target.value

    this.setState((prevState, props) => {
      return update(prevState, { user: { password: { $set: value } } })
    })
  }

  handleSubmit(event) {
    event.preventDefault()

    this.props.login(this.state.user)
  }

  render() {
    return (
      <div className="page page-login">
        <div className="wrapper">
          <div className="logo">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 555 400"><path d="M550 394.9l-117.1-169L280.1 5.2l-135 195.6L4.3 395.2l287.3-113.5-25.2 64.7z"/></svg>
          </div>
          <form onSubmit={this.handleSubmit}>
            <div className="form">
              <div className="inputs">
                <div className="input">
                  <label>E-mail</label>
                  <input type="email"placeholder="exemplo@email.com" value={this.state.user.email} onChange={this.handleEmailChange}/>
                  {(this.props.auth.error && this.props.auth.error.code === 1002) && (
                    <div className="input-alert error">
                      <div className="svg icon">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 510 510"><path d="M255 0C114.8 0 0 114.8 0 255s114.8 255 255 255 255-114.8 255-255S395.3 0 255 0zm30.4 406.8h-60.7v-60.7h60.7v60.7zm0-121.4h-60.7V103.2h60.7v182.2z"/></svg>
                      </div>
                      <span className="message">E-mail incorreto, tente novamente.</span>
                    </div>
                  )}
                </div>
                <div className="input">
                  <label>Senha</label>
                  <input type="password" placeholder="●●●●●●●" value={this.state.user.password} onChange={this.handlePasswordChange}/>
                  {(this.props.auth.error && this.props.auth.error.code === 1001) && (
                    <div className="input-alert error">
                      <div className="svg icon">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 510 510"><path d="M255 0C114.8 0 0 114.8 0 255s114.8 255 255 255 255-114.8 255-255S395.3 0 255 0zm30.4 406.8h-60.7v-60.7h60.7v60.7zm0-121.4h-60.7V103.2h60.7v182.2z"/></svg>
                      </div>
                      <span className="message">Senha incorreta, tente novamente.</span>
                    </div>
                  )}
                </div>
              </div>
              <div className="action">
                <a href="javascript:void(0)" className="forgot"></a>
                <button className="btn btn-secondary" type="submit">Entrar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

