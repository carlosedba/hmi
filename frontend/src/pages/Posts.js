import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import moment from 'moment'

import { apiFetchAllPosts, apiDeletePost } from '@/actions/posts'

import picture_placeholder from '@/assets/img/picture_placeholder.png'
import ic_sq_plus from '@/assets/img/ic_sq_plus.png'
import ic_sq_home from '@/assets/img/ic_sq_home.png'
import ic_sq_posts from '@/assets/img/ic_sq_posts.png'
import ic_sq_pages from '@/assets/img/ic_sq_pages.png'
import ic_sq_calendar from '@/assets/img/ic_sq_calendar.png'
import ic_sq_settings from '@/assets/img/ic_sq_settings.png'
import ic_sq_exit from '@/assets/img/ic_sq_exit.png'

@connect((state) => {
  return {
    posts: state.posts.list,
  }
}, (dispatch, ownProps) => {
  return {
    apiFetchAllPosts() {
      return dispatch(apiFetchAllPosts())
    },

    apiDeletePost(params) {
      return dispatch(apiDeletePost(params))
    },
  }
})
export default class Control extends Component {
  constructor(props) {
    super(props)

    this.handleDeleteSuccess = this.handleDeleteSuccess.bind(this)
    this.handleDeleteError = this.handleDeleteError.bind(this)
  }

  componentDidMount() {
    this.props.apiFetchAllPosts()
  }

  handleDelete(id, event) {
    event.preventDefault()

    this.props.apiDeletePost({ id: id })
      .then(this.handleDeleteSuccess)
      .catch(this.handleDeleteError)
  }

  handleDeleteSuccess() {
    alert('Item deletado com sucesso!')

    this.props.apiFetchAllPosts()
  }

  handleDeleteError(err) {
    alert('Um erro ocorreu. Por favor contatar o administrador da plataforma.')
  }

  render() {
    const { posts } = this.props

    return (
      <div className="page center posts-page">
        <div className="widget widget-buttons">
          <div className="widget-content">
            <Link className="card card-collection card-collection-secondary" to="/posts/new" style={{backgroundImage: `url('${ic_sq_plus}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Novo Post</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/posts" style={{backgroundImage: `url('${ic_sq_posts}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Posts</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/pages" style={{backgroundImage: `url('${ic_sq_pages}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Páginas</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/events" style={{backgroundImage: `url('${ic_sq_calendar}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Eventos</p>
            </Link>
            <Link className="card card-collection card-collection-secondary" to="/logout" style={{backgroundImage: `url('${ic_sq_exit}')`}}>
              <div className="card-collection-overlay"></div>
              <p className="card-collection-title">Sair</p>
            </Link>
          </div>
        </div>

        <div className="widget widget-alpha widget-posts-list">
          <div className="widget-header">
            <p className="widget-title">Posts</p>
            <p className="widget-description">Clique nos botões para alterar ou deletar uma publicação.</p>
          </div>
          <div className="widget-content">

            <div className="list list-alpha">
              <div className="list-header">
                <span className="list-header-attribute" data-attribute="title"><span>Título</span></span>
                <span className="list-header-attribute" data-attribute="description"><span>Descrição</span></span>
                <span className="list-header-attribute" data-attribute="updated_at"><span>Data de modificação</span></span>
                <span className="list-header-attribute" data-attribute="author"><span>Autor</span></span>
              </div>
              <div className="list-rows">
                <Link className="new-item" to="/posts/new">
                  <div className="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42 42">
                      <path fill="#000" d="M42 19H23V0h-4v19H0v4h19v19h4V23h19"/>
                    </svg>
                  </div>
                  <span className="text">Nova publicação</span>
                </Link>
              {(posts.items) && posts.items.map((post, i) => {
                return (
                  <div key={i} className="list-row">
                    <div className="left">
                      <div className="list-row-attribute" data-attribute="title">
                        <div className="value">{post.title}</div>
                      </div>
                      <div className="list-row-attribute" data-attribute="description">
                        <div className="value">{post.description}</div>
                      </div>
                      <div className="list-row-attribute" data-attribute="updated_at">
                        <div className="value">{moment(post.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY HH:mm')}</div>
                      </div>
                      <div className="list-row-attribute" data-attribute="author">
                        <div className="value">{post.user.first_name}</div>
                      </div>
                    </div>
                    <div className="right">
                      <div className="actions">
                        <Link className="action" to={`/posts/${post.id}`}>
                          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 432.5 432.5">
                            <path d="M0 313.8v118.7h118.8L356.3 195 237.5 76.2 0 313.8zM103.6 396H73v-36.5H36.6V329l26-26 67 67-26 26zm143-271.2c4.3 0 6.4 2 6.4 6.2 0 2-.7 3.6-2 5L96.2 290.6c-1.3 1.3-3 2-4.8 2-4.2 0-6.3-2.2-6.3-6.3 0-2 .7-3.6 2-5L242 127c1.4-1.4 3-2 5-2zM422 77.7l-67-67C347.6 3.8 339 0 329 0c-10.4 0-19 3.6-25.8 10.8L255.8 58l118.8 118.7 47.4-47.4c7-7 10.5-15.6 10.5-25.7 0-10-3.5-18.5-10.5-26z"/>
                          </svg>
                        </Link>
                        <a className="action" href="javascript:void(0)" onClick={this.handleDelete.bind(this, post.id)}>
                          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path fill="#000" d="M425.3 51.4h-91.5V16.7C333.8 7.5 326.4 0 317 0H195C185.5 0 178 7.5 178 16.7v34.7H86.7C77.5 51.4 70 58.8 70 68v51.4c0 9.2 7.5 16.7 16.7 16.7h338.6c9.2 0 16.7-7.4 16.7-16.6V68c0-9.2-7.5-16.6-16.7-16.6zm-125 0h-88.8v-18h89v18zM93.2 169.5L107 496c.4 9 7.8 16 16.7 16h264.6c9 0 16.3-7 16.7-16l13.8-326.5H93.2zM205.5 444c0 9.3-7.4 16.8-16.7 16.8-9.2 0-16.7-7.5-16.7-16.7V237.5c0-9.2 7.6-16.7 16.8-16.7 9.3 0 16.7 7.5 16.7 16.7V444zm67.2 0c0 9.3-7.5 16.8-16.7 16.8s-16.7-7.5-16.7-16.7V237.5c0-9.2 7.5-16.7 16.7-16.7s16.7 7.5 16.7 16.7V444zm67.2 0c0 9.3-7.6 16.8-16.8 16.8s-16.7-7.5-16.7-16.7V237.5c0-9.2 7.4-16.7 16.7-16.7s16.7 7.5 16.7 16.7V444z"/>
                          </svg>
                        </a>
                      </div>
                    </div>
                  </div>
                )
              })}
              </div>
            </div>

          </div>
        </div>
      </div>
    )
  }
}

