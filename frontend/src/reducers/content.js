import {
	API_FETCH_ALL_CONTENT, API_FETCH_ALL_CONTENT_LOADING, API_FETCH_ALL_CONTENT_SUCCESS, API_FETCH_ALL_CONTENT_ERROR,
	API_FETCH_BY_COMPONENT, API_FETCH_BY_COMPONENT_LOADING, API_FETCH_BY_COMPONENT_SUCCESS, API_FETCH_BY_COMPONENT_ERROR,
	API_FETCH_CONTENT, API_FETCH_CONTENT_LOADING, API_FETCH_CONTENT_SUCCESS, API_FETCH_CONTENT_ERROR,
	API_CREATE_CONTENT, API_CREATE_CONTENT_LOADING, API_CREATE_CONTENT_SUCCESS, API_CREATE_CONTENT_ERROR,
	API_UPDATE_CONTENT, API_UPDATE_CONTENT_LOADING, API_UPDATE_CONTENT_SUCCESS, API_UPDATE_CONTENT_ERROR,
	API_DELETE_CONTENT, API_DELETE_CONTENT_LOADING, API_DELETE_CONTENT_SUCCESS, API_DELETE_CONTENT_ERROR,
	CHANGE_ACTIVE_CONTENT, CHANGE_NEW_CONTENT, CHANGE_UPDATED_CONTENT
} from '@/constants/ActionTypes'

const INITIAL_STATE = {
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 				{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case API_FETCH_ALL_CONTENT:
		return { ...state, list: { items: [], error: null, loading: null } }

	case API_FETCH_ALL_CONTENT_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case API_FETCH_ALL_CONTENT_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case API_FETCH_ALL_CONTENT_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case API_FETCH_BY_COMPONENT:
		return { ...state, list: { items: [], error: null, loading: null } }

	case API_FETCH_BY_COMPONENT_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case API_FETCH_BY_COMPONENT_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case API_FETCH_BY_COMPONENT_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case API_FETCH_CONTENT:
		return { ...state, active: { item: null, error: null, loading: null } }

	case API_FETCH_CONTENT_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case API_FETCH_CONTENT_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case API_FETCH_CONTENT_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case API_CREATE_CONTENT:
		return { ...state, new: { item: null, error: null, loading: null } }

	case API_CREATE_CONTENT_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case API_CREATE_CONTENT_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case API_CREATE_CONTENT_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case API_UPDATE_CONTENT:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case API_UPDATE_CONTENT_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case API_UPDATE_CONTENT_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case API_UPDATE_CONTENT_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case API_DELETE_CONTENT:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case API_DELETE_CONTENT_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case API_DELETE_CONTENT_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case API_DELETE_CONTENT_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }


	case CHANGE_ACTIVE_CONTENT:
		return { ...state, active: { item: action.payload, error: null, loading: null } }


	case CHANGE_NEW_CONTENT:
		return { ...state, new: { item: action.payload, error: null, loading: null } }


	case CHANGE_UPDATED_CONTENT:
		return { ...state, updated: { item: action.payload, error: null, loading: null } }

	default:
		return state;
  }
}
