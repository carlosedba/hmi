import {
	API_FETCH_ALL_PAGES, API_FETCH_ALL_PAGES_LOADING, API_FETCH_ALL_PAGES_SUCCESS, API_FETCH_ALL_PAGES_ERROR,
	API_FETCH_PAGE, API_FETCH_PAGE_LOADING, API_FETCH_PAGE_SUCCESS, API_FETCH_PAGE_ERROR,
	API_CREATE_PAGE, API_CREATE_PAGE_LOADING, API_CREATE_PAGE_SUCCESS, API_CREATE_PAGE_ERROR,
	API_UPDATE_PAGE, API_UPDATE_PAGE_LOADING, API_UPDATE_PAGE_SUCCESS, API_UPDATE_PAGE_ERROR,
	API_DELETE_PAGE, API_DELETE_PAGE_LOADING, API_DELETE_PAGE_SUCCESS, API_DELETE_PAGE_ERROR,
	CHANGE_ACTIVE_PAGE, CHANGE_NEW_PAGE, CHANGE_UPDATED_PAGE
} from '@/constants/ActionTypes'

const INITIAL_STATE = {
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 				{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case API_FETCH_ALL_PAGES:
		return { ...state, list: { items: [], error: null, loading: null } }

	case API_FETCH_ALL_PAGES_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case API_FETCH_ALL_PAGES_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case API_FETCH_ALL_PAGES_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case API_FETCH_PAGE:
		return { ...state, active: { item: null, error: null, loading: null } }

	case API_FETCH_PAGE_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case API_FETCH_PAGE_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case API_FETCH_PAGE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case API_CREATE_PAGE:
		return { ...state, new: { item: null, error: null, loading: null } }

	case API_CREATE_PAGE_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case API_CREATE_PAGE_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case API_CREATE_PAGE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case API_UPDATE_PAGE:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case API_UPDATE_PAGE_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case API_UPDATE_PAGE_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case API_UPDATE_PAGE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case API_DELETE_PAGE:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case API_DELETE_PAGE_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case API_DELETE_PAGE_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case API_DELETE_PAGE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }


	case CHANGE_ACTIVE_PAGE:
		return { ...state, active: { item: action.payload, error: null, loading: null } }


	case CHANGE_NEW_PAGE:
		return { ...state, new: { item: action.payload, error: null, loading: null } }


	case CHANGE_UPDATED_PAGE:
		return { ...state, updated: { item: action.payload, error: null, loading: null } }

	default:
		return state;
  }
}
