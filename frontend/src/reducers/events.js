import {
  API_FETCH_ALL_EVENTS, API_FETCH_ALL_EVENTS_LOADING, API_FETCH_ALL_EVENTS_SUCCESS, API_FETCH_ALL_EVENTS_ERROR,
  API_FETCH_EVENT, API_FETCH_EVENT_LOADING, API_FETCH_EVENT_SUCCESS, API_FETCH_EVENT_ERROR,
  API_CREATE_EVENT, API_CREATE_EVENT_LOADING, API_CREATE_EVENT_SUCCESS, API_CREATE_EVENT_ERROR,
  API_UPDATE_EVENT, API_UPDATE_EVENT_LOADING, API_UPDATE_EVENT_SUCCESS, API_UPDATE_EVENT_ERROR,
  API_DELETE_EVENT, API_DELETE_EVENT_LOADING, API_DELETE_EVENT_SUCCESS, API_DELETE_EVENT_ERROR,
  CHANGE_ACTIVE_EVENT, CHANGE_NEW_EVENT, CHANGE_UPDATED_EVENT
} from '@/constants/ActionTypes'

const INITIAL_STATE = {
  list:       { items: [], error: null, loading: null },
  active:     { item: null, error: null, loading: null },
  new:        { item: null, error: null, loading: null },
  updated:    { item: null, error: null, loading: null },
  deleted:    { item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
  case API_FETCH_ALL_EVENTS:
    return { ...state, list: { items: [], error: null, loading: null } }

  case API_FETCH_ALL_EVENTS_LOADING:
    return { ...state, list: { items: [], error: null, loading: true } }

  case API_FETCH_ALL_EVENTS_SUCCESS:
    return { ...state, list: { items: action.payload.data, error: null, loading: false } }

  case API_FETCH_ALL_EVENTS_ERROR:
    error = action.payload.data || { message: action.payload.message }
    return { ...state, list: { items: [], error: error, loading: false } }


  case API_FETCH_EVENT:
    return { ...state, active: { item: null, error: null, loading: null } }

  case API_FETCH_EVENT_LOADING:
    return { ...state, active: { item: null, error: null, loading: true } }

  case API_FETCH_EVENT_SUCCESS:
    return { ...state, active: { item: action.payload.data, error: null, loading: false } }

  case API_FETCH_EVENT_ERROR:
    error = action.payload.data || { message: action.payload.message }
    return { ...state, active: { item: null, error: error, loading: false } }


  case API_CREATE_EVENT:
    return { ...state, new: { item: null, error: null, loading: null } }

  case API_CREATE_EVENT_LOADING:
    return { ...state, new: { item: null, error: null, loading: true } }

  case API_CREATE_EVENT_SUCCESS:
    return { ...state, new: { item: action.payload.data, error: null, loading: false } }

  case API_CREATE_EVENT_ERROR:
    error = action.payload.data || { message: action.payload.message }
    return { ...state, new: { item: null, error: error, loading: false } }


  case API_UPDATE_EVENT:
    return { ...state, updated: { item: null, error: null, loading: null } }

  case API_UPDATE_EVENT_LOADING:
    return { ...state, updated: { item: null, error: null, loading: true } }

  case API_UPDATE_EVENT_SUCCESS:
    return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

  case API_UPDATE_EVENT_ERROR:
    error = action.payload.data || { message: action.payload.message }
    return { ...state, updated: { item: null, error: error, loading: false } }


  case API_DELETE_EVENT:
    return { ...state, deleted: { item: null, error: null, loading: null } }

  case API_DELETE_EVENT_LOADING:
    return { ...state, deleted: { item: null, error: null, loading: true } }

  case API_DELETE_EVENT_SUCCESS:
    return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

  case API_DELETE_EVENT_ERROR:
    error = action.payload.data || { message: action.payload.message }
    return { ...state, deleted: { item: null, error: error, loading: false } }


  case CHANGE_ACTIVE_EVENT:
    return { ...state, active: { item: action.payload, error: null, loading: null } }


  case CHANGE_NEW_EVENT:
    return { ...state, new: { item: action.payload, error: null, loading: null } }


  case CHANGE_UPDATED_EVENT:
    return { ...state, updated: { item: action.payload, error: null, loading: null } }

  default:
    return state;
  }
}
