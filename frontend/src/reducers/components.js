import {
  API_FETCH_COMPONENTS, API_FETCH_COMPONENTS_LOADING, API_FETCH_COMPONENTS_SUCCESS, API_FETCH_COMPONENTS_ERROR,
  API_FETCH_COMPONENT, API_FETCH_COMPONENT_LOADING, API_FETCH_COMPONENT_SUCCESS, API_FETCH_COMPONENT_ERROR,
  API_CREATE_COMPONENT, API_CREATE_COMPONENT_LOADING, API_CREATE_COMPONENT_SUCCESS, API_CREATE_COMPONENT_ERROR,
  API_UPDATE_COMPONENT, API_UPDATE_COMPONENT_LOADING, API_UPDATE_COMPONENT_SUCCESS, API_UPDATE_COMPONENT_ERROR,
  API_DELETE_COMPONENT, API_DELETE_COMPONENT_LOADING, API_DELETE_COMPONENT_SUCCESS, API_DELETE_COMPONENT_ERROR
} from '@/constants/ActionTypes'

const INITIAL_STATE = {
  list:       { items: [], error: null, loading: null },
  active:     { item: null, error: null, loading: null },
  new:        { item: null, error: null, loading: null },
  updated:    { item: null, error: null, loading: null },
  deleted:    { item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
  case API_FETCH_COMPONENTS:
    return { ...state, list: { items: [], error: null, loading: null } }

  case API_FETCH_COMPONENTS_LOADING:
    return { ...state, list: { items: [], error: null, loading: true } }

  case API_FETCH_COMPONENTS_SUCCESS:
    return { ...state, list: { items: action.payload.data, error: null, loading: false } }

  case API_FETCH_COMPONENTS_ERROR:
    error = action.payload.data || { message: action.payload.message }
    return { ...state, list: { items: [], error: error, loading: false } }


  case API_FETCH_COMPONENT:
    return { ...state, active: { item: null, error: null, loading: null } }

  case API_FETCH_COMPONENT_LOADING:
    return { ...state, active: { item: null, error: null, loading: true } }

  case API_FETCH_COMPONENT_SUCCESS:
    return { ...state, active: { item: action.payload.data, error: null, loading: false } }

  case API_FETCH_COMPONENT_ERROR:
    error = action.payload.data || { message: action.payload.message }
    return { ...state, active: { item: null, error: error, loading: false } }


  case API_CREATE_COMPONENT:
    return { ...state, new: { item: null, error: null, loading: null } }

  case API_CREATE_COMPONENT_LOADING:
    return { ...state, new: { item: null, error: null, loading: true } }

  case API_CREATE_COMPONENT_SUCCESS:
    return { ...state, new: { item: action.payload.data, error: null, loading: false } }

  case API_CREATE_COMPONENT_ERROR:
    error = action.payload.data || { message: action.payload.message }
    return { ...state, new: { item: null, error: error, loading: false } }


  case API_UPDATE_COMPONENT:
    return { ...state, updated: { item: null, error: null, loading: null } }

  case API_UPDATE_COMPONENT_LOADING:
    return { ...state, updated: { item: null, error: null, loading: true } }

  case API_UPDATE_COMPONENT_SUCCESS:
    return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

  case API_UPDATE_COMPONENT_ERROR:
    error = action.payload.data || { message: action.payload.message }
    return { ...state, updated: { item: null, error: error, loading: false } }


  case API_DELETE_COMPONENT:
    return { ...state, deleted: { item: null, error: null, loading: null } }

  case API_DELETE_COMPONENT_LOADING:
    return { ...state, deleted: { item: null, error: null, loading: true } }

  case API_DELETE_COMPONENT_SUCCESS:
    return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

  case API_DELETE_COMPONENT_ERROR:
    error = action.payload.data || { message: action.payload.message }
    return { ...state, deleted: { item: null, error: error, loading: false } }

  default:
    return state;
  }
}
