import {
	API_FETCH_ALL_POSTS, API_FETCH_ALL_POSTS_LOADING, API_FETCH_ALL_POSTS_SUCCESS, API_FETCH_ALL_POSTS_ERROR,
	API_FETCH_POST, API_FETCH_POST_LOADING, API_FETCH_POST_SUCCESS, API_FETCH_POST_ERROR,
	API_CREATE_POST, API_CREATE_POST_LOADING, API_CREATE_POST_SUCCESS, API_CREATE_POST_ERROR,
	API_UPDATE_POST, API_UPDATE_POST_LOADING, API_UPDATE_POST_SUCCESS, API_UPDATE_POST_ERROR,
	API_DELETE_POST, API_DELETE_POST_LOADING, API_DELETE_POST_SUCCESS, API_DELETE_POST_ERROR,
	CHANGE_ACTIVE_POST, CHANGE_NEW_POST, CHANGE_UPDATED_POST
} from '@/constants/ActionTypes'

const INITIAL_STATE = {
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 				{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case API_FETCH_ALL_POSTS:
		return { ...state, list: { items: [], error: null, loading: null } }

	case API_FETCH_ALL_POSTS_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case API_FETCH_ALL_POSTS_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case API_FETCH_ALL_POSTS_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case API_FETCH_POST:
		return { ...state, active: { item: null, error: null, loading: null } }

	case API_FETCH_POST_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case API_FETCH_POST_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case API_FETCH_POST_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case API_CREATE_POST:
		return { ...state, new: { item: null, error: null, loading: null } }

	case API_CREATE_POST_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case API_CREATE_POST_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case API_CREATE_POST_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case API_UPDATE_POST:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case API_UPDATE_POST_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case API_UPDATE_POST_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case API_UPDATE_POST_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case API_DELETE_POST:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case API_DELETE_POST_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case API_DELETE_POST_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case API_DELETE_POST_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }


	case CHANGE_ACTIVE_POST:
		return { ...state, active: { item: action.payload, error: null, loading: null } }


	case CHANGE_NEW_POST:
		return { ...state, new: { item: action.payload, error: null, loading: null } }


	case CHANGE_UPDATED_POST:
		return { ...state, updated: { item: action.payload, error: null, loading: null } }

	default:
		return state;
  }
}
