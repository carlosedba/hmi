import { combineReducers } from 'redux'

import session from './session'
import users from './users'

import components from '@/reducers/components'
import content from '@/reducers/content'
import pages from '@/reducers/pages'
import posts from '@/reducers/posts'
import events from '@/reducers/events'
import eventSubscriptions from '@/reducers/eventSubscriptions'

const reducer = combineReducers({
  session,
  users,
  components,
  content,
  pages,
  posts,
  events,
  eventSubscriptions,
})

export default reducer

