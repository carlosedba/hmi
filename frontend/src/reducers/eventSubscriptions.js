import {
	API_FETCH_ALL_EVENT_SUBSCRIPTIONS, API_FETCH_ALL_EVENT_SUBSCRIPTIONS_LOADING, API_FETCH_ALL_EVENT_SUBSCRIPTIONS_SUCCESS, API_FETCH_ALL_EVENT_SUBSCRIPTIONS_ERROR,
	API_FETCH_EVENT_SUBSCRIPTION, API_FETCH_EVENT_SUBSCRIPTION_LOADING, API_FETCH_EVENT_SUBSCRIPTION_SUCCESS, API_FETCH_EVENT_SUBSCRIPTION_ERROR,
	API_CREATE_EVENT_SUBSCRIPTION, API_CREATE_EVENT_SUBSCRIPTION_LOADING, API_CREATE_EVENT_SUBSCRIPTION_SUCCESS, API_CREATE_EVENT_SUBSCRIPTION_ERROR,
	API_UPDATE_EVENT_SUBSCRIPTION, API_UPDATE_EVENT_SUBSCRIPTION_LOADING, API_UPDATE_EVENT_SUBSCRIPTION_SUCCESS, API_UPDATE_EVENT_SUBSCRIPTION_ERROR,
	API_DELETE_EVENT_SUBSCRIPTION, API_DELETE_EVENT_SUBSCRIPTION_LOADING, API_DELETE_EVENT_SUBSCRIPTION_SUCCESS, API_DELETE_EVENT_SUBSCRIPTION_ERROR,
	CHANGE_ACTIVE_EVENT_SUBSCRIPTION, CHANGE_NEW_EVENT_SUBSCRIPTION, CHANGE_UPDATED_EVENT_SUBSCRIPTION
} from '@/constants/ActionTypes'

const INITIAL_STATE = {
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 				{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case API_FETCH_ALL_EVENT_SUBSCRIPTIONS:
		return { ...state, list: { items: [], error: null, loading: null } }

	case API_FETCH_ALL_EVENT_SUBSCRIPTIONS_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case API_FETCH_ALL_EVENT_SUBSCRIPTIONS_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case API_FETCH_ALL_EVENT_SUBSCRIPTIONS_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case API_FETCH_EVENT_SUBSCRIPTION:
		return { ...state, active: { item: null, error: null, loading: null } }

	case API_FETCH_EVENT_SUBSCRIPTION_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case API_FETCH_EVENT_SUBSCRIPTION_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case API_FETCH_EVENT_SUBSCRIPTION_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case API_CREATE_EVENT_SUBSCRIPTION:
		return { ...state, new: { item: null, error: null, loading: null } }

	case API_CREATE_EVENT_SUBSCRIPTION_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case API_CREATE_EVENT_SUBSCRIPTION_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case API_CREATE_EVENT_SUBSCRIPTION_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case API_UPDATE_EVENT_SUBSCRIPTION:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case API_UPDATE_EVENT_SUBSCRIPTION_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case API_UPDATE_EVENT_SUBSCRIPTION_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case API_UPDATE_EVENT_SUBSCRIPTION_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case API_DELETE_EVENT_SUBSCRIPTION:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case API_DELETE_EVENT_SUBSCRIPTION_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case API_DELETE_EVENT_SUBSCRIPTION_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case API_DELETE_EVENT_SUBSCRIPTION_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }


	case CHANGE_ACTIVE_EVENT_SUBSCRIPTION:
		return { ...state, active: { item: action.payload, error: null, loading: null } }


	case CHANGE_NEW_EVENT_SUBSCRIPTION:
		return { ...state, new: { item: action.payload, error: null, loading: null } }


	case CHANGE_UPDATED_EVENT_SUBSCRIPTION:
		return { ...state, updated: { item: action.payload, error: null, loading: null } }

	default:
		return state;
  }
}
