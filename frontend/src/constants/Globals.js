export const PRODUCTION = true
export const SERVER_PORT = location.port
export const SERVER_ADDRESS = (PRODUCTION) ? 'http://hmi.net.br' : `https://server.com/hmi/server/public_html/dev.php`
export const API_ENDPOINT = `${SERVER_ADDRESS}/api/v1`
export const HMI_API_ENDPOINT = `${SERVER_ADDRESS}/api/hmi`

