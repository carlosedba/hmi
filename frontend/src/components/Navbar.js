import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { SERVER_ADDRESS } from '@/constants/Globals'

import '../assets/css/navbar.css'

@connect((state) => {
  return {
    user: state.session.user
  }
})
export default class Navbar extends Component {
  constructor(props) {
    super(props)
  }

  getUserDataLink(filename) {
    return `${SERVER_ADDRESS}/data/user_data/${filename}`
  }

  getContentDataLink(filename) {
    return `${SERVER_ADDRESS}/data/content_data/${filename}`
  }

  render() {
    return (
      <nav styleName="navbar">
        <div styleName="navbar-left"></div>
        <div styleName="navbar-center">
          <div styleName="navbar-logo">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 555 400">
              <path d="M550 394.9l-117.1-169L280.1 5.2l-135 195.6L4.3 395.2l287.3-113.5-25.2 64.7z"/>
            </svg>
          </div>
        </div>
        <div styleName="navbar-right">
          <div styleName="navbar-user">
            <div styleName="navbar-user-picture" style={{ backgroundImage: `url(${this.props.user.data && this.props.user.data.picture && this.getUserDataLink(this.props.user.data.picture)})`}}></div>
          </div>
        </div>
      </nav>
    )
  }
}

