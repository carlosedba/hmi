import React, { Component } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import update from 'immutability-helper'
import { CSSTransitionGroup } from 'react-transition-group'
import store from 'store'

import Navbar from '@/components/Navbar'

import Logout from '@/pages/Logout'
import Control from '@/pages/Control'
import Inputs from '@/pages/Inputs'

import ComPatrocinadoresCreate from '@/pages/ComPatrocinadoresCreate'
import ComPatrocinadoresUpdate from '@/pages/ComPatrocinadoresUpdate'

import ComOQueOferecemosCreate from '@/pages/ComOQueOferecemosCreate'
import ComOQueOferecemosUpdate from '@/pages/ComOQueOferecemosUpdate'

import ComCasesCreate from '@/pages/ComCasesCreate'
import ComCasesUpdate from '@/pages/ComCasesUpdate'

import ComSliderCreate from '@/pages/ComSliderCreate'
import ComSliderUpdate from '@/pages/ComSliderUpdate'

import ComEquipeCreate from '@/pages/ComEquipeCreate'
import ComEquipeUpdate from '@/pages/ComEquipeUpdate'

import Posts from '@/pages/Posts'
import NewPost from '@/pages/NewPost'
import UpdatePost from '@/pages/UpdatePost'

import Pages from '@/pages/Pages'
import PageQuemSomos from '@/pages/PageQuemSomos'
import PageOQueOferecemos from '@/pages/PageOQueOferecemos'
import PageEventos from '@/pages/PageEventos'

import Events from '@/pages/Events'
import NewEvent from '@/pages/NewEvent'
import UpdateEvent from '@/pages/UpdateEvent'
import EventSubscriptions from '@/pages/EventSubscriptions'

import { apiFetchComponents } from '@/actions/components'
import { apiFetchAllContent } from '@/actions/content'

@connect((state) => {
  return {
    pending: state.session.auth.pending,
    authenticated: state.session.auth.authenticated,
  }
}, (dispatch, ownProps) => {
  return {}
})
export default class AuthorizedLayout extends Component {
  constructor(props) {
    super(props)

    this.state = {
      location: null,
    }
  }

  componentDidMount() {
    console.log('AuthorizedLayout > componentDidMount called!')

    const { location } = this.props

    this.setLocation(location)
  }

  componentWillReceiveProps(nextProps) {
    const { location } = nextProps

    this.setLocation(location)
  } 

  setLocation(location) {
    this.setState((prevState, props) => {
      return update(prevState, {
        $merge: { location: location }
      })
    })
  }

  render() {
    return (
      <div className="app">
        <header className="header">
          <Navbar location={this.state.location} user={this.state.user}/>
        </header>

        <Switch>
          <Route path="/events/new" component={NewEvent}/>
          <Route path="/events/:id/subscriptions" component={EventSubscriptions}/>
          <Route path="/events/:id" component={UpdateEvent}/>
          <Route path="/events" component={Events}/>

          <Route path="/posts/new" component={NewPost}/>
          <Route path="/posts/:id" component={UpdatePost}/>
          <Route path="/posts" component={Posts}/>

          <Route path="/pages/eventos" component={PageEventos}/>
          <Route path="/pages/o-que-oferecemos" component={PageOQueOferecemos}/>
          <Route path="/pages/quem-somos" component={PageQuemSomos}/>
          <Route path="/pages" component={Pages}/>

          <Route path="/components/equipe/update/:id" component={ComEquipeUpdate}/>
          <Route path="/components/equipe/create" component={ComEquipeCreate}/>

          <Route path="/components/slider/update/:id" component={ComSliderUpdate}/>
          <Route path="/components/slider/create" component={ComSliderCreate}/>
          
          <Route path="/components/cases/update/:id" component={ComCasesUpdate}/>
          <Route path="/components/cases/create" component={ComCasesCreate}/>
          
          <Route path="/components/o-que-oferecemos/update/:id" component={ComOQueOferecemosUpdate}/>
          <Route path="/components/o-que-oferecemos/create" component={ComOQueOferecemosCreate}/>
          
          <Route path="/components/patrocinadores/update/:id" component={ComPatrocinadoresUpdate}/>
          <Route path="/components/patrocinadores/create" component={ComPatrocinadoresCreate}/>
          
          <Route path="/inputs" component={Inputs}/>

          <Route path="/" component={Control}/>
        </Switch>
      </div>
    )
  }
}
