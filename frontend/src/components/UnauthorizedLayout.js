import React, { Component } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import Login from '@/pages/Login'
import Logout from '@/pages/Logout'

export default class UnauthorizedLayout extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div>
        <header className="header"></header>

        <Switch>
          <Route path="/logout" component={Logout}/>
          <Route path="/login" component={Login}/>
          <Redirect to="/login"/>
        </Switch>
      </div>
    )
  }
}