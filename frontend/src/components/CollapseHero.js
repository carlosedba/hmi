import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import classNames from 'classnames'

import { SERVER_ADDRESS } from '../constants/Globals'

import { apiFetchContent, apiUpdateContent, changeActiveContent } from '@/actions/content'

import Collapse from '@/components/Collapse'

import picture_placeholder from '@/assets/img/picture_placeholder.png'

@connect((state) => {
  return {
    content: state.content.active,
  }
}, (dispatch, ownProps) => {
  return {
    apiFetchContent(params) {
      return dispatch(apiFetchContent(params))
    },

    apiUpdateContent(params, props) {
      return dispatch(apiUpdateContent(params, props))
    },

    changeActiveContent(props) {
      return dispatch(changeActiveContent(props))
    },
  }
})
export default class CollapseHero extends Component {
  constructor(props) {
    super(props)

    this.formImagem = React.createRef()

    this.state = {
      form: {
        title: '',
        picture: {
          filename: picture_placeholder,
          hash: ''
        }
      },
    }

    this.handleTituloChange = this.handleTituloChange.bind(this)
    this.handlePictureChange = this.handlePictureChange.bind(this)
    this.handleSave = this.handleSave.bind(this)
    this.handleSaveSuccess = this.handleSaveSuccess.bind(this)
    this.handleSaveError = this.handleSaveError.bind(this)
  }

  componentDidMount() {
    console.log('token', store.get('token'))
    this.props.apiFetchContent({ id: '67ab33ae' })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.open !== this.props.open) this.props.apiFetchContent({ id: '67ab33ae' })
  }

  getUserDataLink(filename) {
    return `${SERVER_ADDRESS}/data/user_data/${filename}`
  }

  getContentDataLink(filename) {
    return `${SERVER_ADDRESS}/data/content_data/${filename}`
  }

  handleTituloChange(event) {
    const { content } = this.props
    const value = event.target.value

    if (value !== undefined && value !== null) {
      this.props.changeActiveContent(update(content.item, {
          data: { $merge: { title: value } }
      }))
    }
  }

  handlePictureChange(event) {
    const target = event.currentTarget
    const previewPicture = target.parentNode

    for (let i = 0; i < target.files.length; i++) {
      if (i === 0) {
        const file = target.files[i]

        if (!file.type.startsWith('image/')){ continue }

        const reader = new FileReader()
        reader.onload = function (event) {
          previewPicture.style.backgroundImage = `url(${event.target.result})`
        }
        reader.readAsDataURL(file)
      }
    }
  }

  handleSave(event) {
    const { content } = this.props
    const data = new FormData()

    for (var property in content.item) {
      if (content.item.hasOwnProperty(property)) {
        if (property === 'data') data.append(property, JSON.stringify(content.item[property]))
      }
    }

    if (this.formImagem.current.files && this.formImagem.current.files[0]) {
      data.append('picture', this.formImagem.current.files[0])
    }

    this.props.apiUpdateContent({ id: content.item.id }, data)
      .then(this.handleSaveSuccess)
      .catch(this.handleSaveError)
  }

  handleSaveSuccess(result) {
    //this.resetForm()
    alert('Dados salvos com sucesso!')
  }

  handleSaveError(err) {
    //this.resetForm()
    alert('Um erro ocorreu. Por favor contatar o administrador da plataforma.')
  }

  resetForm() {
    this.formImagem.current.parentNode.style.backgroundImage = `url(${picture_placeholder})`

    this.setState((prevState, props) => {
      return update(prevState, {
        $merge: { form: {
          title: ''
        } }
      })
    })

  }

  render() {
    const { content } = this.props

    return (
      <div className="collapse-content">
        <div className="collapse-row">
          <div className="left">
            <div className="inputs">
              <div className="input-picture-alpha">
                <label>Imagem:</label>
                <div className="preview-picture" style={{ backgroundImage: `url(${content.item && content.item.data.picture && this.getContentDataLink(content.item.data.picture.filename) || picture_placeholder})`}} ref={this.previewPicture}>
                  <div className="preview-picture-overlay"></div>
                  <p className="preview-picture-description">Clique para alterar</p>
                  <input type="file" ref={this.formImagem} onChange={this.handlePictureChange}/>
                </div>
              </div>
            </div>
          </div>
          <div className="right">
            <div className="inputs">
              <div className="input w4-q1">
                <label>Título:</label>
                <input type="text" value={content.item && content.item.data && content.item.data.title || ''} onChange={this.handleTituloChange}/>
              </div>
            </div>
          </div>
        </div>
        <div className="collapse-row">
          <div className="buttons">
            <button className="btn btn-one" onClick={this.handleSave}>Salvar</button>
          </div>
        </div>
      </div>
    )
  }
}




