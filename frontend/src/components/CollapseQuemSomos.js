import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import classNames from 'classnames'

import { apiFetchContent, apiUpdateContent, changeActiveContent } from '@/actions/content'

import Collapse from '@/components/Collapse'

import picture_placeholder from '@/assets/img/picture_placeholder.png'

@connect((state) => {
  return {
    content: state.content.active,
  }
}, (dispatch, ownProps) => {
  return {
    apiFetchContent(params) {
      return dispatch(apiFetchContent(params))
    },

    apiUpdateContent(params, props) {
      return dispatch(apiUpdateContent(params, props))
    },

    changeActiveContent(props) {
      return dispatch(changeActiveContent(props))
    },
  }
})
export default class CollapseQuemSomos extends Component {
  constructor(props) {
    super(props)

    this.formImagem = React.createRef()

    this.state = {
      form: {
        title: '',
        picture: {
          filename: picture_placeholder,
          hash: ''
        }
      },
    }

    this.handleTitleChange = this.handleTitleChange.bind(this)
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this)
    this.handleBoxTextChange = this.handleBoxTextChange.bind(this)
    this.handleBoxLinkChange = this.handleBoxLinkChange.bind(this)
    this.handleSave = this.handleSave.bind(this)
    this.handleSaveSuccess = this.handleSaveSuccess.bind(this)
    this.handleSaveError = this.handleSaveError.bind(this)
  }

  componentDidMount() {
    this.props.apiFetchContent({ id: '0689c90d' })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.open !== this.props.open) this.props.apiFetchContent({ id: '0689c90d' })
  }

  handleTitleChange(event) {
    const { content } = this.props
    const value = event.target.value

    if (value !== undefined && value !== null) {
      this.props.changeActiveContent(update(content.item, {
          data: { $merge: { title: value } }
      }))
    }
  }

  handleDescriptionChange(event) {
    const { content } = this.props
    const value = event.target.value

    if (value !== undefined && value !== null) {
      this.props.changeActiveContent(update(content.item, {
          data: { $merge: { description: value } }
      }))
    }
  }

  handleBoxTextChange(event) {
    const { content } = this.props
    const value = event.target.value

    if (value !== undefined && value !== null) {
      this.props.changeActiveContent(update(content.item, {
          data: { box: { $merge: { text: value } } }
      }))
    }
  }

  handleBoxLinkChange(event) {
    const { content } = this.props
    const value = event.target.value

    if (value !== undefined && value !== null) {
      this.props.changeActiveContent(update(content.item, {
          data: { box: { $merge: { link: value } } }
      }))
    }
  }

  handleSave(event) {
    const { content } = this.props
    const data = new FormData()

    for (var property in content.item) {
      if (content.item.hasOwnProperty(property)) {
        if (property === 'data') data.append(property, JSON.stringify(content.item[property]))
      }
    }
    
    this.props.apiUpdateContent({ id: content.item.id }, data)
      .then(this.handleSaveSuccess)
      .catch(this.handleSaveError)
  }

  handleSaveSuccess(result) {
    //this.resetForm()
    alert('Dados salvos com sucesso!')
  }

  handleSaveError(err) {
    //this.resetForm()
    alert('Um erro ocorreu. Por favor contatar o administrador da plataforma.')
  }

  resetForm() {
    this.formImagem.current.parentNode.style.backgroundImage = `url(${picture_placeholder})`

    this.setState((prevState, props) => {
      return update(prevState, {
        $merge: { form: {
          title: ''
        } }
      })
    })

  }

  render() {
    const { content } = this.props

    return (
      <div className="collapse-content">
        <div className="collapse-row">
          <div className="left">
            <div className="inputs">
              <div className="input w3">
                <label>Título:</label>
                <input type="text" value={content.item && content.item.data && content.item.data.title || ''} onChange={this.handleTitleChange}/>
              </div>
              <div className="input w5">
                <label>Descrição:</label>
                <textarea value={content.item && content.item.data && content.item.data.description || ''} onChange={this.handleDescriptionChange}></textarea>
              </div>
              <div className="input w4">
                <label>Texto do Box:</label>
                <textarea value={content.item && content.item.data && content.item.data.box && content.item.data.box.text || ''} onChange={this.handleBoxTextChange}></textarea>
              </div>
            </div>
          </div>
          <div className="right"></div>
        </div>
        <div className="collapse-row">
          <div className="buttons">
            <button className="btn btn-one" onClick={this.handleSave}>Salvar</button>
          </div>
        </div>
      </div>
    )
  }
}




