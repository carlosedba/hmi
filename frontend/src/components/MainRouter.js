import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BrowserRouter, HashRouter, Switch, Route, Redirect, Link } from 'react-router-dom'

import { SERVER_ADDRESS } from '@/constants/Globals'

import AuthorizedRoute from '@/components/AuthorizedRoute'
import UnauthorizedLayout from '@/components/UnauthorizedLayout'
import AuthorizedLayout from '@/components/AuthorizedLayout'
import Login from '@/pages/Login'
import Logout from '@/pages/Logout'

export default class MainRouter extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    console.log('MainRouter > componentDidMount called!')
  }

  render() {
    return (
      <BrowserRouter basename="control">
        <Switch>
          <Route path="/logout" component={Logout}/>
          <Route path="/login" component={Login}/>
          <AuthorizedRoute path="/" component={AuthorizedLayout}/>
        </Switch>
      </BrowserRouter>
    )
  }
}

