import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'
import update from 'immutability-helper'
import store from 'store'

import { renewToken, verifyToken } from '@/actions/session'

@connect((state) => {
  return {
    pending: state.session.auth.pending,
    authenticated: state.session.auth.authenticated,
  }
}, (dispatch, ownProps) => {
  return {
    verifyToken(token) {
      return dispatch(verifyToken(token))
    }
  }
})
export default class AuthorizedRoute extends Component {
  constructor(props) {
    super(props)

    this.state = {
      pending: true
    }
  }

  componentDidMount() {
    this.renewTokenIfExists()
  }

  async renewTokenIfExists() {
    let token = store.get('token')

    if (token) {
      this.props.verifyToken(token)
        .then(() => {
          this.setState((prevState, props) => {
            return update(prevState, {
              $merge: { pending: false }
            })
          })
        })
    } else {
      this.setState((prevState, props) => {
        return update(prevState, {
          $merge: { pending: false }
        })
      })
    }
  }

  render() {
    const { component: Component, pending, authenticated, ...rest } = this.props

    return (
      <Route {...rest} render={props => {
        if (pending || this.state.pending) return (
          <div className="page">
            <div className="page-loading">
              <div className="lds-dual-ring"></div>
            </div>
          </div>
        )
        return authenticated
          ? <Component {...this.props}/>
          : <Redirect to="/login"/>
      }} />
    )
  }
}