import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import classNames from 'classnames'

import { SERVER_ADDRESS } from '../constants/Globals'

import { apiFetchContent, apiUpdateContent, changeActiveContent } from '@/actions/content'

import Collapse from '@/components/Collapse'

@connect((state) => {
  return {
    content: state.content.active,
  }
}, (dispatch, ownProps) => {
  return {
    apiFetchContent(params) {
      return dispatch(apiFetchContent(params))
    },

    apiUpdateContent(params, props) {
      return dispatch(apiUpdateContent(params, props))
    },

    changeActiveContent(props) {
      return dispatch(changeActiveContent(props))
    },
  }
})
export default class CollapseBoxRoxoQuemSomos extends Component {
  constructor(props) {
    super(props)

    this.target = React.createRef()

    this.handleSave = this.handleSave.bind(this)
    this.handleSaveSuccess = this.handleSaveSuccess.bind(this)
    this.handleSaveError = this.handleSaveError.bind(this)
    this.setEditorContent = this.setEditorContent.bind(this)
  }

  componentDidMount() {
    const { id } = this.props

    this.props.apiFetchContent({ id: id }).then(this.setEditorContent)
  }

  componentDidUpdate(prevProps, prevState) {
    const { id } = this.props

    if (prevProps.open !== this.props.open) {
      this.setEditor()
      this.props.apiFetchContent({ id: id }).then(this.setEditorContent)
    }
  }

  setEditor() {
    if (window.tinymce) {
      window.tinymce.init({
        target: this.target.current,
        height: 300,
        menubar: false,
        resize: false,
        elementpath: false,
        plugins: 'autolink autosave link image wordcount',
        toolbar: 'styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist blockquote | outdent indent | link unlink image | code',
         style_formats: [
          {title: 'Header 1', format: 'h1'},
          {title: 'Header 2', format: 'h2'},
          {title: 'Header 3', format: 'h3'},
          {title: 'Paragraph', format: 'p'},
          {title: 'Blockquote', format: 'blockquote'}
        ],
        fontsize_formats: '12pt 14pt 18pt 24pt 36pt',
        language: 'pt_BR',
      })
    }
  }

  handleTextInputChange(name, event) {
    const { content } = this.props
    const value = event.target.value

    if (value !== undefined && value !== null) {
      this.props.changeActiveContent(update(content.item, {
          data: { $merge: { [name]: value } }
      }))
    }
  }

  handleSave(event) {
    const { content } = this.props
    const data = new FormData()
    const editorData = tinyMCE.activeEditor.getContent({ format: 'raw' }) || null

    for (var property in content.item) {
      if (content.item.hasOwnProperty(property)) {

        if (property === 'data') {
          let payload = content.item[property]
          if (editorData) payload = Object.assign({}, payload, { content: editorData })
            
          data.append(property, JSON.stringify(payload))
        } else {
          data.append(property, content.item[property])
        }

      }
    }

    this.props.apiUpdateContent({ id: content.item.id }, data)
      .then(this.handleSaveSuccess)
      .catch(this.handleSaveError)
  }

  handleSaveSuccess(result) {
    alert('Dados salvos com sucesso!')
  }

  handleSaveError(err) {
    alert('Um erro ocorreu. Por favor contatar o administrador da plataforma.')
  }

  setEditorContent() {
    const { content } = this.props

    if (tinyMCE.activeEditor && tinyMCE.activeEditor.contentDocument && content.item && content.item.data.content) {
      tinyMCE.activeEditor.execCommand('mceSetContent', false, content.item.data.content)
    }

    if ((!content.item && content.loading) || !tinyMCE.activeEditor.contentDocument) {
      setTimeout(() => { this.setEditorContent() }, 100)
    }
  }

  render() {
    const { content } = this.props

    return (
      <div className="collapse-content">
        <div className="collapse-row">
          <div className="left">
            <div className="inputs">
              <div className="input w5-q3">
                <label>Título:</label>
                <input type="text" value={content.item && content.item.data && content.item.data.title || ''} onChange={this.handleTextInputChange.bind(this, 'title')}/>
              </div>
              <div className="input input-tinymce">
                <label>Texto:</label>
                <textarea ref={this.target}></textarea>
              </div>
            </div>
          </div>
          <div className="right"></div>
        </div>
        <div className="collapse-row">
          <div className="buttons">
            <button className="btn btn-one" onClick={this.handleSave}>Salvar</button>
          </div>
        </div>
      </div>
    )
  }
}




