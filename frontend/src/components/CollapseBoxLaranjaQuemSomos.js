import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import classNames from 'classnames'

import { SERVER_ADDRESS } from '../constants/Globals'

import { apiFetchContent, apiUpdateContent, changeActiveContent } from '@/actions/content'

import Collapse from '@/components/Collapse'

@connect((state) => {
  return {
    content: state.content.active,
  }
}, (dispatch, ownProps) => {
  return {
    apiFetchContent(params) {
      return dispatch(apiFetchContent(params))
    },

    apiUpdateContent(params, props) {
      return dispatch(apiUpdateContent(params, props))
    },

    changeActiveContent(props) {
      return dispatch(changeActiveContent(props))
    },
  }
})
export default class CollapseBoxLaranjaQuemSomos extends Component {
  constructor(props) {
    super(props)

    this.handleSave = this.handleSave.bind(this)
    this.handleSaveSuccess = this.handleSaveSuccess.bind(this)
    this.handleSaveError = this.handleSaveError.bind(this)
  }

  componentDidMount() {
    const { id } = this.props

    this.props.apiFetchContent({ id: id })
  }

  componentDidUpdate(prevProps, prevState) {
    const { id } = this.props

    if (prevProps.open !== this.props.open) this.props.apiFetchContent({ id: id })
  }

  handleTextInputChange(name, event) {
    const { content } = this.props
    const value = event.target.value

    if (value !== undefined && value !== null) {
      this.props.changeActiveContent(update(content.item, {
          data: { $merge: { [name]: value } }
      }))
    }
  }

  handleSave(event) {
    const { content } = this.props
    const data = new FormData()

    for (var property in content.item) {
      if (content.item.hasOwnProperty(property)) {
        if (property === 'data') data.append(property, JSON.stringify(content.item[property]))
        else data.append(property, content.item[property])
      }
    }

    this.props.apiUpdateContent({ id: content.item.id }, data)
      .then(this.handleSaveSuccess)
      .catch(this.handleSaveError)
  }

  handleSaveSuccess(result) {
    alert('Dados salvos com sucesso!')
  }

  handleSaveError(err) {
    alert('Um erro ocorreu. Por favor contatar o administrador da plataforma.')
  }

  render() {
    const { content } = this.props

    return (
      <div className="collapse-content">
        <div className="collapse-row">
          <div className="left">
            <div className="inputs">
              <div className="input w5-q3">
                <label>Texto:</label>
                <input type="text" value={content.item && content.item.data && content.item.data.text || ''} onChange={this.handleTextInputChange.bind(this, 'text')}/>
              </div>
            </div>
          </div>
          <div className="right"></div>
        </div>
        <div className="collapse-row">
          <div className="buttons">
            <button className="btn btn-one" onClick={this.handleSave}>Salvar</button>
          </div>
        </div>
      </div>
    )
  }
}




