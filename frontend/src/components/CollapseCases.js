import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import classNames from 'classnames'

import { apiFetchByComponent, apiDeleteContent } from '@/actions/content'

import Collapse from '@/components/Collapse'
import WidgetFields from '@/components/WidgetFields'

import picture_placeholder from '@/assets/img/picture_placeholder.png'

@connect((state) => {
  return {
    content: state.content.list,
  }
}, (dispatch, ownProps) => {
  return {
    apiFetchByComponent(params) {
      return dispatch(apiFetchByComponent(params))
    },
  }
})
export default class CollapseCases extends Component {
  constructor(props) {
    super(props)

    this.formImagem = React.createRef()

    //this.handleSave = this.handleSave.bind(this)
  }

  componentDidMount() {
    this.props.apiFetchByComponent({ id: '4' })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.open !== this.props.open) this.props.apiFetchByComponent({ id: '4' })
  }

  render() {
    const { content } = this.props
    const widgetFieldsConfig = {
      source: content,
      componentId: 4,
      createRoute: '/components/cases/create',
      updateRoute: '/components/cases/update',
      deleteAction: apiDeleteContent,
      fields: [
        {
          name: 'Nome',
          attribute: 'name',
          percentage: '50%'
        },
        {
          name: 'Descrição',
          attribute: 'description',
          percentage: '50%'
        }
      ],
      methods: {}
    }

    return (
      <div className="collapse-content">
        <WidgetFields config={widgetFieldsConfig} history={this.props.history}/>
      </div>
    )
  }
}




