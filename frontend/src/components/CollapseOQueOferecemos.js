import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import classNames from 'classnames'

import { apiFetchByComponent, apiDeleteContent } from '@/actions/content'

import Collapse from '@/components/Collapse'
import WidgetFields from '@/components/WidgetFields'

import picture_placeholder from '@/assets/img/picture_placeholder.png'

@connect((state) => {
  return {
    content: state.content.list,
  }
}, (dispatch, ownProps) => {
  return {
    apiFetchByComponent(params) {
      return dispatch(apiFetchByComponent(params))
    },
  }
})
export default class CollapseOQueOferecemos extends Component {
  constructor(props) {
    super(props)

    this.formImagem = React.createRef()

    this.state = {
      form: {
        title: '',
        picture: {
          filename: picture_placeholder,
          hash: ''
        }
      },
    }

    //this.handleSave = this.handleSave.bind(this)
  }

  componentDidMount() {
    this.props.apiFetchByComponent({ id: '3' })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.open !== this.props.open) this.props.apiFetchByComponent({ id: '3' })
  }

  render() {
    const { content } = this.props
    const oQueOferecemosConfig = {
      source: content,
      componentId: 3,
      createRoute: '/components/o-que-oferecemos/create',
      updateRoute: '/components/o-que-oferecemos/update',
      deleteAction: apiDeleteContent,
      fields: [
        {
          name: 'Nome',
          attribute: 'name',
          percentage: '100%'
        }
      ],
      methods: {}
    }

    return (
      <div className="collapse-content">
        <WidgetFields config={oQueOferecemosConfig} history={this.props.history}/>
      </div>
    )
  }
}




