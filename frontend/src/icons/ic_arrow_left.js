import React, { Component } from 'react'

export default () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 600 600"><path d="M25 300l285.3 286.3 30-29.6L84.5 300 340.3 43.5l-30-29.8z"/><path d="M82.8 278.7H575v42.7H82.8z"/></svg>
)

