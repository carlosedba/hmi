import axios from 'axios'
import { HMI_API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function apiFetchAllEventSubscriptions() {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/event-subscriptions`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_FETCH_ALL_EVENT_SUBSCRIPTIONS,
		payload: request
	}
}

export function apiFetchEventSubscriptionsByEvent(params) {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/event-subscriptions/event/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_FETCH_ALL_EVENT_SUBSCRIPTIONS,
		payload: request
	}
}

export function apiFetchEventSubscription(params) {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/event-subscriptions/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_FETCH_EVENT_SUBSCRIPTION,
		payload: request
	}
}

export function apiCreateEventSubscription(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${HMI_API_ENDPOINT}/event-subscriptions/create`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_CREATE_EVENT_SUBSCRIPTION,
		payload: request
	}
}

export function apiUpdateEventSubscription(params, props) {
	const request = axios({
		method: 'put',
		data: props,
		url: `${HMI_API_ENDPOINT}/event-subscriptions/update/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_UPDATE_EVENT_SUBSCRIPTION,
		payload: request
	}
}

export function apiDeleteEventSubscription(params) {
	const request = axios({
		method: 'delete',
		url: `${HMI_API_ENDPOINT}/event-subscriptions/delete/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_DELETE_EVENT_SUBSCRIPTION,
		payload: request
	}
}