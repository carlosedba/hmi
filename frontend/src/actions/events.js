import axios from 'axios'
import { HMI_API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function apiFetchAllEvents() {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/events`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_FETCH_ALL_EVENTS,
		payload: request
	}
}

export function apiFetchEvent(params) {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/events/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_FETCH_EVENT,
		payload: request
	}
}

export function apiCreateEvent(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${HMI_API_ENDPOINT}/events/create`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`,
			'Event-Type': 'multipart/form-data',
		},
	})

	return {
		type: types.API_CREATE_EVENT,
		payload: request
	}
}

export function apiUpdateEvent(params, props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${HMI_API_ENDPOINT}/events/update/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`,
			'Event-Type': 'multipart/form-data',
		},
	})

	return {
		type: types.API_UPDATE_EVENT,
		payload: request
	}
}

export function apiDeleteEvent(params) {
	const request = axios({
		method: 'delete',
		url: `${HMI_API_ENDPOINT}/events/delete/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`,
		},
	})

	return {
		type: types.API_DELETE_EVENT,
		payload: request
	}
}

export function changeActiveEvent(props) {
	return {
		type: types.CHANGE_ACTIVE_EVENT,
		payload: props
	}
}

export function changeNewEvent(props) {
	return {
		type: types.CHANGE_NEW_EVENT,
		payload: props
	}
}

export function changeUpdatedEvent(props) {
	return {
		type: types.CHANGE_UPDATED_EVENT,
		payload: props
	}
}