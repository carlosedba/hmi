import axios from 'axios'
import { HMI_API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function apiFetchAllPosts() {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/posts`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_FETCH_ALL_POSTS,
		payload: request
	}
}

export function apiFetchPost(params) {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/posts/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_FETCH_POST,
		payload: request
	}
}

export function apiCreatePost(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${HMI_API_ENDPOINT}/posts/create`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`,
			'Post-Type': 'multipart/form-data',
		},
	})

	return {
		type: types.API_CREATE_POST,
		payload: request
	}
}

export function apiUpdatePost(params, props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${HMI_API_ENDPOINT}/posts/update/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`,
			'Post-Type': 'multipart/form-data',
		},
	})

	return {
		type: types.API_UPDATE_POST,
		payload: request
	}
}

export function apiDeletePost(params) {
	const request = axios({
		method: 'delete',
		url: `${HMI_API_ENDPOINT}/posts/delete/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`,
		},
	})

	return {
		type: types.API_DELETE_POST,
		payload: request
	}
}

export function changeActivePost(props) {
	return {
		type: types.CHANGE_ACTIVE_POST,
		payload: props
	}
}

export function changeNewPost(props) {
	return {
		type: types.CHANGE_NEW_POST,
		payload: props
	}
}

export function changeUpdatedPost(props) {
	return {
		type: types.CHANGE_UPDATED_POST,
		payload: props
	}
}