import axios from 'axios'
import { HMI_API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function apiFetchAllContent() {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/content`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_FETCH_ALL_CONTENT,
		payload: request
	}
}

export function apiFetchByComponent(params) {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/content/component/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_FETCH_BY_COMPONENT,
		payload: request
	}
}

export function apiFetchContent(params) {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/content/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_FETCH_CONTENT,
		payload: request
	}
}

export function apiCreateContent(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${HMI_API_ENDPOINT}/content/create`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`,
			'Content-Type': 'multipart/form-data',
		},
	})

	return {
		type: types.API_CREATE_CONTENT,
		payload: request
	}
}

export function apiUpdateContent(params, props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${HMI_API_ENDPOINT}/content/update/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`,
			'Content-Type': 'multipart/form-data',
		},
	})

	return {
		type: types.API_UPDATE_CONTENT,
		payload: request
	}
}

export function apiDeleteContent(params) {
	const request = axios({
		method: 'delete',
		url: `${HMI_API_ENDPOINT}/content/delete/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`,
		},
	})

	return {
		type: types.API_DELETE_CONTENT,
		payload: request
	}
}

export function changeActiveContent(props) {
	return {
		type: types.CHANGE_ACTIVE_CONTENT,
		payload: props
	}
}

export function changeNewContent(props) {
	return {
		type: types.CHANGE_NEW_CONTENT,
		payload: props
	}
}

export function changeUpdatedContent(props) {
	return {
		type: types.CHANGE_UPDATED_CONTENT,
		payload: props
	}
}