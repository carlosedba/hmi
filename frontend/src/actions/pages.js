import axios from 'axios'
import { HMI_API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function apiFetchAllPages() {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/pages`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_FETCH_ALL_PAGES,
		payload: request
	}
}

export function apiFetchPage(params) {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/pages/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_FETCH_PAGE,
		payload: request
	}
}

export function apiCreatePage(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${HMI_API_ENDPOINT}/pages/create`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`,
			'Page-Type': 'multipart/form-data',
		},
	})

	return {
		type: types.API_CREATE_PAGE,
		payload: request
	}
}

export function apiUpdatePage(params, props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${HMI_API_ENDPOINT}/pages/update/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`,
			'Page-Type': 'multipart/form-data',
		},
	})

	return {
		type: types.API_UPDATE_PAGE,
		payload: request
	}
}

export function apiDeletePage(params) {
	const request = axios({
		method: 'delete',
		url: `${HMI_API_ENDPOINT}/pages/delete/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`,
		},
	})

	return {
		type: types.API_DELETE_PAGE,
		payload: request
	}
}

export function changeActivePage(props) {
	return {
		type: types.CHANGE_ACTIVE_PAGE,
		payload: props
	}
}

export function changeNewPage(props) {
	return {
		type: types.CHANGE_NEW_PAGE,
		payload: props
	}
}

export function changeUpdatedPage(props) {
	return {
		type: types.CHANGE_UPDATED_PAGE,
		payload: props
	}
}