import axios from 'axios'
import { HMI_API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function apiFetchComponents() {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/components`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_FETCH_COMPONENTS,
		payload: request
	}
}

export function apiFetchComponent(params) {
	const request = axios({
		method: 'get',
		url: `${HMI_API_ENDPOINT}/components/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_FETCH_COMPONENT,
		payload: request
	}
}

export function apiCreateComponent(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${HMI_API_ENDPOINT}/components/create`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_CREATE_COMPONENT,
		payload: request
	}
}

export function apiUpdateComponent(params, props) {
	const request = axios({
		method: 'put',
		data: props,
		url: `${HMI_API_ENDPOINT}/components/update/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_UPDATE_COMPONENT,
		payload: request
	}
}

export function apiDeleteComponent(params) {
	const request = axios({
		method: 'delete',
		url: `${HMI_API_ENDPOINT}/components/delete/${params.id}`,
		headers: {
			'Authorization': `Bearer ${store.get('token')}`
		},
	})

	return {
		type: types.API_DELETE_COMPONENT,
		payload: request
	}
}